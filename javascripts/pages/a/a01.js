window.Manipulator = (function() {

    var _count;
    var _instance;

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        _count = 0;
        _instance = this;
        this.$grid = $('.grid');

        this.$grid.isotope({
            itemSelector: '.col-1',
            resizable: true,
            masonry: {
                columnWidth: 300,
                gutter: 30
            },
            getSortData: {
                date_created: '[data-date-created]',
                like_number: '[data-like-number]',
            }
        });

        var imgLoad  = imagesLoaded(this.$grid);
        imgLoad.on('always', $.proxy(this.doImageLoadedHandle, this));

        this.initCarousel();
        this.setupEvents();
    };

    Manipulator.prototype.doImageLoadedHandle = function(imgLoad) {
        for(var i=0, len = imgLoad.images.length; i< len; i++){
            var image = imgLoad.images[i];
            if(image.isLoaded){
                var parent = $(image.img).parent().parent().parent();
                $(parent).find('.col-sm-pics').removeClass('isLoaded');
                $(parent).find('.col-sm-context').vrModuleDot({});
            }
        }

        if(imgLoad.isComplete){
            imgLoad.off('always');
            this.$grid.isotope('layout');
        }
    };

    Manipulator.prototype.setupEvents = function(){
        $(window).on('resize', $.proxy( this.onResizeHandle, this ));
        $(window).on('orientationchange', $.proxy( this.onOrientationChange, this ));
        $('.col-sm-like').on('click', $.proxy( this.onLikeClicked, this )); 
        $('.btn-loadmore').on('click', $.proxy( this.onBntLoadClicked, this ));
        $('.icons-gototop').on('click', $.proxy( this.onSrolltotop, this ));        
    };

    Manipulator.prototype.onSrolltotop = function(evt){
        $('html, body').stop().animate({ scrollTop: '0' }, 600 , 'easeInOutCubic');
    }

    // just a append element example
    Manipulator.prototype.onBntLoadClicked = function(evt){
        $('.btn-loadmore').off('click', $.proxy( this.onBntLoadClicked, this ));
        $(document).off('scroll', $.proxy(this.onContainerScroll ,this));

        $.ajax({
                url: "/cyberisland/html/partials/ajaxLoadPics.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.appendGriditem, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.appendGriditem = function($html){
        _count = _count + 1;
        var dom = $('<div>' + $html +'</div>');
        var grid = this.$grid;
        var imgLoad =  imagesLoaded(dom);
        imgLoad.on('always', function(){
            for(var i=0, len = imgLoad.images.length; i<len; i++){
                var image = imgLoad.images[i];
                if(image.isLoaded){
                    $(image.img).parent().parent().removeClass('isLoaded');
                }
            }

            if(imgLoad.isComplete){
                dom.find('.col-1').each(function(){
                    var elem = $(this);
                    grid.append( elem ).isotope('appended', elem );
                    elem.find('.col-sm-context').vrModuleDot({});
                });

                if(_count == 1){
                    $('.btn-loadmore').off('click', $.proxy( _instance.onBntLoadClicked, _instance ))
                        .addClass("icons-loading").removeClass("icons-btn-load");

                    $(document).on('scroll', $.proxy(_instance.onContainerScroll ,_instance));
                }else if(_count == 3){
                    $('.btn-loadmore').on('click', $.proxy( _instance.onBntLoadClicked, _instance ))
                        .addClass("icons-btn-load").removeClass("icons-loading");

                    $(document).off('scroll', $.proxy(_instance.onContainerScroll ,_instance));
                    _count = 0;
                }else{
                    $(document).on('scroll', $.proxy(_instance.onContainerScroll ,_instance));
                }
            }

            imgLoad.off('always');
            grid.isotope('layout');
        });

        //grid.isotope('layout');

    }

    Manipulator.prototype.onContainerScroll = function(e){
        var target = e.currentTarget;
        var inner_top = document.querySelector(".carousel");
        var inner_buttom = document.querySelector(".container.main");
        var outer = document.querySelector("body");
        var h1 = inner_top.offsetHeight + inner_buttom.offsetHeight + 30;
        var h2 = outer.clientHeight;
        var h3 = $(target).scrollTop();
        if((h1 - h2) - h3 <= 0){
            this.onBntLoadClicked();
        }
    };

    Manipulator.prototype.onLikeClicked = function(evt){
        var target = evt.currentTarget;
        var isLogin = ($(target).parent().index() == 1);
        if(!isLogin){
            $.ajax({
                url: "/cyberisland/html/partials/login.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
            }).success(  $.proxy( this.showBlockUI, this ))
            .fail( $.proxy( this.onAjaxFail, this ));
            return;
        }

        var target = evt.currentTarget;
        var line_number = 0;
        if($(target).hasClass("icons-like")){
            $(target).removeClass("icons-like").addClass("icons-unlike");
            var line_number = parseInt($(target).find('p').text()) - 1;
        }else{
            $(target).removeClass("icons-unlike").addClass("icons-like");
            var line_number = parseInt($(target).find('p').text()) + 1;
        }

        $(target).find('p').text( line_number);
        this.$grid.isotope( 'updateSortData', this.$grid.children() );
    };

    Manipulator.prototype.onBtnConfirmClick = function(evt){
        var target = evt.currentTarget;
        $('.btn-confirm').off('click', $.proxy( this.onBtnConfirmClick, this ));
        $('.btn-cancel').off('click', $.proxy( this.onBtnCancelClick, this ));
        $('.pop-alert').parent().addClass('hidden');
    }

    Manipulator.prototype.onBtnCancelClick = function(evt){
        var target = evt.currentTarget;
        $('.btn-confirm').off('click', $.proxy( this.onBtnConfirmClick, this ));
        $('.btn-cancel').off('click', $.proxy( this.onBtnCancelClick, this ));
        $('.pop-alert').parent().addClass('hidden');
    }

    Manipulator.prototype.initCarousel = function(){
        $('.carousel').vrModuleCarousel({
            container: $(".carousel-inner"),
            trigger: $(".carousel-left, .carousel-right")
        });
    }

    Manipulator.prototype.onResizeHandle = function(){
        //this.$grid.isotope('layout');
        $('.carousel').vrModuleCarousel("update");
    }

    Manipulator.prototype.onOrientationChange = function(){
        this.$grid.isotope('layout');
    }

    Manipulator.prototype.showBlockUI = function(html){
        $.vrModuleBlockUI({html: html});
    }

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
       console.log("Request failed: " + textStatus )
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});