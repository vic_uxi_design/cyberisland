window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.setupEvents();

        var imgLoad  = imagesLoaded($('.col-pics'));
        imgLoad.on('always', $.proxy(this.doImageLoadedHandle, this));

        $('.col-pics .mask-helf').each(function(){
            $(this).vrModuleDot({
                elemHeight: 80,
                lineHeight: 30,
                threshold_line: 2,
                threshold_word_per_line: 12
            });

        });
    };

    Manipulator.prototype.setupEvents = function(){
        $('.search-condi-box').on('click', $.proxy( this.onClickHandle, this ));
        $('.btn-readMore.group').on('click', $.proxy( this.onLoadMoreAlbumGroup, this ));
        $('.btn-readMore.album').on('click', $.proxy( this.onLoadMoreAlbum, this ));
        $('.btn-readMore.pics').on('click', $.proxy( this.onLoadMorePics, this ));
        $('.gototop').on('click', $.proxy( this.onSrolltotop, this ));    
    };

    Manipulator.prototype.doImageLoadedHandle = function(imgLoad) {
        for(var i=0, len = imgLoad.images.length; i< len; i++){
            var image = imgLoad.images[i];
            if(image.isLoaded){
                $(image.img).parent().parent().removeClass('isLoaded');
            }
        }

        if(imgLoad.isComplete){
            imgLoad.off('always');
        }
    };

    Manipulator.prototype.onSrolltotop = function(evt){
        $('html, body').stop().animate({ scrollTop: '0' }, 600 , 'easeInOutCubic');
    }

    Manipulator.prototype.onLoadMoreAlbumGroup = function(evt){ 
        $.ajax({
                url: "/cyberisland/html/partials/ajaxLoadAlbumGroup.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.doAppendMoreAlbumGroup, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.doAppendMoreAlbumGroup = function(html){
        var bnt = $(".btn-readMore.group");
        var parent = bnt.parent();
        bnt.before(html);
        var dom = "<div>" + html + "</div>";
        
        parent.find('.col-pics .mask-helf').each(function(){
            $(this).vrModuleDot({
                elemHeight: 80,
                lineHeight: 30,
                threshold_line: 2,
                threshold_word_per_line: 12
            });
        });

        var imgLoad  = imagesLoaded(parent.find('.col-pics'));
        imgLoad.on('always', $.proxy(this.doImageLoadedHandle, this));
    }

    Manipulator.prototype.onLoadMoreAlbum = function(evt){
        $.ajax({
                url: "/cyberisland/html/partials/ajaxLoadAlbum.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.doAppendMoreAlbum, this ))
        .fail( $.proxy( this.onAjaxFail, this ));

    }

    Manipulator.prototype.doAppendMoreAlbum = function(html){
        
        var bnt = $(".btn-readMore.album");
        var parent = bnt.parent();
        bnt.before(html);

        var dom = "<div>" + html + "</div>";

        var imgLoad  = imagesLoaded(parent.find('.col-pics'));
        imgLoad.on('always', $.proxy(this.doImageLoadedHandle, this));
    }

    Manipulator.prototype.onLoadMorePics = function(evt){
        $.ajax({
                url: "/cyberisland/html/partials/ajaxLoadAlbum.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.doAppendMorePics, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.doAppendMorePics = function(html){
        var bnt = $(".btn-readMore.pics");
        var parent = bnt.parent();
        bnt.before(html);
        
        var dom = "<div>" + html + "</div>";
        var imgLoad  = imagesLoaded(parent.find('.col-pics'));
        imgLoad.on('always', $.proxy(this.doImageLoadedHandle, this));
    }

    Manipulator.prototype.onClickHandle = function(evt){
        var target = evt.currentTarget;
        var datetype = $(target).attr('data-type');
        var isShow = $(target).hasClass('select');
        if(isShow){
            $(target).removeClass('select').addClass('unselect');
            $(target).find(".search-check").removeClass('icon-check-dark').addClass('icon-check-lite');
            $('.search-condi-'+datetype).addClass('hide');

        }else{
            $(target).removeClass('unselect').addClass('select');
            $(target).find(".search-check").removeClass('icon-check-lite').addClass('icon-check-dark');
            $('.search-condi-'+datetype).removeClass('hide');
        }
    };


    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
       console.log("Request failed: " + textStatus )
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});