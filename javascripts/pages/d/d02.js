window.Manipulator = (function() {

    var _count;
    var _instance;

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        _count = 0;
        _instance = this;
        this.$grid = $('.grid');

        this.$grid.isotope({
            itemSelector: '.col-1',
            resizable: true,
            masonry: {
                columnWidth: 300,
                gutter: 30
            },
            getSortData: {
                date_created: '[data-date-created]',
                like_number: '[data-like-number]',
            }
        });

        var imgLoad  = imagesLoaded(this.$grid);
        imgLoad.on('always', $.proxy(this.doImageLoadedHandle, this));

        this.initMap();
        this.setupEvents();
    };

    Manipulator.prototype.doImageLoadedHandle = function(imgLoad) {
        for(var i=0, len = imgLoad.images.length; i< len; i++){
            var image = imgLoad.images[i];
            if(image.isLoaded){
                var parent = $(image.img).parent().parent().parent();
                $(parent).find('.col-sm-pics').removeClass('isLoaded');
                $(parent).find('.col-sm-context').vrModuleDot({});
               
            }
        }

        if(imgLoad.isComplete){
            imgLoad.off('always');
            this.$grid.isotope('layout');
        }
    };

    Manipulator.prototype.setupEvents = function(){
        $(window).on('resize', $.proxy( this.onResizeHandle, this ));
        $(window).on('orientationchange', $.proxy( this.onOrientationChange, this ));
        $('.btn-confirm').on('click', $.proxy( this.onBtnConfirmClick, this ));
        $('.btn-cancel').on('click', $.proxy( this.onBtnCancelClick, this ));
        $('.gototop').on('click', $.proxy( this.onSrolltotop, this ));
        $('.btn-loadmore').on('click', $.proxy( this.onBntLoadClicked, this ));
        $('.btn-openmap').on('click', $.proxy( this.onOpenMapHandle, this ));

        $('.btn-openCityList').on('click', $.proxy( this.onDoOpenCityLst, this ));
         $('.col-sm-like').on('click', $.proxy( this.onLikeClicked, this ));
    };

    Manipulator.prototype.initMap = function(){
        var clientHeight = $(window).innerHeight();
        $('.tw-map-wraper').css('height', clientHeight - 100);
        this.$map = $('.tw-map-wraper').vrModuleMap({});
    }

   Manipulator.prototype.onOpenMapHandle = function(e){
        var target = e.currentTarget;
        $(target).off('click', $.proxy( this.onOpenMapHandle, this ));
        $(target).on('click', $.proxy( this.onCloseMapHandle, this ));
        $('.btn-openCityList').addClass('hidden');
        $('.tw-map-wraper').removeClass('hidden');
        $('.btn-openmap p').text('收起臺灣地圖');
        $('.btn-openmap').addClass('active');
    }

    Manipulator.prototype.onCloseMapHandle = function(e){
        var target = e.currentTarget;
        $(target).off('click', $.proxy( this.onCloseMapHandle, this ));
        $(target).on('click', $.proxy( this.onOpenMapHandle, this ));

        $('.btn-openCityList').removeClass('hidden');
        $('.tw-map-wraper').addClass('hidden');
        $('.tw-map-wraper').vrModuleMap('backToWhole');
        $('.btn-openmap p').text('展開臺灣地圖');
        $('.btn-openmap').removeClass('active');
    }

    Manipulator.prototype.onDoOpenCityLst = function(evt){
        if($('.citylstPage').length > 0){
            $('.citylstPage').remove();
            return false;
        }

        $.ajax({
                url: "/cyberisland/html/partials/cityList.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.openCityListPage, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.openCityListPage = function($html){
        if($('.citylstPage').length > 0){
            return false;
        }
        $('body').append($html);
    }

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
       console.log("Request failed: " + textStatus )
    }

    Manipulator.prototype.caltwMapWraper = function(){
        var clientHeight = $(window).innerHeight();
        $('.tw-map-wraper').css('height', clientHeight - 100);
        $('.tw-map-wraper').vrModuleMap({});
    }

    Manipulator.prototype.onSrolltotop = function(evt){
        $('html, body').stop().animate({ scrollTop: '0' }, 600 , 'easeInOutCubic');
    }

    // just a append element example
    Manipulator.prototype.onBntLoadClicked = function(evt){
        $('.btn-loadmore').off('click', $.proxy( this.onBntLoadClicked, this ));
        $(document).off('scroll', $.proxy(this.onContainerScroll ,this));

        $.ajax({
                url: "/cyberisland/html/partials/ajaxLoadPics.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.appendGriditem, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.appendGriditem = function($html){
        _count = _count + 1;
        var dom = $('<div>' + $html +'</div>');
        var grid = this.$grid;
        var imgLoad =  imagesLoaded(dom);
        imgLoad.on('always', function(){

            for(var i=0, len = imgLoad.images.length; i<len; i++){
                var image = imgLoad.images[i];
                if(image.isLoaded){
                    $(image.img).parent().parent().removeClass('isLoaded');
                }
            }

            if(imgLoad.isComplete){

                dom.find('.col-1').each(function(){
                    var elem = $(this);
                    grid.append( elem ).isotope('appended', elem );
                    elem.find('.col-sm-context').vrModuleDot({});
                });

                if(_count == 1){
                    $('.btn-loadmore').off('click', $.proxy( _instance.onBntLoadClicked, _instance ))
                        .addClass("icons-loading").removeClass("icons-btn-load");

                    $(document).on('scroll', $.proxy(_instance.onContainerScroll ,_instance));
                }else if(_count == 3){
                    $('.btn-loadmore').on('click', $.proxy( _instance.onBntLoadClicked, _instance ))
                        .addClass("icons-btn-load").removeClass("icons-loading");

                    $(document).off('scroll', $.proxy(_instance.onContainerScroll ,_instance));
                    _count = 0;
                }else{
                    $(document).on('scroll', $.proxy(_instance.onContainerScroll ,_instance));
                }
            }

            imgLoad.off('always');
            grid.isotope('layout');
        });
    }

    Manipulator.prototype.onContainerScroll = function(e){
        var target = e.currentTarget;
        var inner = document.querySelector(".container.main");
        var outer = document.querySelector("body");
        var h1 = inner.offsetHeight;
        var h2 = outer.clientHeight;
        var h3 = $(target).scrollTop();
        if((h1 - h2) - h3 <= 0){
            this.onBntLoadClicked();
        }
    };

    Manipulator.prototype.onLikeClicked = function(evt){
        var target = evt.currentTarget;
        var isLogin = ($(target).parent().index() == 1);
        if(!isLogin){
            $.ajax({
                url: "/cyberisland/html/partials/login.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
            }).success(  $.proxy( this.showBlockUI, this ))
            .fail( $.proxy( this.onAjaxFail, this ));
            return;
        }

        var line_number = 0;
        if($(target).hasClass("icons-like")){
            $(target).removeClass("icons-like").addClass("icons-unlike");
            line_number = parseInt($(target).find('p').text()) - 1;
        }else{
            $(target).removeClass("icons-unlike").addClass("icons-like");
            line_number = parseInt($(target).find('p').text()) + 1;
        }

        $(target).find('p').text( line_number);
        //$(target).parent().attr('data-like-number', line_number);
        this.$grid.isotope( 'updateSortData', this.$grid.children() );
    };

    Manipulator.prototype.onResizeHandle = function(){
        this.$grid.isotope('layout');
    }

    Manipulator.prototype.onOrientationChange = function(){
        this.$grid.isotope('layout');
    }


    Manipulator.prototype.showBlockUI = function(html){
        $.vrModuleBlockUI({html: html});
    }

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
       console.log("Request failed: " + textStatus )
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});
