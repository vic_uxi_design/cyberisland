window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        $('.svgwraper').vrModuleMap({});
        this.setupEvents();
    };


    Manipulator.prototype.setupEvents = function(){
        //$(window).on('resize', $.proxy(this.onResizeHandle, this ));
        $('.btn-openCityList').on('click', $.proxy( this.onDoOpenCityLst, this ));
        //$(window).on('touchmove', $.proxy( this.onWinTouchMove, this ));
    };

    Manipulator.prototype.onWinTouchMove = function(evt){
        evt.preventDefault();
    }

    Manipulator.prototype.onResizeHandle = function(evt){
        var h = $('.randomplate').height() - 48;
        $('.navbar-fluid-bottom').css('top', h);
    }

    Manipulator.prototype.onDoOpenCityLst = function(evt){
        if($('.citylstPage').length > 0){
            $('.citylstPage').remove();
            return false;
        }

        $.ajax({
                url: "/cyberisland/html/partials/cityList.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.openCityListPage, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.openCityListPage = function($html){
        if($('.citylstPage').length > 0){
            return false;
        }
        $('body').append($html);
    }

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
       console.log("Request failed: " + textStatus )
    }
    
    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});