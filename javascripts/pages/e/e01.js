window.Manipulator = (function() {

    var _instance;

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        _instance = this;
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.switch').on('click', $.proxy( this.onCataClicked, this ));
        $('.pro_album').on('mouseenter', $.proxy( this.onAlbumMouseHover, this ));
        $('.btn-switch').on('click', $.proxy( this.openArtistListPage, this));
    };

    Manipulator.prototype.openArtistListPage = function(evt){
        var target = evt.currentTarget;
        if($(target).hasClass('active')){
            $(target).removeClass('active');
            $('.main').find('.catalogue').remove();
            return;
        }

        $(target).addClass('active');
        var request = $.ajax({
                url: "/cyberisland/html/partials/artist.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).done(  $.proxy( this.doOpenPage, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.doOpenPage = function(html){
        $(".main").append(html);
    }

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
       console.log("Request failed: " + textStatus )
    }

   

    Manipulator.prototype.onAlbumMouseHover = function(evt){
        var target = evt.currentTarget;
        $(target).off('mouseenter', $.proxy( this.onAlbumMouseHover, this ));
        $(target).on('mouseleave', $.proxy( this.onAlbumMouseLeave, this ));

        this.doSliderShowPics(target);
        target.cycle = true;
    }

    Manipulator.prototype.doSliderShowPics= function(target){
        //console.log("doSliderShowPics");
        var len = $(target).find('img').length;
        var active = $(target).find('img.active');
        var next = (active.index() == len-1) ? $(target).find('img:first-child') : active.next();
        
        //
        target.cycle = true;
        active.removeClass('active');
        next.addClass('active');
        next.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',  function(e) {
            if(target.cycle == false){
                return;
            }
            _instance.doSliderShowPics(target);
            
        });
    }

    Manipulator.prototype.onAlbumMouseLeave = function(evt){
        var target = evt.currentTarget;
        $(target).on('mouseenter', $.proxy( this.onAlbumMouseHover, this ));
        $(target).off('mouseleave', $.proxy( this.onAlbumMouseLeave, this ));

        var active = $(target).find('img.active');
        var first = $(target).find('img:first-child')
        active.removeClass('active');
        first.addClass('active');

        target.cycle = false;
    }

    Manipulator.prototype.onCataClicked = function(evt){
        var target = evt.currentTarget;

        if($(target).hasClass("on")){
            $(target).removeClass("on");
            $(target).addClass("off");

            $('.catalogue .row').removeClass("show");
            $('.catalogue .row').addClass("hidden");
        }else{
            $(target).removeClass("off");
            $(target).addClass("on");
            $('.catalogue .row').removeClass("hidden");
            $('.catalogue .row').addClass("show");
        }
    }


    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});