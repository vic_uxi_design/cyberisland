window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {

        var imgLoader = imagesLoaded(document.querySelectorAll(".col-pics"));;
        imgLoader.on("always", $.proxy(this.doImageLoaded ,this));

        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.cycle-concentr-a').on('click', $.proxy( this.onAvartaClicked, this ));
    };

    Manipulator.prototype.doImageLoaded = function(imgLoad){
        for(var i=0, len = imgLoad.images.length; i< len; i++){
            var image = imgLoad.images[i];
            if(image.isLoaded){
                $(image.img).parent().parent().removeClass('isLoaded');
            }
        }

        if(imgLoad.isComplete){
            imgLoad.off('always');
        }
    }

    Manipulator.prototype.onAvartaClicked = function(){
        $.ajax({
                url: "/cyberisland/html/partials/menberInfo.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.showBlockUI, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.showBlockUI = function(html){
        $.vrModuleBlockUI({html: html, position: "absolute"});
    }

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
       console.log("Request failed: " + textStatus )
    }


    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});