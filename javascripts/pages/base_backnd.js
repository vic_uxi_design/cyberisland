window.Base = (function() {

    var Base = function(){
        this.init();
    };

    Base.prototype.init = function() {
        this.calViewPortScale();
        this.setupEvents();
    };

    Base.prototype.setupEvents = function(){
        $(window).on('orientationchange', $.proxy( this.onOrientationChange, this ));
        $('.nav-user-control').on('click',$.proxy(this.onNavUsrCntrClick, this));
        $('.opiton-item.setting').on('click',$.proxy(this.openSettingPanel, this));
        $('.nav-upload-control').on('click',$.proxy(this.openUploadPanel, this));
    };

    Base.prototype.onOrientationChange = function(e){
        this.calViewPortScale();
    }

    Base.prototype.calViewPortScale = function(){
        if(window.orientation == undefined){
            return false;
        }

        var content_width = 990, screen_dimension = 0;

        if (window.orientation == 0 || window.orientation == 180) {
            // portrait
            screen_dimension = screen.width * 0.98; // fudge factor was necessary in my case
        } else if (window.orientation == 90 || window.orientation == -90) {
            // landscape
            screen_dimension = screen.height;
        }
        content_width = (screen_dimension >= 1320) ? 1320 : 990;
        var viewport_scale = screen_dimension / content_width;
        
        if(viewport_scale == NaN || viewport_scale == 0){
            viewport_scale = 1;
        }

        // resize viewport
         $('meta[name=viewport]').attr('content',
                                      'width='+ content_width +',' +
                                      'minimum-scale=' + viewport_scale + ', maximum-scale=' + viewport_scale);
    }

    Base.prototype.openUploadPanel = function(){
        var request = $.ajax({
                url: "/cyberisland/html/partials/upload.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).done(  $.proxy( this.donCallEditPanel, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Base.prototype.openSettingPanel = function(){
        var request = $.ajax({
                url: "/cyberisland/html/partials/m01_setting.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).done(  $.proxy( this.donCallEditPanel, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Base.prototype.openEditAlbimPanel = function(evt){
        console.log("openEditAlbimPanel");
        var request = $.ajax({
                url: "/cyberisland/html/partials/editAlbum.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).done(  $.proxy( this.donCallEditPanel, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    };

    Base.prototype.donCallEditPanel = function(html){
        $.vrModuleBlockUI({html: html, position: 'absolute'});
    };

    Base.prototype.onNavUsrCntrClick = function(){
        var option = $('.nav-user-opiton');
        if($('.nav-user-opiton').hasClass('active')){
            option.removeClass('active');
        }else{
            option.addClass('active');
        }
    }

    Base.prototype.onAjaxFail = function(jqXHR, textStatus){
       console.log("Request failed: " + textStatus )
    }

    return Base;
}(jQuery));

$(document).ready(function() {
    new window.Base();
});