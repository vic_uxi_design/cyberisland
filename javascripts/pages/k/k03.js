window.Manipulator = (function() {

    var _count;
    var _instance;

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        _count = 0;
        _instance = this;
        this.$grid = $('.grid');

        this.$grid.isotope({
            itemSelector: '.col-1',
            resizable: true,
            masonry: {
                columnWidth: 300,
                gutter: 30
            },
            getSortData: {
                date_created: function(elem){
                                return Date.parse($(elem).find('.createdate').text());
                                
                            }, 
                like_number: function(elem){
                                return parseInt($(elem).find('.likenum').text(), 10);
                            }
            }
        });

        var imgLoad  = imagesLoaded(this.$grid);
        imgLoad.on('always', $.proxy(this.doImageLoadedHandle, this));

        this.setupEvents();
       
    };

    Manipulator.prototype.doImageLoadedHandle = function(imgLoad) {
        for(var i=0, len = imgLoad.images.length; i< len; i++){
            var image = imgLoad.images[i];
            if(image.isLoaded){
                var parent = $(image.img).parent().parent().parent();
                $(parent).find('.col-sm-pics').removeClass('isLoaded');
                $(parent).find('.col-sm-context').vrModuleDot({});
            }
        }

        if(imgLoad.isComplete){
            imgLoad.off('always');
            this.$grid.isotope('layout');
        }
    };

    Manipulator.prototype.setupEvents = function(){
        $(window).on('orientationchange', $.proxy( this.onOrientationChange, this ));
        $('.avatar').on('click', $.proxy( this.onAvatarClicked, this ));
        $('.gototop').on('click', $.proxy(this.onSrolltotop, this));
        $('.grid-sort-control').on('change', $.proxy( this.onSortChange, this ));
        $('.btn-loadmore').on('click', $.proxy( this.onBntLoadClicked, this ));
    };

    Manipulator.prototype.onAvatarClicked = function(){
        $.ajax({
                url: "/cyberisland/html/partials/userinfo.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.getProInfoSuccess, this ))
        .fail( $.proxy( this.getProInfofail, this ));
    }
 
    Manipulator.prototype.getProInfoSuccess = function(html){
        $.vrModuleBlockUI({html: html, position: "absolute"});
    }

    Manipulator.prototype.getProInfofail = function(jqXHR, textStatus){

        console.log("Request failed: " + textStatus);
    }

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
       console.log("Request failed: " + textStatus )
    };


    Manipulator.prototype.onSortChange = function(evt){
        var value = evt.target.value;
        this.$grid.isotope({ sortBy: value, sortAscending : false});
    };

    Manipulator.prototype.onSrolltotop = function(evt){
        $('html, body').stop().animate({ scrollTop: '0' }, 600 , 'easeInOutCubic');
    }

    Manipulator.prototype.onBntLoadClicked = function(evt){
        $('.btn-loadmore').off('click', $.proxy( this.onBntLoadClicked, this ));
        $(document).off('scroll', $.proxy(this.onContainerScroll ,this));
        $.ajax({
                url: "/cyberisland/html/partials/ajaxLoadPics.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.appendGriditem, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.appendGriditem = function($html){
        _count = _count + 1;
        var dom = $('<div>' + $html +'</div>');
        var grid = this.$grid;
        var imgLoad =  imagesLoaded(dom);
        imgLoad.on('always', function(){

            for(var i=0, len = imgLoad.images.length; i<len; i++){
                var image = imgLoad.images[i];
                if(image.isLoaded){
                    $(image.img).parent().parent().removeClass('isLoaded');
                }
            }

            if(imgLoad.isComplete){

                dom.find('.col-1').each(function(){
                    var elem = $(this);
                    grid.append( elem ).isotope('appended', elem );
                    elem.find('.col-sm-context').vrModuleDot({});
                });

                if(_count == 1){
                    $('.btn-loadmore').off('click', $.proxy( _instance.onBntLoadClicked, _instance ))
                        .addClass("icons-loading").removeClass("icons-btn-load");

                    $(document).on('scroll', $.proxy(_instance.onContainerScroll ,_instance));
                }else if(_count == 3){
                    $('.btn-loadmore').on('click', $.proxy( _instance.onBntLoadClicked, _instance ))
                        .addClass("icons-btn-load").removeClass("icons-loading");

                    $(document).off('scroll', $.proxy(_instance.onContainerScroll ,_instance));
                    _count = 0;
                }else{
                    $(document).on('scroll', $.proxy(_instance.onContainerScroll ,_instance));
                }
            }

            imgLoad.off('always');
            grid.isotope('layout');
        });
    }

    Manipulator.prototype.onContainerScroll = function(e){
        var target = e.currentTarget;
        var inner = document.querySelector(".container.main");
        var outer = document.querySelector("body");
        var h1 = inner.offsetHeight;
        var h2 = outer.clientHeight;
        var h3 = $(target).scrollTop();
        if((h1 - h2) - h3 <= 0){
            this.onBntLoadClicked();
        }
    };

    Manipulator.prototype.onOrientationChange = function(){
        this.$grid.isotope('layout');
    }


    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});