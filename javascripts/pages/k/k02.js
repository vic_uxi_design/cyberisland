window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        $('.google-map-wraper').height($(window).innerHeight() - 274 - 99 -15 -48);
        this.initGoogleMap();
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.avatar').on('click', $.proxy( this.onAvatarClicked, this ));
    };

    Manipulator.prototype.onAvatarClicked = function(){
        $.ajax({
                url: "/cyberisland/html/partials/userinfo.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).done(  $.proxy( this.getProInfoSuccess, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.getProInfoSuccess = function(html){
        $.vrModuleBlockUI({html: html, position: "absolute"});
    }
 
    Manipulator.prototype.onAjaxDone = function(html){
        $.vrModuleBlockUI({html: html});
    }

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
        console.log("Request failed: " + textStatus);
    }

    Manipulator.prototype.initGoogleMap = function(){ 
        var agence = new google.maps.LatLng(23.965142, 120.972905),
              parliament = new google.maps.LatLng(23.965142, 120.972905),
              image = '/cyberisland/images/darkroom/icn_anchor.png',
              marker,
              map;

        var mapOptions = {
          zoom:  7,
          disableDefaultUI: false,
          draggable:true,
          disableDoubleClickZoom:false,
          scrollwheel:true,
          mapTypeControl:false,
          streetViewControl:false,
          zoomControl: true,
          panControl: true,
          zoomControlOptions: {
              style:google.maps.ZoomControlStyle.SMALL
          },


          mapTypeId: google.maps.MapTypeId.ROADMAP,

          styles: [
              {
                featureType: 'landscape',
                elementType: 'all',
                stylers: [
                  { hue: '#ebebeb' },
                  { saturation: -100 },
                  { lightness: 29 },
                  { visibility: 'on' }
                ]
              },{
                featureType: 'landscape.man_made',
                elementType: 'all',
                stylers: [
                  { hue: '#ebebeb' },
                  { saturation: -100 },
                  { lightness: 29 },
                  { visibility: 'on' }
                ]
              },{
                featureType: 'landscape.natural',
                elementType: 'all',
                stylers: [
                  { hue: '#d6d6d6' },
                  { saturation: -100 },
                  { lightness: -12 },
                  { visibility: 'on' }
                ]
              },{
                featureType: 'road.local',
                elementType: 'all',
                stylers: [
                  { hue: '#ffffff' },
                  { saturation: -100 },
                  { lightness: 100 },
                  { visibility: 'on' }
                ]
              },{
                featureType: 'poi',
                elementType: 'all',
                stylers: [
                  { hue: '#cccccc' },
                  { saturation: -100 },
                  { lightness: 9 },
                  { visibility: 'on' }
                ]
              },{
                featureType: 'road.highway',
                elementType: 'all',
                stylers: [
                  { hue: '#f7f7f7' },
                  { saturation: -100 },
                  { lightness: 91 },
                  { visibility: 'on' }
                ]
              },{
                featureType: 'water',
                elementType: 'all',
                stylers: [
                  { hue: '#c8d6ed' },
                  { saturation: 10 },
                  { lightness: 40 },
                  { visibility: 'on' }
                ]
              }
            ],
            center: agence
        };

        map = new google.maps.Map($('.google-map-wraper')[0],mapOptions);

        marker = new google.maps.Marker({
          icon: image,
          map:map,
          draggable:false,
          animation: google.maps.Animation.DROP,
          position: parliament
        });
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});