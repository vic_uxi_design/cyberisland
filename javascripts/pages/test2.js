window.Manipulator = (function() {

    var bounds, mc,x1, x2, xOffset, xOverlap, t1, t2, timer, xVelocity;


    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {

        this.masker = $('.dr-sliders-masker');
        mc = $('.dr-watercourse');
        bounds = {left:this.masker.offset().left, top:this.masker.offset().top, width: this.masker.width(), height: this.masker.height()};
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        this.masker.on('mousedown', $.proxy(this.onMouseDownHandle, this));
    };

    Manipulator.prototype.onMouseDownHandle = function(e){
        var target = e.currentTarget;
        x1 = x2 = mc.offset().left;
        xOffset = e.pageX - x1;
        xOverlap = Math.max(0, mc.width() - bounds.width);
        t1 = t2 = new Date().getTime();
        $(target).on('mousemove', $.proxy(this.onMouseMoveHandle, this));
        $(target).on('mouseup', $.proxy(this.onMouseUpHandle, this));

        if(timer !=null && timer != undefined){
            clearTimeout(timer);
            timer = null;
        }

        timer = setTimeout(this.trackVelocity, 25);
        
    };

    Manipulator.prototype.onMouseMoveHandle = function(e){

       
        var target = e.currentTarget;
        var newX = (e.pageX - xOffset);
        var posX;

        if (newX > bounds.left) {
            posX = (newX + bounds.left) * 0.5;
        } else if (newX < bounds.left - xOverlap) {
            posX = (newX + bounds.left - xOverlap) * 0.5;
        } else {
            posX = newX;
        }

        var diff1 = mc.offset().left - bounds.left;
        var diff2 = posX - mc.offset().left;
        mc.css('left', diff1 + diff2);

/*

        var t = new Date().getTime();
        if (t - t2 > 50) {
            t2 = t1;
            x1 = mc.offset().left;
            t1 = t;
        }*/

        if(e.pageX < bounds.left || e.pageX >  bounds.left + bounds.width || e.pageY < bounds.top || e.pageY > bounds.top + bounds.height){
            mc.trigger('mouseup');
        }
    };

    Manipulator.prototype.onMouseUpHandle = function(e){
        var target = e.currentTarget;
        if(timer !=null && timer != undefined){
            clearTimeout(timer);
            timer = null;
        }
       
        var max = bounds.left, 
            min = bounds.left - xOverlap,
            resistance = 300,
            friction = 0.9,
            spring = 0.1;
        
        (function(){
            timer = setTimeout(function(){
               xVelocity = xVelocity * friction;
               var diff = mc.offset().left - bounds.left;
               var posx = diff + xVelocity;

               var dx = max - posx;
               var ax = dx * spring;
               
/*
               if(posx > max || posx < min){
                  posx = posx * ax;
               }*/

               mc.css('left', posx);
               if(Math.abs(xVelocity) > 0){
                 setTimeout(arguments.callee,25);//每移动一次停留25毫秒
               }else{
                 window.clearTimeout(timer);
                 timer = null;
               }
            },25);
        })();

        $(target).off('mousemove', $.proxy(this.onMouseMoveHandle, this));
        $(target).off('mouseup', $.proxy(this.onMouseUpHandle, this));
    };

    Manipulator.prototype.trackVelocity = function(e){
        xVelocity = mc.offset().left - x1;
        x1 = mc.offset().left;
        timer = setTimeout(arguments.callee, 25);
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});