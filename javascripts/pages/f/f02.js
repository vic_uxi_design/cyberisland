window.Manipulator = (function() {

    var mc, masker, bounds, x1 , xOverlap, next, buoy, _instance, _isAjaxLocked, _isMoved ;

    var Manipulator = function(){
        this.dr_body = $('.dr-pic');
        this.dr_img = $('.dr-pic img');
        this.init();
    };

    Manipulator.prototype.init = function() {
        _instance = this;
        _isAjaxLocked = false;
        _isMoved = false;

        var imgLoader1  = imagesLoaded($('.col-pics'));
        imgLoader1.on('always', $.proxy(this.doImageLoadedHandle, this));

        var imgLoader2  = imagesLoaded($('.dr-thumb'));
        imgLoader2.on('always', $.proxy(this.doImageLoadedHandle2, this));

        next = parseInt($('.initPagerNext').text());
        buoy = $('.buoy');
        mc = $('.dr-watercourse');
        masker = $('.dr-sliders-masker');
        bounds = {left:masker.offset().left, top:masker.offset().top, width:masker.width() , height:masker.height() };
        this.initGoogleMap();
        this.setupEvents();
    };


    Manipulator.prototype.setupEvents = function(){
        $(window).on('resize', $.proxy( this.onResizeHandle, this ));
        $('.dr-pictitle').on('click',$.proxy(this.openSliderPanel, this) );

        $('.dr-thumb img').on('dragstart',$.proxy( this.onThumbImageDrag, this ))
                           .on('click',$.proxy( this.onThumbImageSelect, this ));

        $('.dr-report p').on('click',$.proxy(this.onReportRequest, this) );
        $('.report-box-confirm').on('click',$.proxy( this.onReportConfirmClick, this ));
        $('.report-box-cancel').on('click',$.proxy( this.onReportCancelClick, this ));
        $('.dr-exif-info').on('click',$.proxy( this.onExifclick, this ));
        $('.dr-like').on('click',$.proxy( this.onLikeclick, this ));
        $('.lightbox-full').on('click',$.proxy( this.doFullScreenHandle, this ));

        google.maps.event.addDomListener(window, 'resize', $.proxy( this.initGoogleMap, this ));
    };

    Manipulator.prototype.doImageLoadedHandle = function(imgLoad){
        for(var i=0, len = imgLoad.images.length; i< len; i++){
            var image = imgLoad.images[i];
            if(image.isLoaded){
               $(image.img).parent().removeClass('isLoaded');
            }
        }

        if(imgLoad.isComplete){
            imgLoad.off('always');
        }
    }


    Manipulator.prototype.doImageLoadedHandle2 = function(imgLoad){
      for(var i=0, len = imgLoad.images.length; i< len; i++){
          var image = imgLoad.images[i];
          if(image.isLoaded){
             $(image.img).parent().removeClass('isLoaded');
          }
      }

      if(imgLoad.isComplete){
          imgLoad.off('always');

          if(next){
              buoy.removeClass('hidden');
          }
            
      }
    }

    Manipulator.prototype.calDrakRoomImage = function(imgLoad){
        var image = imgLoad.images[0];
        if(image.isLoaded){
            $(image.img).parent().removeClass('isLoaded');

            /*
             if(this.dr_img.width() > this.dr_img.height()){
             this.dr_img.addClass('fit-width');
             }else{
             this.dr_img.addClass('fit-height');
             }*/
        }

        if(imgLoad.isComplete){
            imgLoad.off('always');
        }
    }

    Manipulator.prototype.doFullScreenHandle = function(evt){
        $('.lightbox-full').off('click',$.proxy( this.doFullScreenHandle, this ));
        $('.lightbox-close').on('click', $.proxy(this.doCloseLightBox, this));
        $(document).on('fscreenchange', $.proxy(this.onFscreenchange, this));
        $('.dr-pic').fullscreen();
        evt.preventDefault();
    };

    Manipulator.prototype.onFscreenchange = function(evt){
        if ($.fullscreen.isFullScreen()) {
            $('.dr-pic').addClass("fullscreem");
        }else{
            $('.lightbox-close').off('click', $.proxy(this.doCloseLightBox, this));
            $('.lightbox-full').on('click',$.proxy( this.doFullScreenHandle, this ));
            $('.dr-pic').removeClass("fullscreem");
            $(document).off('fscreenchange', $.proxy(this.onFscreenchange, this));
        }
    }

    Manipulator.prototype.doCloseLightBox = function(evt){
        $.fullscreen.exit();
    }

    Manipulator.prototype.openSliderPanel = function(evt){
      var wraper = $('.dr-footer');
      if(wraper.hasClass('active')){
          wraper.removeClass('active');
          $('.dr-btn-left').off('click');
          $('.dr-btn-right').off('click');
          $('.dr-sliders-masker').off('mousedown');
          $('.dr-sliders-masker').off('touchstart');
      }else{
          wraper.addClass('active');
          if(!$('.dr-btn-left').hasClass('hidden')){
              $('.dr-btn-left').on('click',$.proxy(this.onBntRightClicked, this) );
              $('.dr-btn-right').on('click',$.proxy(this.onBntLeftClicked, this) );
          }

          $('.dr-sliders-masker').on('mousedown',$.proxy(this.onSliderMouseDown, this));
          $('.dr-sliders-masker').on('touchstart',$.proxy(this.onSliderTouchStart, this));
      }
    }

    Manipulator.prototype.onSliderMouseDown = function(evt){
        var target = evt.currentTarget;
        _isMoved = false;

        $(target).off('mousedown',$.proxy(this.onSliderMouseDown, this));

        var _orgx = evt.pageX + document.documentElement.scrollTop;
        $(target).on('mousemove',{orgx: _orgx},$.proxy(this.onSliderMouseMove, this));
        $(target).on('mouseup',$.proxy(this.onSliderMouseUp, this));
        evt.preventDefault();
    }

    Manipulator.prototype.onSliderMouseMove = function(evt){
      var target = evt.currentTarget;
      _isMoved = true;
      var sx = evt.pageX + document.documentElement.scrollTop;
      var _diffx = sx - evt.data.orgx;

      if(_diffx == 0){
        return false;
      }

      if(_diffx > 0){
         $('.dr-btn-left').trigger('click');
      }else if(_diffx < 0){
          $('.dr-btn-right').trigger('click');
      }

      $(target).trigger('mouseup');
      evt.preventDefault();
    }

    Manipulator.prototype.onSliderMouseUp = function(evt){
        var target = evt.currentTarget;
        $(target).off('mousemove',$.proxy(this.onSliderMouseMove, this));
        $(target).off('mouseup',$.proxy(this.onSliderMouseUp, this));
        $(target).on('mousedown',$.proxy(this.onSliderMouseDown, this));
    }

    Manipulator.prototype.onSliderTouchStart = function(evt){
        var target = evt.currentTarget;
        _isMoved = false;

        $(target).off('touchstart',$.proxy(this.onSliderTouchStart, this));

        evt.preventDefault();
        var touch = evt.originalEvent.touches[0];
        var _orgx = touch.pageX + document.documentElement.scrollTop;

        //$(target).on('touchmove',{orgx: _orgx}, $.proxy(this.onSliderTouchMove, this));
        $(target).on('touchend',$.proxy(this.onSliderTouchEnd, this));
    }

    Manipulator.prototype.onSliderTouchMove = function(evt){
        var target = evt.currentTarget;
        _isMoved = true;

        var touch = evt.originalEvent.touches[0];
        var sx = touch.pageX + document.documentElement.scrollTop;
        var _diffx = sx - evt.data.orgx;

        if(_diffx == 0){
            return false;
        }

        if(_diffx > 0){
           $('.dr-btn-left').trigger('click');
        }else if(_diffx < 0){
            $('.dr-btn-right').trigger('click');
        }

        evt.preventDefault();
        $(target).trigger('touchend');
    }

    Manipulator.prototype.onSliderTouchEnd = function(evt){
        var target = evt.currentTarget;
        $(target).off('touchmove', $.proxy(this.onSliderTouchMove, this));
        $(target).off('touchend',$.proxy(this.onSliderTouchEnd, this));
        $(target).on('touchstart',$.proxy(this.onSliderTouchStart, this));
    }


    Manipulator.prototype.onBntLeftClicked = function(evt){
        var x1 = parseInt(mc.css('left'));
        
        var mcw = 0;
        if(next){
            mcw = Math.floor(buoy.offset().left + buoy.width() + 15 - mc.offset().left);
        }else{
            mcw = Math.floor(buoy.offset().left - mc.offset().left);
        }
        
        var xOverlap = Math.max(0, mcw - bounds.width);
        
        var va = -140;
        x1 = x1 + va;

        if(x1 < -xOverlap ){
            x1 = -xOverlap - 20;
        }

        mc.stop().animate({left: x1 + "px"}, { duration: 300, queue: false, easing:'easeOutCubic', complete: function(){
            if( x1 < -xOverlap ){
                mc.stop().animate({left: -xOverlap + "px"}, { duration: 200, queue: false, easing:'easeOutCubic'});
            }

            if(x1 < (-xOverlap + 90) && !_isAjaxLocked && next){
                _isAjaxLocked = true;

                $.ajax({
                    url: "/cyberisland/json/apiGetF01Content.json",
                    type: "GET",
                    data: { id : '0' },
                    dataType: 'html'
                }) .done(function( data ) {
                    _instance.appendContent(data);
                });

            }

        }});
    }

    Manipulator.prototype.onBntRightClicked = function(evt){
        var x1 = parseInt(mc.css('left'));
        
        var va = 140;
        x1 = x1 + va;

        if(x1 > 0){
            x1 = 20;
        }

        mc.stop().animate({left: x1 + "px"}, { duration: 300, queue: false, easing:'easeOutCubic', complete: function(){
            if( x1 > 0 ){
                mc.stop().animate({left:  "0px"}, { duration: 200, queue: false, easing:'easeOutCubic'});
            }
        }});
      
    }

     Manipulator.prototype.appendContent = function(json){
      
        var result = $.parseJSON(json);
        if(result.next){
           next = parseInt(result.next);
        }

       
      
        if(next){
          if(!buoy.hasClass('isLoaded')){
            buoy.addClass('isLoaded');
          } 
        }else{
          if(buoy.hasClass('isLoaded')){
            buoy.removeClass('isLoaded');
          } 
        }

/*
        var dom = $("<div>");
      
        for(var i=0, len = result.images.length; i< len; i++){
          var html = "<div class=\"dr-thumb\">"
                  + "<img alt=\""+ result.images[i][0] +  "\" src="+ result.images[i][0] +">"
                  + "</div>";
          dom.append(html);
        }

        var imgLoad = imagesLoaded(dom);

        console.log(dom);
*/

        buoy.remove();
        $("#tmpl").tmpl(result.images).appendTo(".dr-watercourse");
        buoy.appendTo(".dr-watercourse");


        var imgLoad = imagesLoaded('.dr-thumb');
        imgLoad.on('always', function(){
          for(var i=0, len = imgLoad.images.length; i< len; i++){
              var image = imgLoad.images[i];
              if(image.isLoaded){
                 $(image.img).parent().removeClass('isLoaded');
                 $(image.img).on('dragstart',$.proxy( _instance.onThumbImageDrag, _instance ))
                           .on('click',$.proxy( _instance.onThumbImageSelect, _instance ));
              }
          }

          if(imgLoad.isComplete){
              imgLoad.off('always');
              //buoy.before($(dom).html());
              _isAjaxLocked = false;
          }
        });       
        
     },

    Manipulator.prototype.onThumbImageDrag = function(evt){
        return false;
    },

    Manipulator.prototype.onThumbImageSelect = function(evt){
      
      if(_isMoved){
         return;
      }


      var img = evt.currentTarget;
      if($(img).attr('src') == ""){
        return; 
      }

      this.dr_img.addClass("isLoaded").attr('src', $(img).attr('src')).removeAttr( "class" );
      var imgLoader  = imagesLoaded(this.dr_img);
      imgLoader.on("always", $.proxy( this.doImageLoadedHandle, this));
    }

    Manipulator.prototype.onReportRequest = function(evt){ alert("open");
        if($('.report-box').hasClass('hidden')){
            $('.report-box').removeClass('hidden');
        }else{
            $('.report-box').addClass('hidden');
        }
    }

    Manipulator.prototype.onReportConfirmClick = function(evt){
        $('.report-box').addClass('hidden');
        $('.report-response-box').removeClass('hidden');
        setTimeout(function(){
           $('.report-response-box').addClass('hidden');
        },2000);
    }

    Manipulator.prototype.onReportCancelClick = function(evt){
        $('.report-box').addClass('hidden');
    }

    Manipulator.prototype.onLikeclick = function(){
        var value = parseInt($('.dr-like-number').text());
        if( $('.dr-like').hasClass('icons-like') ){
            $('.dr-like').removeClass('icons-like');
            $('.dr-like').addClass('icons-unlike');
            $('.dr-like-number').text(value - 1);
        }else{
            $('.dr-like').removeClass('icons-unlike');
            $('.dr-like').addClass('icons-like');
            $('.dr-like-number').text(value + 1);
        }
    }

    Manipulator.prototype.onExifclick = function(){
        if( $('.dr-exif-content').hasClass('hidden') ){
            $('.dr-exif-content').removeClass('hidden');
            $('.dr-exif-string p').text('收起EXIF資訊');
        }else{
            $('.dr-exif-content').addClass('hidden');
            $('.dr-exif-string p').text('展開EXIF資訊');
        }
    }

    Manipulator.prototype.onReportRequest = function(evt){
        if($('.report-box').hasClass('hidden')){
            $('.report-box').removeClass('hidden');
        }else{
            $('.report-box').addClass('hidden');
        }
    }

    Manipulator.prototype.onReportConfirmClick = function(evt){
        $('.report-box').addClass('hidden');
        $('.report-response-box').removeClass('hidden');
        setTimeout(function(){
           $('.report-response-box').addClass('hidden');
        },2000);
    }

    Manipulator.prototype.onReportCancelClick = function(evt){
        $('.report-box').addClass('hidden');
    }


    Manipulator.prototype.onLikeclick = function(){
        var value = parseInt($('.dr-like-number').text());
        if( $('.dr-like').hasClass('icons-like') ){
            $('.dr-like').removeClass('icons-like');
            $('.dr-like').addClass('icons-unlike');
            $('.dr-like-number').text(value - 1);
        }else{
            $('.dr-like').removeClass('icons-unlike');
            $('.dr-like').addClass('icons-like');
            $('.dr-like-number').text(value + 1);
        }
    }

    Manipulator.prototype.onExifclick = function(){
        if( $('.dr-exif-content').hasClass('hidden') ){
            $('.dr-exif-content').removeClass('hidden');
            $('.dr-exif-string p').text('收起EXIF資訊');
        }else{
            $('.dr-exif-content').addClass('hidden');
            $('.dr-exif-string p').text('展開EXIF資訊');
        }
    }

    Manipulator.prototype.initGoogleMap = function(){
        var agence = new google.maps.LatLng(23.965142, 120.972905),
              parliament = new google.maps.LatLng(23.965142, 120.972905),
              image = '/cyberisland/images/darkroom/icn_anchor.png',
              marker,
              map;

        var mapOptions = {
          zoom:  8,
          disableDefaultUI: false,
          draggable:true,
          disableDoubleClickZoom:false,
          scrollwheel:true,
          mapTypeControl:false,
          streetViewControl:false,
          zoomControl: true,
          panControl: true,
          zoomControlOptions: {
              style:google.maps.ZoomControlStyle.SMALL
          },


          mapTypeId: google.maps.MapTypeId.ROADMAP,

          styles: [
              {
                featureType: 'landscape',
                elementType: 'all',
                stylers: [
                  { hue: '#ebebeb' },
                  { saturation: -100 },
                  { lightness: 29 },
                  { visibility: 'on' }
                ]
              },{
                featureType: 'landscape.man_made',
                elementType: 'all',
                stylers: [
                  { hue: '#ebebeb' },
                  { saturation: -100 },
                  { lightness: 29 },
                  { visibility: 'on' }
                ]
              },{
                featureType: 'landscape.natural',
                elementType: 'all',
                stylers: [
                  { hue: '#d6d6d6' },
                  { saturation: -100 },
                  { lightness: -12 },
                  { visibility: 'on' }
                ]
              },{
                featureType: 'road.local',
                elementType: 'all',
                stylers: [
                  { hue: '#ffffff' },
                  { saturation: -100 },
                  { lightness: 100 },
                  { visibility: 'on' }
                ]
              },{
                featureType: 'poi',
                elementType: 'all',
                stylers: [
                  { hue: '#cccccc' },
                  { saturation: -100 },
                  { lightness: 9 },
                  { visibility: 'on' }
                ]
              },{
                featureType: 'road.highway',
                elementType: 'all',
                stylers: [
                  { hue: '#f7f7f7' },
                  { saturation: -100 },
                  { lightness: 91 },
                  { visibility: 'on' }
                ]
              },{
                featureType: 'water',
                elementType: 'all',
                stylers: [
                  { hue: '#c8d6ed' },
                  { saturation: 10 },
                  { lightness: 40 },
                  { visibility: 'on' }
                ]
              }
            ],
            center: agence
        };

        map = new google.maps.Map($('.dr-goole-map')[0],mapOptions);

        marker = new google.maps.Marker({
          icon: image,
          map:map,
          draggable:false,
          animation: google.maps.Animation.DROP,
          position: parliament
        });
    }

    Manipulator.prototype.onResizeHandle = function(){
        bounds = {left:masker.offset().left, top:masker.offset().top, width:masker.width() , height:masker.height() };
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});
