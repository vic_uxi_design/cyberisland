window.Manipulator = (function() {

    var Manipulator = function(){
        this.lightbox_body = $('.lightbox-body');
        this.lightbox_wraper = $('.lightbox-wraper');
        this.lightbox_img = $('.lightbox-wraper img');
        this.init();
    };

    Manipulator.prototype.init = function() {
        this._org_url  = location.href
        this.calLightBox();
        this.setupEvents();
    };

    

    Manipulator.prototype.setupEvents = function(){
        $(window).on('resize', $.proxy( this.onResizeHandle, this ));
        $('.block-wraper').on('load', $.proxy( this.onloaded, this ));

        $('.lightbox-info').on('click',$.proxy( this.onRisingHandle, this ));
        
        
        
        if($('.lightbox-watercourse').width() > $('.lightbox-playbody').width()){
            $('.lightbox-btn-left').on('click',$.proxy( this.onMoveLeftHandle, this ));
            $('.lightbox-btn-right').on('click',$.proxy( this.onMoveRightHandle, this ));
        }else{
            $('.lightbox-btn-left').addClass('hidden');
            $('.lightbox-btn-right').addClass('hidden');
        }

        $('.lightbox-thumb').on('click',$.proxy( this.onThumbClickHandle, this ));

        if($.support.fullscreen){
            $('.lightbox-full').on('click',$.proxy( this.onFullScreenHandle, this ));
        }else{
            $('.lightbox-full').addClass('hidden');
        }

        $('.lightbox-report').on('click',$.proxy(this.onReportRequest, this) );
        
        $('.report-box-confirm').on('click',$.proxy( this.onReportConfirmClick, this ));
        $('.report-box-cancel').on('click',$.proxy( this.onReportCancelClick, this ));

        $('.dr-exif-info').on('click',$.proxy( this.onExifclick, this ));
        $('.dr-like').on('click',$.proxy( this.onLikeclick, this ));
        $('.lightbox-close').on('click',$.proxy( this.onCloseclick, this ));

        google.maps.event.addDomListener(window, 'resize', $.proxy( this.initGoogleMap, this ));
    };

    Manipulator.prototype.onloaded = function(){
       this.initGoogleMap();
    }

    Manipulator.prototype.onCloseclick = function(){
        $('.block-wraper').parent().addClass('hidden');
        $('.main').removeClass('hidden');
        window.history.pushState({"pageTitle":'歡迎光臨－數位島嶼'},
                                  'title',
                                  this._org_url);
    }

    Manipulator.prototype.onLikeclick = function(){
        var value = parseInt($('.dr-like-number').text());
        if( $('.dr-like').hasClass('icons-like') ){
            $('.dr-like').removeClass('icons-like');
            $('.dr-like').addClass('icons-unlike');
            $('.dr-like-number').text(value - 1);
        }else{
            $('.dr-like').removeClass('icons-unlike');
            $('.dr-like').addClass('icons-like');
            $('.dr-like-number').text(value + 1);
        }
    }

    Manipulator.prototype.onExifclick = function(){
        if( $('.dr-exif-content').hasClass('hidden') ){
            $('.dr-exif-content').removeClass('hidden');
            $('.dr-exif-string p').text('收起EXIF資訊');
        }else{
            $('.dr-exif-content').addClass('hidden');
            $('.dr-exif-string p').text('展開EXIF資訊');
        }
    }

    Manipulator.prototype.onReportRequest = function(evt){
        if($('.report-box').hasClass('hidden')){
            $('.report-box').removeClass('hidden');
        }else{
            $('.report-box').addClass('hidden');
        }
    }

    Manipulator.prototype.onReportConfirmClick = function(evt){
        $('.report-box').addClass('hidden');
        $('.report-response-box').removeClass('hidden');
        setTimeout(function(){
           $('.report-response-box').addClass('hidden');
        },2000);
    }

    Manipulator.prototype.onReportCancelClick = function(evt){
        $('.report-box').addClass('hidden');
    }

    Manipulator.prototype.onFullScreenHandle = function(evt){
         $('.lightbox').fullScreen();
         evt.preventDefault();
    }

    Manipulator.prototype.onThumbClickHandle = function(evt){
        var target = evt.currentTarget;
        var src = $(target).find('img').attr('src');
        this.lightbox_img.attr('src',src);
        this.lightbox_img.removeAttr( "style" );
        this.calLightBox();
    }

    Manipulator.prototype.onMoveLeftHandle = function(){
        var limit = parseInt($('.lightbox-watercourse').width()) - parseInt($('.lightbox-playbody').width());
        var offset = parseInt($('.lightbox-watercourse').css('margin-left'));

        console.log(limit + "--" + offset);

        if(offset - 140 >= -limit){
            offset = offset - 140;
            $('.lightbox-watercourse').animate({marginLeft: offset} , 1000, 'easeInOutCubic');
        }
    }

    Manipulator.prototype.onMoveRightHandle = function(){
        var offset = parseInt($('.lightbox-watercourse').css('margin-left'));
        if(offset + 140 <= 0){
            offset = offset + 140;
            $('.lightbox-watercourse').animate({marginLeft: offset} , 1000, 'easeInOutCubic');
        }
    }

    Manipulator.prototype.onRisingHandle = function(){
        if($('.lightbox-footer').hasClass('active'))
        {
            $('.lightbox-footer').removeClass('active')
                                 .stop()
                                 .animate({ marginTop: "0" }, 1000, 'easeInOutCubic');
            return;
        }

        $('.lightbox-footer').addClass('active')
                             .stop()
                             .animate({ marginTop: "-178px" }, 1000, 'easeInOutCubic');

    }
   
    Manipulator.prototype.calLightBox = function(){
        console.log('enter cal linght box')
        var limit = 588;
        var clientHeight = $(window).innerHeight();
        var _light_w = this.lightbox_wraper.width();
        var _light_h = clientHeight - 220;
        
        if(_light_h < limit) {
            _light_h = limit;
        }

        this.lightbox_wraper.css('height', _light_h);

        var _org_w, _org_h, _new_w, _new_h, ratio_1, ratio_2;
        _org_w =  this.lightbox_img.width();
        _org_h =  this.lightbox_img.height();


        console.log( _org_w + 'and' + _org_h);
        
        ratio_1 = _org_h / _org_w;
        ratio_2 = _org_w / _org_h;

        if(_org_w > _org_h){ 
            _new_w = _light_w;
            _new_h = _new_w * ratio_1;
        }else if(_org_w < _org_h){
            _new_h = _light_h;
            _new_w = _new_h * ratio_2;
        }else{
            _new_h = _light_h;
            _new_w = _new_h * ratio_2;
        }

        var offset_x, offset_y;
        offset_x = (_light_w - _new_w) / 2;
        offset_y = (_light_h - _new_h) / 2;


        this.lightbox_img.width(_new_w);
        this.lightbox_img.height(_new_h);


        if(offset_x != 0){
            this.lightbox_img.css('margin-left', offset_x + "px");
        }        

        if(offset_y != 0){
            this.lightbox_img.css('margin-top', offset_y  + "px");
        }
    };

    Manipulator.prototype.onResizeHandle = function(){
        this.lightbox_img.removeAttr( "style" );
        this.calLightBox();
    };

    Manipulator.prototype.initGoogleMap = function(){ 
        var agence = new google.maps.LatLng(23.965142, 120.972905),
              parliament = new google.maps.LatLng(23.965142, 120.972905),
              image = '/cyberisland/images/darkroom/icn_anchor.png',
              marker,
              map;

        var mapOptions = {
          zoom:  8,
          disableDefaultUI: false,
          draggable:true,
          disableDoubleClickZoom:false,
          scrollwheel:true,
          mapTypeControl:false,
          streetViewControl:false,
          zoomControl: true,
          panControl: true,
          zoomControlOptions: {
              style:google.maps.ZoomControlStyle.SMALL
          },


          mapTypeId: google.maps.MapTypeId.ROADMAP,

          styles: [
              {
                "stylers": [
                  { "saturation": -100 }
                ]
              },{
                "featureType": "poi",
                "stylers": [
                  { "visibility": "on" }
                ]
              },{
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                  { "visibility": "on" },
                  { "lightness": 100 }
                ]
              },{
                "elementType": "labels.text.stroke",
                "stylers": [
                  { "lightness": 100 }
                ]
              },{
                "featureType": "transit",
                "stylers": [
                  { "visibility": "on" },
                  { "gamma": 0.01 }
                ]
              },{
                "featureType": "water",
                "stylers": [
                  { "visibility": "off" }
                ]
              },{
                "elementType": "labels.icon",
                "stylers": [
                  { "visibility": "off" }
                ]
              },{
                "featureType": "road.arterial",
                "elementType": "geometry.stroke",
                "stylers": [
                  { "lightness": -23 }
                ]
              },{
                "featureType": "poi",
                "stylers": [
                  { "visibility": "simplified" },
                  { "lightness": 57 }
                ]
              },{
                "elementType": "geometry.stroke",
                "stylers": [
                  { "lightness": 44 },
                  { "visibility": "simplified" }
                ]
              },{
                "featureType": "road.local",
                "elementType": "geometry.fill",
                "stylers": [
                  { "lightness": 100 }
                ]
              },{
                "featureType": "transit.station",
                "stylers": [
                  { "visibility": "on" },
                  { "gamma": 9.91 }
                ]
              },{
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                  { "lightness": 100 }
                ]
              },{
                "featureType": "transit.line",
                "elementType": "geometry.fill",
                "stylers": [
                  { "lightness": 100 }
                ]
              },{
              }
            ],
            center: agence
        };

        map = new google.maps.Map($('.dr-goole-map')[0],mapOptions);

        marker = new google.maps.Marker({
          icon: image,
          map:map,
          draggable:false,
          animation: google.maps.Animation.DROP,
          position: parliament
        });
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});