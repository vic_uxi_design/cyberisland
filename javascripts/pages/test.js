window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function(){
        //this.detectDevice();
        this.setupEvents();
    };

    Manipulator.prototype.detectDevice = function(){
        var _w = $(window).innerWidth();
        var _h = $(window).innerHeight();



        $('.device p').text("device w : " +  _w + " device h : " + _h);
    }

    

    Manipulator.prototype.setupEvents = function(){ 

        $('.device').on('click', $.proxy( this.onDiveceClick, this ))
        //$('.device').on('mousedown', $.proxy( this.onWinMouseDown, this ));
        /*
        $('.box').on('mousedown', $.proxy(this.onMouseDown, this));
        $('.box').on('touchstart', $.proxy(this.onTouchStart, this));
        //obj.on('dragenter', function (e) 
        //
        $('#dragandrophandler').on('dragenter', $.proxy(this.dragEnterHandle, this));
        $('#dragandrophandler').on('dragover', $.proxy(this.dragOverHandle, this));
        $('#dragandrophandler').on('drop', $.proxy(this.drogHandle, this));

        $(document).on('dragenter', $.proxy(this.ondocDragEnterHandle, this));
        $(document).on('dragover',  $.proxy(this.ondocDragOverHandle, this));
        $(document).on('drop', $.proxy(this.ondocDropHandle, this));

         $(document).on('touchmove', $.proxy( this.onWinTouchMove, this ));*/
    };

    Manipulator.prototype.onDiveceClick = function(e){
        var target = e.currentTarget;
        var len = $(target).find('img').length;
        var active = $(target).find('img.active');
        var next = (active.index() == len-1) ? $(target).find('img:first-child') : active.next();
        active.removeClass('active');
        next.addClass('active');
    }

    Manipulator.prototype.onWinMouseDown = function(e){
        var value=e.button;
        if(value==2||value==3){
            alert('点击的是鼠标右键');
        }else{
            alert('点击的是鼠标左键');
        }
    }

    Manipulator.prototype.onWinTouchMove = function(evt){
        return false;
    }

    Manipulator.prototype.ondocDragEnterHandle = function(e){
        e.stopPropagation();
        e.preventDefault();
    }

    Manipulator.prototype.ondocDragOverHandle = function(e){
        e.stopPropagation();
        e.preventDefault();
       // obj.css('border', '2px dotted #0B85A1');
    }

    Manipulator.prototype.ondocDropHandle = function(e){
        e.stopPropagation();
        e.preventDefault();
    }

    Manipulator.prototype.dragEnterHandle = function(e){
        var target = e.currentTarget;
        e.stopPropagation();
        e.preventDefault();
        $(target).css('border', '2px solid #0B85A1');
    }

    Manipulator.prototype.dragOverHandle = function(e){
        var target = e.currentTarget;
        e.stopPropagation();
        e.preventDefault();
    }

    Manipulator.prototype.drogHandle = function(e){
        var target = e.currentTarget;
        e.preventDefault();
        $(target).css('border', '2px dotted #0B85A1');
        var files = e.originalEvent.dataTransfer.files;
        this.handleFileUpload(files, target);
    }

    Manipulator.prototype.handleFileUpload = function(files, obj){
        for (var i = 0; i < files.length; i++)
       {
            var imageFiles = $.map(files, function (f, i) {
                //只留下type為image/*者，例如: image/gif, image/jpeg, image/png...
                return f.type.indexOf("image") == 0 ? f : null;
            });

           $.each(imageFiles, function (i, file) {
                //使用File API讀取圖檔內容轉為DataUri
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $(new Image())
                    img.attr('name', file.name).attr('src', e.target.result); 
                    $('.files').append(img);    
                }
                reader.readAsDataURL(file);
            });
        }
    }

    Manipulator.prototype.onMouseDown = function(evt){
        $('.box').off('mousedown', $.proxy(this.onMouseDown, this));
        $('.box').on('mousemove', $.proxy(this.onMouseMove, this));
        $('.box').on('mouseup', $.proxy(this.onMouseUp, this));


        console.log('onMouseDown');
        $('H6').text('on Mouse Dowm');


    }

    Manipulator.prototype.onMouseMove = function(evt){
        console.log('onMouseMove');
        $('H6').text('on Mouse Move');
    }

    Manipulator.prototype.onMouseUp = function(evt){
        $('.box').off('mousemove', $.proxy(this.onMouseMove, this));
        $('.box').off('mouseup', $.proxy(this.onMouseUp, this));
        $('.box').on('mousedown', $.proxy(this.onMouseDown, this));

        console.log('onMouseUp');
        $('H6').text('on Mouse Up');
    }

    Manipulator.prototype.onTouchStart = function(evt){
        $('.box').off('touchstart', $.proxy(this.onTouchStart, this));
        $('.box').on('touchmove', $.proxy(this.onTouchMove, this));
        $('.box').on('touchend', $.proxy(this.onTouchEnd, this));
        var touches = evt.originalEvent.touches[0];
       console.log(touches.pageX);
       console.log(touches.pageY);

        console.log('onTouchStart');
        $('H6').text('on Touch Start');
    }

    Manipulator.prototype.onTouchMove = function(evt){
       console.log('onTouchMove');
       $('H6').text('on Touch Move');
    }

    Manipulator.prototype.onTouchEnd = function(evt){
        $('.box').off('touchmove', $.proxy(this.onTouchMove, this));
        $('.box').off('touchend', $.proxy(this.onTouchEnd, this));
        $('.box').on('touchstart', $.proxy(this.onTouchStart, this));
        
        console.log('onTouchEnd');
        $('H6').text('on Touch End');
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});