window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.$grid = $('.grid');
        this.$grid.imagesLoaded($.proxy( this.initModuleGrid, this));
        this.initCarousel();
        this.setupEvents();
        $('.block-ui').load("/cyberisland/html/F/F01.html");
    };

    Manipulator.prototype.setupEvents = function(){
        $(window).on('scroll', $.proxy( this.onScrollHandel, this ));
        $(window).on('resize', $.proxy( this.onResizeHandle, this ));
        
        $('.navbar-header .keyword').on('focus', $.proxy( this.onTextFocus, this ));
        $('.navbar-header .keyword').on('blur', $.proxy( this.onTextBlur, this ));
        $('.navbar-header .keyword').on('keypress', $.proxy( this.onTextKeypress, this ));
    };

    
    Manipulator.prototype.onTextKeypress = function(evt){
        if (evt.which == 13 ) {
            var keywrod = $('.navbar-header .keyword').val();
            if(keywrod == '站內搜尋' || keywrod == ''){
                $('.navbar-header .keyword').blur();
            }
            window.location.href = '/cyberisland/html/A/search.html'; 
            evt.preventDefault();
        }
    };

    Manipulator.prototype.onTextFocus = function(evt){
        //$('.keyword').css('width', '120px');
        if($('.navbar-header .keyword').val() == '站內搜尋'){
            $('.navbar-header .keyword').val("");
        }
        $('.navbar-header .keyword').stop().animate({ width: "+=77px" }, 1000);
        $('.navbar-header .navbar-right').stop().animate({ width: "+=77px" }, 1000);
        $('.navbar-header .keyword').parent().next().stop().animate({ opacity: "0" }, 1000);
    };

    Manipulator.prototype.onTextBlur = function(evt){
        var _w = 338;
        if($(window).width() <= 1315){
            _w = 263;
        }

        $('.navbar-header .keyword').val('站內搜尋'); 
        $('.navbar-header .keyword').stop().animate({ width: "68px" }, 1000);
        $('.navbar-header .navbar-right').stop().animate({ width: _w }, 1000);
        $('.navbar-header .keyword').parent().next().stop().animate({ opacity: "1" }, 1000);
    };

    Manipulator.prototype.onResizeHandle = function(evt){
        

    };

    Manipulator.prototype.onScrollHandel = function(evt){
        var distanceY = $(document).scrollTop(),
            shrinkOn = 300,
            container = $(".navbar-header");
        if (distanceY > shrinkOn) {
            container.addClass("navbar-header_small");
            container.parent().addClass("navbar-header_small");
        } else {
            if(container.hasClass("navbar-header_small")){ 
                container.removeClass("navbar-header_small"); 
                container.parent().removeClass("navbar-header_small");
            }
        }

    };
    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});