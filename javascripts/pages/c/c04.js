c02.jswindow.Manipulator = (function() {

    var _count;

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        var imgLoad  = imagesLoaded($('.col-pics'));
        imgLoad.on('always', $.proxy(this.doImageLoadedHandle, this));
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.grid-sort-control').on('change', $.proxy( this.onChangeHandel, this ));
    };

    Manipulator.prototype.doImageLoadedHandle = function(imgLoad) {
        for(var i=0, len = imgLoad.images.length; i< len; i++){
            var image = imgLoad.images[i];
            if(image.isLoaded){
                $(image.img).parent().parent().removeClass('isLoaded');
            }
        }

        if(imgLoad.isComplete){
            imgLoad.off('always');
        }
    };

    Manipulator.prototype.onChangeHandel = function(evt){
        var val = $('.grid-sort-control option:selected').val();
        if(val == '-1'){

            $.ajax({
                url: "/cyberisland/html/partials/popSearchPage.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
            }).success(  $.proxy( this.openSerachPage, this ))
            .fail( $.proxy( this.onAjaxFail, this ));
            return;   
        }
    };


    Manipulator.prototype.openSerachPage = function(html){
         $.vrModuleBlockUI({html: html});
    }

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
       console.log("Request failed: " + textStatus )
    }
    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});