window.Manipulator = (function() {

    var _count, _instance;

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        _instance = this;
        _count = 0;
        var imgLoader1 = imagesLoaded(document.querySelectorAll(".event"));;
        var imgLoader2 = imagesLoaded(document.querySelectorAll(".col-sm-pics"));
        imgLoader1.on("always", $.proxy(this.doLeftSideImageLoaded ,this));
        imgLoader2.on("always", $.proxy(this.doRightSideImageLoaded ,this));

        $('.evessay .col-sm-title2').each(function(){
            $(this).vrModuleDot({
                elemHeight: 80,
                lineHeight: 30,
                threshold_line: 2,
                threshold_word_per_line: 12
            });

        });


        $('.evessay .col-sm-context').each(function(){
            $(this).vrModuleDot({
                elemHeight: 115,
                lineHeight: 24,
                threshold_line: 4,
                threshold_word_per_line: 18
            });
        });

        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.grid-sort-control').on('change', $.proxy( this.onChangeHandel, this ));
        $('.icons-gototop').on('click', $.proxy( this.onSrolltotop, this ));
        $('.btn-loadmore').on('click', $.proxy( this.loadMoreFunction, this ));
    };

    Manipulator.prototype.doLeftSideImageLoaded = function(imgLoad){
        for(var i=0, len = imgLoad.images.length; i< len; i++){
            var image = imgLoad.images[i];
            if(image.isLoaded){
                $(image.img).parent().parent().removeClass('isLoaded');
            }
        }

        if(imgLoad.isComplete){
            imgLoad.off('always');
        }
    }

    Manipulator.prototype.doRightSideImageLoaded = function(imgLoad){
        for(var i=0, len = imgLoad.images.length; i< len; i++){
            var image = imgLoad.images[i];
            if(image.isLoaded){
                $(image.img).parent().removeClass('isLoaded');
            }
        }

        if(imgLoad.isComplete){
            imgLoad.off('always');
        }
    }

    Manipulator.prototype.onSrolltotop = function(evt){
        $('html, body').stop().animate({ scrollTop: '0' }, 600 , 'easeInOutCubic');
    }

    Manipulator.prototype.loadMoreFunction = function(){
        $('.btn-loadmore').off('click', $.proxy( this.loadMoreFunction, this ));
        $(document).off('scroll', $.proxy(this.onContainerScroll ,this));

        if(_count == 0){
            $('.btn-loadmore').addClass("icons-loading").removeClass("icons-btn-load");
        }

        var evcate = document.querySelector('.evcate');
        var evessay = document.querySelector('.evessay');

        var lh =  evcate.clientHeight;
        var rh =  evessay.clientHeight;

        var lLastChildren = evcate.children;
        var rLastChildren = evessay.children;

        var lLastChild = lLastChildren[lLastChildren.length - 1];
        var rLastChild = rLastChildren[rLastChildren.length - 1];

        var r1h, r3h, l1h, l3h, min, index;

        r1h = 60 + rLastChild.clientHeight;
        l1h = lLastChild.clientHeight;

        r3h = 3 * r1h;
        l3h = 3 * l1h;

        var diffArray = [
            [ Math.abs((lh + l3h) - (rh + r3h))            , 'apiGetC01Content3x3' ],
            [ Math.abs((lh + l3h) - (rh + r3h + r1h))      , 'apiGetC01Content3x4' ],
            [ Math.abs((lh + l3h + l1h) - (rh + r3h))      , 'apiGetC01Content4x3' ],
            [ Math.abs((lh + l3h + l1h) - (rh + r3h + r1h)), 'apiGetC01Content4x4' ]
        ];

        min = diffArray[0][0];
        index = 0;

        for(var i= 1, len = 4; i< len; i++ ){
            if(min > diffArray[i][0]){
                min = diffArray[i][0];
                index = i;
            }
        }

        $.ajax({
            url: "/cyberisland/json/" + diffArray[index][1] +".json",
            type: "GET",
            data: { id : '0' },
            dataType: 'html'
        }) .done(function( data ) {
            _instance.appendContent(data);
        });

    };

    Manipulator.prototype.appendContent = function(json){
        var result = $.parseJSON(json);
        console.log(result);

        _count = _count + 1;

        $('#tmpl1').tmpl(result.evcate).appendTo('.evcate');
        $('#tmpl2').tmpl(result.evessay).appendTo('.evessay');

        var imgLoader1 = imagesLoaded(document.querySelectorAll(".event"));;
        var imgLoader2 = imagesLoaded(document.querySelectorAll(".col-sm-pics"));
        imgLoader1.on("always", $.proxy(this.doLeftSideImageLoaded ,this));
        imgLoader2.on("always", $.proxy(this.doRightSideImageLoaded ,this));

        $('.evessay .col-sm-title2').each(function(){
            $(this).vrModuleDot({
                elemHeight: 80,
                lineHeight: 30,
                threshold_line: 2,
                threshold_word_per_line: 12
            });

        });


        $('.evessay .col-sm-context').each(function(){
            $(this).vrModuleDot({
                elemHeight: 115,
                lineHeight: 24,
                threshold_line: 4,
                threshold_word_per_line: 18
            });
        });

        if(_count == 1){
            $(document).on('scroll', $.proxy(this.onContainerScroll ,this));
        }else if(_count == 3){
            $('.btn-loadmore').on('click', $.proxy( this.loadMoreFunction, this ))
                .addClass("icons-btn-load").removeClass("icons-loading");

            $(document).off('scroll', $.proxy(this.onContainerScroll ,this));
            _count = 0;
        }else{
            $(document).on('scroll', $.proxy(this.onContainerScroll ,this));
        }
    }

    Manipulator.prototype.onContainerScroll = function(e){
        var target = e.currentTarget;
        var inner = document.querySelector(".container.main");
        var outer = document.querySelector("body");
        var h1 = inner.offsetHeight;
        var h2 = outer.clientHeight;
        var h3 = $(target).scrollTop();
        if((h1 - h2) - h3 <= 0){
            this.loadMoreFunction();
        }
    };

    Manipulator.prototype.onChangeHandel = function(evt){
        var val = $('.grid-sort-control option:selected').val();
        if(val == '-1'){

            $.ajax({
                url: "/cyberisland/html/partials/popSearchPage.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
            }).success(  $.proxy( this.openSerachPage, this ))
            .fail( $.proxy( this.onAjaxFail, this ));
            return;   
        }
    };


    Manipulator.prototype.openSerachPage = function(html){
         $.vrModuleBlockUI({html: html});
    }

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
       //console.log("Request failed: " + textStatus );
        console.log(jqXHR);
    }
    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});