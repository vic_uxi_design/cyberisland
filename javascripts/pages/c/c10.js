window.Manipulator = (function() {

    var _count;

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        var imgLoad  = imagesLoaded($('.col-pics'));
        imgLoad.on('always', $.proxy(this.doImageLoadedHandle, this));
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){

    };

    Manipulator.prototype.doImageLoadedHandle = function(imgLoad) {
        for(var i=0, len = imgLoad.images.length; i< len; i++){
            var image = imgLoad.images[i];
            if(image.isLoaded){
                $(image.img).parent().removeClass('isLoaded');
            }
        }

        if(imgLoad.isComplete){
            imgLoad.off('always');
        }
    };

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});