window.Manipulator = (function() {

    var _count, _instance;

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        _instance = this;
        _count = 0;
        var imgLoader = imagesLoaded(document.querySelectorAll(".col-pics"));;
        imgLoader.on("always", $.proxy(this.doImageLoaded ,this));

        $('.col-pics .mask').each(function(){
            $(this).vrModuleDot({
                elemHeight: 80,
                lineHeight: 30,
                threshold_line: 2,
                threshold_word_per_line: 12
            });

        });

        this.setupEvents();
    };

    Manipulator.prototype.doImageLoaded = function(imgLoad){
        for(var i=0, len = imgLoad.images.length; i< len; i++){
            var image = imgLoad.images[i];
            if(image.isLoaded){
                $(image.img).parent().removeClass('isLoaded');
            }
        }

        if(imgLoad.isComplete){
            imgLoad.off('always');
        }
    }

    Manipulator.prototype.setupEvents = function(){
    };

    

    
    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});