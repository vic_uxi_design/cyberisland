window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function(){
        this.header = $('.essay .header');
        this.banner = $('.essay .banner');
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        //$(window).on('resize', $.proxy( this.onResizeHandle, this ));
        $('.scrollToTop').on('click', $.proxy( this.doScrollToTop, this ));
    };


    Manipulator.prototype.onResizeHandle = function(){
        //this.calLayout();
    }

    Manipulator.prototype.doScrollToTop = function(){
        $('html, body').stop().animate({ scrollTop: '0' }, 600 , 'easeInOutCubic');
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});