window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        //$('.cycle img').imagesLoaded($.proxy( this.initCalAvarta, this));
        this.setupEvents();
    };

     Manipulator.prototype.initCalAvarta = function(){
        var cycle = $('.cycle');
        var avartar = cycle.find('img');
        var _cycle_w = cycle.width();
        var _cycle_h = cycle.height();
        var _avartar_w = avartar.width();
        var _avartar_h = avartar.height();

        var offset_x = (_cycle_w - _avartar_w) / 2;
        var offset_y = (_cycle_h - _avartar_h) / 2;

        avartar.css('margin-left',offset_x);
        avartar.css('margin-top',offset_y);
    };

    Manipulator.prototype.setupEvents = function(){
        $('.avatar').on('click', $.proxy( this.onAvatarClicked, this ));
       // $(window).on('resize', $.proxy( this.onResizeHandle, this ));
    };

    Manipulator.prototype.onAvatarClicked = function(){
        $.ajax({
                url: "/cyberisland/html/partials/proinfo.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.getProInfoSuccess, this ))
        .fail( $.proxy( this.getProInfofail, this ));
    }
 
    Manipulator.prototype.getProInfoSuccess = function(html){
        $.vrModuleBlockUI({html: html, position: "absolute"});
    }

    Manipulator.prototype.getProInfofail = function(jqXHR, textStatus){
        alert("onAjaxFail");
        console.log("Request failed: " + textStatus);
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});