window.Manipulator = (function() {

    var _count;
    var _instance;

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        _count = 0;
        _instance = this;

        //$('.cycle img').imagesLoaded($.proxy( this.initCalAvarta, this));
        
        this.$grid = $('.grid');
        this.$grid.isotope({
            itemSelector: '.col-1',
            resizable: true,
            masonry: {
                columnWidth: 300,
                gutter: 30
            },
            getSortData: {
                date_created: function(elem){
                                return Date.parse($(elem).find('.createdate').text());
                                
                            }, 
                like_number: function(elem){
                                return parseInt($(elem).find('.likenum').text(), 10);
                            }
            }
        });

        var imgLoad  = imagesLoaded(this.$grid);
        imgLoad.on('always', $.proxy(this.doImageLoadedHandle, this));

        this.setupEvents();
    };

    Manipulator.prototype.doImageLoadedHandle = function(imgLoad) {
      for(var i=0, len = imgLoad.images.length; i< len; i++){
          var image = imgLoad.images[i];
          if(image.isLoaded){
              var parent = $(image.img).parent().parent().parent();
              $(parent).find('.col-sm-pics').removeClass('isLoaded');
              $(parent).find('.col-sm-context').vrModuleDot({});
          }
       }

      if(imgLoad.isComplete){
          imgLoad.off('always');
          this.$grid.isotope('layout');
      }
    };

    Manipulator.prototype.initCalAvarta = function(){
        var cycle = $('.cycle');
        var avartar = cycle.find('img');
        var _cycle_w = cycle.width();
        var _cycle_h = cycle.height();
        var _avartar_w = avartar.width();
        var _avartar_h = avartar.height();

        var offset_x = (_cycle_w - _avartar_w) / 2;
        var offset_y = (_cycle_h - _avartar_h) / 2;

        avartar.css('margin-left',offset_x);
        avartar.css('margin-top',offset_y);
    };

    Manipulator.prototype.setupEvents = function(){
        $(window).on('resize', $.proxy( this.onResizeHandle, this ));
        $(window).on('orientationchange', $.proxy( this.onOrientationChange, this ));
        $('.btn-loadmore').on('click', $.proxy( this.onBntLoadClicked, this ));
        $('.col-sm-like').on('click', $.proxy( this.onLikeClicked, this ));

        $('.grid-sort-control').on('change', $.proxy( this.onSortChange, this ));
        $('.gototop').on('click', $.proxy( this.onSrolltotop, this ));
        $('.avatar').on('click', $.proxy( this.onAvatarClicked, this ));
    };

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
       console.log("Request failed: " + textStatus )
    }

    Manipulator.prototype.onAvatarClicked = function(){
        $.ajax({
                url: "/cyberisland/html/partials/proinfo.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.getProInfoSuccess, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.getProInfoSuccess = function(html){
       $.vrModuleBlockUI({html: html, position: "absolute"});
    }
 

    Manipulator.prototype.showBlockUI = function(html){
        $.vrModuleBlockUI({html: html});
    }

    Manipulator.prototype.onLikeClicked = function(evt){
        var target = evt.currentTarget;
        var isLogin = ($(target).parent().index() == 1);
        if(!isLogin){
            $.ajax({
                url: "/cyberisland/html/partials/login.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
            }).success(  $.proxy( this.showBlockUI, this ))
            .fail( $.proxy( this.onAjaxFail, this ));
            return;
        }

        var target = evt.currentTarget;
        var line_number = 0;
        if($(target).hasClass("icons-like")){
            $(target).removeClass("icons-like").addClass("icons-unlike");
            var line_number = parseInt($(target).find('p').text()) - 1;
        }else{
            $(target).removeClass("icons-unlike").addClass("icons-like");
            var line_number = parseInt($(target).find('p').text()) + 1;
        }

        $(target).find('.likenum').text( line_number);
        this.$grid.isotope( 'updateSortData', this.$grid.children() );
        
    };

    Manipulator.prototype.onBtnConfirmClick = function(evt){
        var target = evt.currentTarget;
        $('.btn-confirm').off('click', $.proxy( this.onBtnConfirmClick, this ));
        $('.btn-cancel').off('click', $.proxy( this.onBtnCancelClick, this ));
        $('.pop-alert').parent().addClass('hidden');
    }

    Manipulator.prototype.onBtnCancelClick = function(evt){
        var target = evt.currentTarget;
        $('.btn-confirm').off('click', $.proxy( this.onBtnConfirmClick, this ));
        $('.btn-cancel').off('click', $.proxy( this.onBtnCancelClick, this ));
        $('.pop-alert').parent().addClass('hidden');
    }

    Manipulator.prototype.onSrolltotop = function(evt){
        $('html, body').stop().animate({ scrollTop: '0' }, 600 , 'easeInOutCubic');
    }
    
    // just a append element example
    Manipulator.prototype.onBntLoadClicked = function(evt){
        $('.btn-loadmore').off('click', $.proxy( this.onBntLoadClicked, this ));
        $(document).off('scroll', $.proxy(this.onContainerScroll ,this));
        $.ajax({
                url: "/cyberisland/html/partials/ajaxLoadPics.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.appendGriditem, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.appendGriditem = function($html){
        _count = _count + 1;
        var dom = $('<div>' + $html +'</div>');
        var grid = this.$grid;
        dom.imagesLoaded( function() {
            dom.find('.col-1').each(function(){
                var elem = $(this);
                grid.append( elem ).isotope('appended', elem );
            });

            if(_count == 1){
                $('.btn-loadmore').off('click', $.proxy( _instance.onBntLoadClicked, _instance ))
                                  .addClass("icons-loading").removeClass("icons-btn-load");

                $(document).on('scroll', $.proxy(_instance.onContainerScroll ,_instance));
            }else if(_count == 3){
                $('.btn-loadmore').on('click', $.proxy( _instance.onBntLoadClicked, _instance ))
                                  .addClass("icons-btn-load").removeClass("icons-loading");
                                  
                $(document).off('scroll', $.proxy(_instance.onContainerScroll ,_instance));
                 _count = 0;
            }else{
                 $(document).on('scroll', $.proxy(_instance.onContainerScroll ,_instance));
            }
        });

        grid.isotope('layout');
    }

    Manipulator.prototype.onContainerScroll = function(e){
        var target = e.currentTarget;
        var inner = document.querySelector(".container.main");
        var outer = document.querySelector("body");
        var h1 = inner.offsetHeight;
        var h2 = outer.clientHeight;
        var h3 = $(target).scrollTop();
        if((h1 - h2) - h3 <= 0){
            this.onBntLoadClicked();
        }
    };

    Manipulator.prototype.onSortChange = function(evt){
        var value = evt.target.value;
        this.$grid.isotope({ sortBy: value, sortAscending : false});
    };

    Manipulator.prototype.onResizeHandle = function(){
        this.$grid.isotope('layout');
    }

    Manipulator.prototype.onOrientationChange = function(){
        this.$grid.isotope('layout');
    }

    Manipulator.prototype.initComponentGrid = function(){
        this.$grid.isotope({
            itemSelector: '.col-1',
            resizable: true,
            masonry: {
                columnWidth: 300,
                gutter: 30
            },
            getSortData: {
                date_created: function(elem){
                                return Date.parse($(elem).find('.createdate').text());
                                
                            }, 
                like_number: function(elem){
                                return parseInt($(elem).find('.likenum').text(), 10);
                            }
            }
        });
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});