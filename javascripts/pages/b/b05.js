window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.header = $('.header');
        this.banner = $('.banner');
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $(window).on('resize', $.proxy( this.onResizeHandle, this ));
    };

    Manipulator.prototype.onResizeHandle = function(){

    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});