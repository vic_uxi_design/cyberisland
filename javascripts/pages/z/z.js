window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $(window).on('resize', $.proxy( this.onResizeHandle, this ));
    };

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});