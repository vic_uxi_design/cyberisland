window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.col-bk-album-create').on('click', $.proxy(this.openEditPicsPanel, this));
        $('.icon-btn-small-edit-d').on('click', $.proxy(this.openEditPicsPanel, this)); 
        $('.col-sm-title').on('click', $.proxy(this.openTitleEditor, this));
    };

    Manipulator.prototype.openTitleEditor = function(evt){
        var target = evt.currentTarget;
        
        var p =  $(target).find('p');
        var editor =  $(target).find('input[type="text"]');
        p.addClass('hidden');
        
        editor.removeClass('hidden');
        editor.trigger('focus');
        editor.val(p.text());
        editor.on('blur', $.proxy(this.onTextFieldBlur, this ));
    }

    Manipulator.prototype.onTextFieldBlur = function(evt){
        var target = evt.currentTarget;
        var parent = $(target).parent();
        var str = $.trim($(target).val());
        if(str == ""){ str = "empty"}

        $(target).off('blur', $.proxy(this.onTextFieldBlur, this ));
        $(parent).find('p').text(str).removeClass('hidden');
        $(target).addClass('hidden');
    }

    Manipulator.prototype.openEditPicsPanel = function(evt){
        var request = $.ajax({
                url: "/cyberisland/html/partials/editCollection.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).done(  $.proxy( this.donCallEditPanel, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    };

    Manipulator.prototype.donCallEditPanel = function(html){
        $.vrModuleBlockUI({html: html, position: 'absolute'});
    };

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
        console.log("Request failed: " + textStatus );
    }


    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});