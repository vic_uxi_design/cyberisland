window.Manipulator = (function() {
    var _instance_ = null;

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        _instance_ = this;
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.icon-btn-small-edit').on('click', $.proxy(this.openEditPicsPanel, this)); 
        $('.icon-bk-btn-move, .icon-btn-small-move').on('click', $.proxy(this.openSelAlbumPanel, this)); 
        $('.icon-bk-btn-select').on('click', this.doSelectAllItem); 
        $('.icon-bk-btn-delete').on('click', $.proxy(this.onConfirmDelete, this));
        $('.icon-btn-small-delete').on('click', $.proxy(this.onConfirmDelete, this)); 
        $('.icon-bk-btn-rotate').on('click', this.doRotateAllItem);
        $('.icon-btn-small-rotate').on('click', this.doRotateItem);
        $('.icon-chk').on('click', this.doCheckItem); 

        $('.col-sm-title').on('click', $.proxy(this.openTitleEditor, this));
        $('.col-sm-meta').on('click', $.proxy(this.openMetaEditor, this));
    };

    Manipulator.prototype.openTitleEditor = function(evt){
        var target = evt.currentTarget;
        
        var p =  $(target).find('p');
        var editor =  $(target).find('input[type="text"]');
        p.addClass('hidden');
        
        editor.on('blur', $.proxy(this.onTextFieldBlur, this ));
        editor.removeClass('hidden');
        editor.trigger('focus');
        editor.val($.trim(p.text()));
    }

    Manipulator.prototype.onTextFieldBlur = function(evt){
        var target = evt.currentTarget;
        var parent = $(target).parent();
        var str = $.trim($(target).val());
        if(str == ""){ str = "empty"}

        $(target).off('blur', $.proxy(this.onTextFieldBlur, this ));
        $(parent).find('p').text(str).removeClass('hidden');
        $(target).addClass('hidden');
    }

    Manipulator.prototype.openMetaEditor = function(evt){
        var target = evt.currentTarget;
        var p =  $(target).find('p');
        var editor =  $(target).find('textarea');
        p.addClass('hidden');

        editor.on('blur', $.proxy(this.onTextAreaBlur, this ));
        editor.removeClass('hidden');
        editor.trigger('focus');
    }

    Manipulator.prototype.onTextAreaBlur = function(evt){
        var target = evt.currentTarget;
        var parent = $(target).parent();
        var str = $.trim($(target).val());
        if(str == ""){ str = "empty"}

        var divH = 63;
        var $p =  $(parent).find('p').eq(0);
        $p.text(str);
        if($p.outerHeight() > divH) {
            $p.text($p.text().replace(/(\s)*([a-zA-Z0-9]+|\W)(\.\.\.)?$/, "..."));
        }; 

        $(target).off('blur', $.proxy(this.onTextAreaBlur, this ));
        $(parent).find('p').removeClass('hidden');
        $(target).addClass('hidden');
    }

    Manipulator.prototype.doCheckItem = function(evt){
        var target = evt.currentTarget;
        if($(target).hasClass('active')){
            $(target).removeClass('active');
            if($('.icon-bk-btn-select').hasClass('active')){
                $('.icon-bk-btn-select').removeClass('active');
            }
        }else{
            $(target).addClass('active');
        }
    }

    Manipulator.prototype.doSelectAllItem = function(evt){
        var target = evt.currentTarget;
        if($(target).hasClass('active')){
            $(target).removeClass('active');
            $('.icon-chk').removeClass('active');
        }else{
            $(target).addClass('active');
            $('.icon-chk').addClass('active');
        }
    };


    Manipulator.prototype.onConfirmDelete = function(){ 
        var request = $.ajax({
                url: "/cyberisland/html/partials/confirmDelete.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy(this.donConfirmDeletePanel, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.donConfirmDeletePanel = function(html){
        $.vrModuleBlockUI({html: html, position: 'fixed'});
    };

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
        console.log("Request failed: " + textStatus );
    }

    Manipulator.prototype.doDelAllItem = function(evt){
        $('.icon-chk.active').each(function(){
            $(this).parent().parent().remove();
        });
    }

    Manipulator.prototype.doDelItem = function(evt){
        var target = evt.currentTarget;
        $(target).parent().parent().parent().remove();
    }

    Manipulator.prototype.doRotateAllItem = function(evt){
        $('.icon-chk.active').each(function(){
            var img = $(this).parent().find('img');
            var _org_w = img.width();
            var _org_h = img.height();

            var angle1 = _instance_.getRotationDegrees($(img));
            angle1 = angle1 - 90;
            $(img).css("-webkit-transform", "rotate("+ angle1+"deg)");
            $(img).css("-moz-transform", "rotate("+ angle1+"deg)");
            $(img).css("-ms-transform", "rotate("+ angle1+"deg)");
            $(img).css("-o-transform", "rotate("+ angle1+"deg)");
            $(img).css("transform", "rotate("+ angle1+"deg)");
        });
    }

    Manipulator.prototype.doRotateItem = function(evt){
        var target = evt.currentTarget;
        var img = $(target).parent().parent().find('img');
        var angle1 = _instance_.getRotationDegrees($(img));
        angle1 = angle1 - 90;
        $(img).css("-webkit-transform", "rotate("+ angle1+"deg)");
        $(img).css("-moz-transform", "rotate("+ angle1+"deg)");
        $(img).css("-ms-transform", "rotate("+ angle1+"deg)");
        $(img).css("-o-transform", "rotate("+ angle1+"deg)");
        $(img).css("transform", "rotate("+ angle1+"deg)");
    }

    Manipulator.prototype.openSelAlbumPanel = function(evt){
        var request = $.ajax({
                url: "/cyberisland/html/partials/addCollection.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).done(  $.proxy( this.donCallEditPanel, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    };

    Manipulator.prototype.openEditPicsPanel = function(evt){
        var request = $.ajax({
                url: "/cyberisland/html/partials/photoEdit.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).done(  $.proxy( this.donCallEditPanel, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    };

    Manipulator.prototype.donCallEditPanel = function(html){
        $.vrModuleBlockUI({html: html, position: 'absolute'});
    };

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
        console.log("Request failed: " + textStatus );
    }

    Manipulator.prototype.getRotationDegrees = function(obj) {
        var matrix = obj.css("-webkit-transform") ||
        obj.css("-moz-transform")    ||
        obj.css("-ms-transform")     ||
        obj.css("-o-transform")      ||
        obj.css("transform");
        if(matrix !== 'none') {
            var values = matrix.split('(')[1].split(')')[0].split(',');
            var a = values[0];
            var b = values[1];
            var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
        } else { var angle = 0; }
        return (angle < 0) ? angle +=360 : angle;
    }
    

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});