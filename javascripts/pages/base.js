window.Base = (function() {

    var Base = function(){
        this.init();
    };

    Base.prototype.init = function() {
        this.calViewPortScale();
        this.setupEvents();
    };

    Base.prototype.setupEvents = function(){
        $(window).on('scroll', $.proxy( this.onScrollHandel, this ));
        $(window).on('orientationchange', $.proxy( this.onOrientationChange, this ));
        $('.navbar-header .signed').on('click', $.proxy( this.openNavUserOpiton, this ));

        $('.navbar-header .keyword').on('focus', $.proxy( this.onTextFocus, this ));
        $('.navbar-header .keyword').on('blur', $.proxy( this.onTextBlur, this ));
        $('.navbar-header .keyword').on('keypress', $.proxy( this.onTextKeypress, this ));
        
        //$(document).on('touchmove', $.proxy( this.onWinTouchMove, this ));

        //$('a').on('focus', $.proxy(this.removeFocusStatus, this));
    };

    Base.prototype.onOrientationChange = function(e){
        this.calViewPortScale();
    };

    Base.prototype.calViewPortScale = function(){
        if(window.orientation == undefined){
            return false;
        }

        var content_width = 990, screen_dimension = 0;

        if (window.orientation == 0 || window.orientation == 180) {
            // portrait
            screen_dimension = screen.width * 0.96; // fudge factor was necessary in my case
        } else if (window.orientation == 90 || window.orientation == -90) {
            // landscape
            screen_dimension = screen.height;
        }
        content_width = (screen_dimension >= 1320) ? 1320 : 990;
        var viewport_scale = screen_dimension / content_width;
            
        if(viewport_scale == NaN || viewport_scale == 0){
            viewport_scale = 1;
        }

        // resize viewport
        $('meta[name=viewport]').attr('content',
                                      'width='+ content_width +',' +
                                      'minimum-scale=' + viewport_scale + ', maximum-scale=' + viewport_scale);
    };

    Base.prototype.onWinTouchMove = function(evt){
        return false;
    };

    Base.prototype.removeFocusStatus = function(evt){
        var traget = evt.currentTarget;
        $(traget).blur();
    };

    Base.prototype.openNavUserOpiton = function(evt){
        var option = $('.nav-user-opiton');
        if(!option.hasClass('active')){
            option.addClass('active');console.log('openNavUserOpiton');
        }else{
            option.removeClass('active');
        }
    };

    Base.prototype.onTextKeypress = function(evt){
        if (evt.which == 13 ) {
            var keywrod = $('.navbar-header .keyword').val();
            if(keywrod == '站內搜尋' || keywrod == ''){
                $('.navbar-header .keyword').blur();
            }
            window.location.href = '/cyberisland/html/A/A02.html'; 
            evt.preventDefault();
        }
    };

    Base.prototype.onTextFocus = function(evt){
        //$('.keyword').css('width', '120px');
        if($('.navbar-header .keyword').val() == '站內搜尋'){
            $('.navbar-header .keyword').val("");
        }
        $('.navbar-header .keyword').stop().animate({ width: "+=42px" }, 1000);
        $('.navbar-header .navbar-right').stop().animate({ width: "+=42px" }, 1000);
        $('.navbar-header .keyword').parent().next().stop().animate({ opacity: "0" }, 1000);
    };

    Base.prototype.onTextBlur = function(evt){
        var _w = 338;
        if($(window).innerWidth() <= 1315){
            _w = 263;
        }

        $('.navbar-header .keyword').val('站內搜尋'); 
        $('.navbar-header .keyword').stop().animate({ width: "63px" }, 1000);
        $('.navbar-header .navbar-right').stop().animate({ width: _w }, 1000);
        $('.navbar-header .keyword').parent().next().stop().animate({ opacity: "1" }, 1000);
    };

    Base.prototype.onScrollHandel = function(evt){
        var distanceY = $(document).scrollTop(),
            shrinkOn = 300,
            container = $(".navbar-header");
        if (distanceY > shrinkOn) {
            container.addClass("navbar-header_small");
            container.parent().addClass("navbar-header_small");
        } else {
            if(container.hasClass("navbar-header_small")){ 
                container.removeClass("navbar-header_small"); 
                container.parent().removeClass("navbar-header_small");
            }
        }

    };
    return Base;
}(jQuery));

$(document).ready(function() {
    new window.Base();
});