window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.onResizeHandle();
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.btn-login').on('click', $.proxy( this.doLoginHandle, this ));
        
    };

    Manipulator.prototype.onResizeHandle = function(){
        var clientWidth = $(window).innerWidth();
        var clientHeight = $(window).innerHeight();
        var height;

        if (window.orientation && window.orientation == -90) {
          height = clientHeight;
        }
        else {
          height = clientWidth;
        }
        $('.bg').height(clientHeight);
    }

    Manipulator.prototype.doLoginHandle = function(){
        window.location.href = "/cyberisland/html/K/K01.html";
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});