window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.onResizeHandle();
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.termsofUse').on('click', $.proxy(this.doOpenTermsPanel, this));
        //$('.btn-singup').on('click',$.proxy(this.doSignUpHandle, this));
    };


    Manipulator.prototype.doSignUpHandle = function(){
        window.location.href = "/cyberisland/html/N/N03.html";
    }

    Manipulator.prototype.doOpenTermsPanel = function(){

        $.ajax({
                url: "/cyberisland/html/partials/signup_guidelines.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).success(  $.proxy( this.getPanelSuccess, this ))
        .fail( $.proxy( this.getProInfofail, this ));
    }
 
    Manipulator.prototype.getPanelSuccess = function(html){
        $.vrModuleBlockUI({html: html, style: 'background-color:rgba(0,0,0, 0.7);', position: 'fixed'});
    }

    Manipulator.prototype.getProInfofail = function(jqXHR, textStatus){

        console.log("Request failed: " + textStatus);
    }

    Manipulator.prototype.onResizeHandle = function(){
        var clientWidth = $(window).innerWidth();
        var clientHeight = $(window).innerHeight();
        var height;

        if (window.orientation && window.orientation == -90) {
          height = clientHeight;
        }
        else {
          height = clientWidth;
        }
        $('.bg').height(clientHeight);
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});