(function(window, $, undefined) {

    var mc, masker , bounds, x1, y1, xOverlap, yOverlap, pt, limit;


    $.fn.isBound = function(type, fn) {

        var data = this.data('events')[type];


        if (data === undefined || data.length === 0) {
            return false;
        }

        return (-1 !== $.inArray(fn, data));
    };
	
	$.vrModuleMap = function vrModuleMap(options, callback, element) {
        this.element = $(element);
        this._init(options, callback);
    };

    $.vrModuleMap.defaults = {

    };

    $.vrModuleMap.prototype = {
    	_init: function init(options, callback){
    		var instance = this, 
    			ops = opts = this.options = $.extend(true, {}, $.vrModuleMap.defaults, options);
                
            this.svgurl = '/cyberisland/images/taiwanmap/taiwanmap_1203.svg';    
            
            
            this.isMoved = false;
            this._setCity();
            this._setDistrictArea();
            this.element.svg();

            this._orgiViewBox= [0, 200, 1440, 720];
            
            var svg = this.element.svg('get');
            svg.load(this.svgurl, {addTo: true, changeSize: true, onLoad: $.proxy(this._onMapLoaded, this)});
            
    		if($.isFunction(callback)) callback(this);
    	},

    	_onMapLoaded:function onMapLoaded(svg){
            this._initMapViewBox();


            $('#Cities .cityArea').on('mouseenter', $.proxy(this._svgCityAreaMEnter, this));

           /*
            $('#Cities .cityArea').on('click', $.proxy(this._svgCityAreaSelect, this))
                                  .on('mouseenter', $.proxy(this._svgCityAreaMEnter, this));
                                  */

            this._setupEvents();
        },

        _initMapViewBox: function initMapViewBox(){
            var _body_w, _body_h, _wraper_w, _wraper_h, _offset_x, _offset_y, _svg_w, _svg_h;
            var _min_w = 990;
            var ratio =  960 / 1440;

            _wraper_w = this.element.width();
            _wraper_h = this.element.height();
            
            var svg = $(this.element).svg('get');
            $(svg.root()).attr('viewBox', this._orgiViewBox.join(' '));
            bounds = {left:0, top:200, width: 1440, height: 720};

            if(!this.element.hasClass('tw-map-wraper')){
                limit = {left:this.element.offset().left, top: this.element.offset().top, width: this.element.width(), height:  this.element.height() - 68};
            }else{
                limit = {left:($(window).innerWidth() - this.element.width()) / 2, top: $('.navbar-header').innerHeight(), width: this.element.width(), height:  $('.navbar-header').innerHeight()+this.element.height()}
            }

            mc = svg.root();
            pt = mc.createSVGPoint();
            masker = this.element;


        },

        _setupEvents:function setUpEvents(){
            $(window).on('resize', $.proxy( this._onResizeHandle, this ));
            masker.on('mousedown', $.proxy(this._onSvgMouseDown, this));
            masker.on('touchstart', $.proxy(this._onSvgTouchStart, this));
        },

        _onResizeHandle: function onResizeHandle(evt){
             //alert("_onResizeHandle");
            this._rePosMapViewBox();

            if(!this.element.hasClass('tw-map-wraper')){
                limit = {left:this.element.offset().left, top: $('.navbar-header').innerHeight(), width: this.element.width(), height:  this.element.height() - 68};
            }else{
                limit = {left:($(window).innerWidth() - this.element.width()) / 2, top: $('.navbar-header').innerHeight(), width: this.element.width(), height:  628}
            }
        },

        _rePosMapViewBox: function rePosMapViewBox(){

            var _body_w, _body_h, _wraper_w, _wraper_h, _offset_x, _offset_y, _svg_w, _svg_h;
            var _min_w = 990;
            var ratio =  960 / 1440;

            _wraper_w = this.element.width();
            _wraper_h = this.element.height();
            
            var svg = $(this.element).svg('get');
            $(svg.root).attr('width', _wraper_w);
            $(svg.root).attr('height', _wraper_h);
            /*
            if(_wraper_w >= 1320){
                 $(svg.root()).attr('viewBox', this._orgiViewBox1280.join(' '));
            }else{
                 $(svg.root()).attr('viewBox', this._orgiViewBox960.join(' '));
            }*/
        },

        _svgCityAreaMEnter: function svgCityAreaMEnter(evt){
            var target = evt.currentTarget;
            var id = $(target).attr('id');
            var name = this._getCity(id)[0];
            if(name == "" || name == undefined){
                return;
            }

            $(target).on('mouseleave', $.proxy(this._svgCityAreaMLeave, this));

            var sx = evt.clientX  + 20;
            var sy = evt.clientY  - 30;

            $('.cityNameFlag').css('left',evt.clientX  + 20);
            $('.cityNameFlag').css('top', evt.clientY  - 30);

            $('.cityNameFlag p').text(name);
            $('.cityNameFlag').removeClass('hidden');

            if(evt.clientY < limit.top || evt.clientY > limit.height) {
                $(target).trigger('mouseleave');
            }

            if(evt.clientX > limit.left + limit.width-100) {
                $('.cityNameFlag').css('left',evt.clientX  - 20);
                $('.cityNameFlag').css('top', sy);

            }else{
                $('.cityNameFlag').css('left',sx);
                $('.cityNameFlag').css('top', sy);
            }
        },

        
        _svgCityAreaMLeave: function svgCityAreaMLeave(evt){
            var target = evt.currentTarget;
            $(target).off('mouseleave', $.proxy(this._svgCityAreaMLeave, this));

            if(! $('.cityNameFlag').hasClass('hidden')) {
                $('.cityNameFlag').addClass('hidden');
            }
        },

        _svgDistrictMEnter: function svgDistrictMEnter(evt){
            var target = evt.currentTarget;
            var id = $(target).attr('id');
            var name = this._getDistrictArea(id);
            $('.cityNameFlag p').text(name);
            $('.cityNameFlag').removeClass('hidden');

            /*
            var sx = evt.pageX + document.documentElement.scrollTop + 20;
            var sy = evt.pageY + document.documentElement.scrollLeft -30;
            $('.cityNameFlag').css('top',sy);
            $('.cityNameFlag').css('left',sx);*/

            $('.cityNameFlag').css('top',evt.clientY -30);
            $('.cityNameFlag').css('left',evt.clientX + 20);


            $(target).on('mouseleave',$.proxy(this._svgDistrictMLeave, this));
        },

        _svgDistrictMLeave: function svgDistrictMLeave(evt){
            var target = evt.currentTarget;
            $(target).off('mouseleave',$.proxy(this._svgDistrictMLeave, this));
            $('.cityNameFlag').addClass('hidden');
        },

        _svgCityAreaSelect: function svgCityAreaSelect(evt){

            if(this.isMoved){
                return;
            }

            var target = evt.target;
            var id = $(target).attr('id');
            var thisCity = this._getCity(id);

            var city = thisCity[2];

            var viewBoxString = thisCity[1];

            this.svgViewBox = $.map(viewBoxString.split(" "), Number);
            $('svg').stop().animate({ svgViewBox: viewBoxString }, 1000, 'easeInOutCubic', function(){
                $('#Cities .cityArea.select').removeClass('hidden').removeClass('select');
                if($('.cityArea#CITY_KM_square').hasClass('hidden')){
                    $('.cityArea#CITY_KM_square').removeClass('hidden');
                }

                $(target).addClass('select').bind('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){;
                    $(target).unbind('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
                    $(target).addClass('hidden');
                    if(id == "CITY_KM"){
                        $('.cityArea#CITY_KM_square').addClass('hidden');
                    }
                });
            });

            if(this.currentCity !=null || this.currentCity != undefined){
                $('#'+this.currentCity+' .districtArea').off('mouseenter', $.proxy(this._svgDistrictMEnter, this));
            }

            if(city !=null || city != undefined){
                $('#'+city+' .districtArea').on('mouseenter', $.proxy(this._svgDistrictMEnter, this));
            }

            this.currentCity = city;

            if(!$('.btn-showWhollMap').hasClass('active')){
                $('.btn-showWhollMap').addClass('active').on('click', $.proxy(this._doBackToWholeMap, this));
            }
        },

        _svgDistrictAreaSelect: function _svgDistrictAreaSelect(evt){

            if(this.isMoved){
                return;
            }

            var target = evt.target;
            var id = $(target).attr('id');
            window.location.href = '/cyberisland/html/D/D02.html';
        },

    	_doBackToWholeMap:function _doBackToWholeMap(evt){
            var target = evt.currentTarget;
            this.isMoved = false;
            $(target).off('click', $.proxy(this._doBackToWholeMap, this));
            if($(target).hasClass('active')){
               $(target).removeClass('active');
            }

            this._showWholeMap();
    	},

    	backToWhole:function backToWhole(){
            this.isMoved = false;
            var t = $('.btn-showWhollMap');
            t.off('click', $.proxy(this._doBackToWholeMap, this));
            if(t.hasClass('active')){
               t.removeClass('active');
            }

            this._showWholeMap();
    	},

        _showWholeMap: function showWholeMap(){
            if(this.currentCity !=null || this.currentCity != undefined){
                $('#'+this.currentCity+' .districtArea').off('mouseenter', $.proxy(this._svgDistrictMEnter, this));

                this.currentCity = null;
            }

            $('#Cities .cityArea.select').removeClass('hidden').removeClass('select');
            var _wraper_w = this.element.width();
            var svg = $(this.element).svg('get');
            $('svg').stop().animate({svgViewBox: this._orgiViewBox.join(' ') }, 1000, 'easeInOutCubic');
        },

        _onSvgTouchStart: function onSvgTouchStart(evt){
            masker.off('touchstart', $.proxy(this._onSvgTouchStart, this));

            this.isMoved = false;

            var touch = evt.originalEvent.touches[0];
            var svgRect = mc.viewBox.baseVal;
            pt.x = touch.clientX;
            pt.y = touch.clientY;

            var loc = pt.matrixTransform(mc.getScreenCTM().inverse());
            x1 = loc.x;
            y1 = loc.y;

            xOverlap = Math.max(0, bounds.width - svgRect.width);
            yOverlap = Math.max(0, bounds.height - svgRect.height);

            $(document).on("touchmove", $.proxy( this._onWinTouchMove, this ));
            masker.on( "touchmove",$.proxy( this._onSvgTouchMove, this ));
            masker.on( "touchend", $.proxy( this._onSvgTouchEnd, this ));

            //evt.preventDefault();
        },

        _onSvgTouchMove: function onSvgTouchMove(evt){
            this.isMoved = true;

            var touch = evt.originalEvent.touches[0];
            var svgRect = mc.viewBox.baseVal;
            
            var doc = document.documentElement, body = document.body;  
            var pageY = touch.clientY + (doc && doc.scrollTop  ||  body && body.scrollTop  || 0) - (doc && doc.clientTop  || body && body.clientTop  || 0);

            pt.x = touch.clientX;
            pt.y = touch.clientY;

            $('.cityNameFlag').css('top',touch.clientY -30);
            $('.cityNameFlag').css('left',touch.clientX + 20);

            var loc =  pt.matrixTransform(mc.getScreenCTM().inverse());
            var diffx = loc.x - x1;
            var diffy = loc.y - y1;

            var newX = svgRect.x - diffx;
            var newY = svgRect.y - diffy;

            svgRect.x  = newX;
            svgRect.y  = newY;

            if(touch.clientX < limit.left || touch.clientX > limit.left + limit.width - 100 || touch.clientY < limit.top || pageY > limit.height){
                if( !$('.cityNameFlag').hasClass('hidden')){
                    $('.cityNameFlag').addClass('hidden');
                }
                masker.trigger("touchend");
            }

/*

            if(newX < bounds.left || newX > bounds.left + bounds.width || newY < bounds.top || newX > bounds.top + bounds.height){
                masker.trigger("mouseup");
            }
*/
        },

        _onSvgTouchEnd: function onSvgTouchEnd(evt){

            $(document).off("touchmove", $.proxy( this._onWinTouchMove, this ));
            masker.off("touchmove",$.proxy( this._onSvgTouchMove, this ));
            masker.off("touchend", $.proxy( this._onSvgTouchEnd, this ));
            masker.on('touchstart', $.proxy(this._onSvgTouchStart, this));
             //evt.preventDefault();

            if($(evt.target).hasClass("cityArea")){

                if( !$('.cityNameFlag').hasClass('hidden')){
                    $('.cityNameFlag').addClass('hidden');
                }

                this._svgCityAreaSelect(evt);
            }else if($(evt.target).hasClass("districtArea")){

                if( !$('.cityNameFlag').hasClass('hidden')){
                    $('.cityNameFlag').addClass('hidden');
                }

                this._svgDistrictAreaSelect(evt);
            }

        },

        _onWinTouchMove: function onWinTouchMove(evt){
            evt.preventDefault();
        },

        _onSvgMouseDown: function onSvgMouseDown(evt){
            if(evt.button == 0 || evt.button == 1){
                this.isMoved = false;
                masker.off('mousedown', $.proxy(this._onSvgMouseDown, this));

                var svgRect = mc.viewBox.baseVal;
                pt.x = evt.clientX;
                pt.y = evt.clientY;

                var loc = pt.matrixTransform(mc.getScreenCTM().inverse());
                x1 = loc.x;
                y1 = loc.y;

                xOverlap = Math.max(0, bounds.width - svgRect.width);
                yOverlap = Math.max(0, bounds.height - svgRect.height);
                
                $(document).on('mousemove',$.proxy(this._onDocMouseMove, this));
                masker.on('mouseup', $.proxy(this._onSvgMouseUp, this));
                evt.preventDefault();
            }
        },

        _onDocMouseMove: function onDocMouseMove(evt){
            this.isMoved = true;

            $('.cityNameFlag').css('top',evt.clientY -30);
            $('.cityNameFlag').css('left',evt.clientX + 20);


            var svgRect = mc.viewBox.baseVal;

            var doc = document.documentElement, body = document.body;  
            var pageY = evt.clientY + (doc && doc.scrollTop  ||  body && body.scrollTop  || 0) - (doc && doc.clientTop  || body && body.clientTop  || 0);

            pt.x = evt.clientX;
            pt.y = evt.clientY;

            var loc =  pt.matrixTransform(mc.getScreenCTM().inverse());
            var diffx = loc.x - x1;
            var diffy = loc.y - y1;

            var newX = svgRect.x - diffx;
            var newY = svgRect.y - diffy;

            svgRect.x  = newX;
            svgRect.y  = newY;


            if(evt.clientX < limit.left || evt.clientX > limit.left + limit.width - 100 || evt.clientY < limit.top || pageY > limit.height){
                

                if( !$('.cityNameFlag').hasClass('hidden')){
                    $('.cityNameFlag').addClass('hidden');
                }

                masker.trigger("mouseup");
            }


            /*
            if(newX < bounds.left || newX > bounds.left + xOverlap || newY < bounds.top || newY > bounds.top + yOverlap){
                masker.trigger("mouseup");
            }*/

        },

        _onSvgMouseUp: function onSvgMouseDown(evt){
             $(document).off('mousemove',$.proxy(this._onDocMouseMove, this));
             masker.off('mouseup', $.proxy(this._onSvgMouseUp, this));
             masker.on('mousedown', $.proxy(this._onSvgMouseDown, this));

             if($(evt.target).hasClass("cityArea")){
                 if( !$('.cityNameFlag').hasClass('hidden')){
                     $('.cityNameFlag').addClass('hidden');
                 }

                this._svgCityAreaSelect(evt);
             }else if($(evt.target).hasClass("districtArea")){
                this._svgDistrictAreaSelect(evt);

                 if( !$('.cityNameFlag').hasClass('hidden')){
                     $('.cityNameFlag').addClass('hidden');
                 }
             }
        },

        _getCity: function getCity(key){
            var v = this.citys[key];
            if(v != undefined){
                return v;
            }
            return "";
        },

        _setCity: function setCity(){
            this.citys  = {
                'CITY_KL'  : ["基 隆 市"  ,"763 180 288 192", "KL"],
                'CITY_TP'  : ["臺 北 市"  ,"733 180 288 192", "TP"],
                'CITY_NP'  : ["新 北 市"  ,"714 200 288 192", "NP"],
                'CITY_TY'  : ["桃 園 市"  ,"677 210 288 192", "TY"],
                'CITY_HC'  : ["新 竹 市"  ,"613 230 288 192", "HC"],
                'CITY_HCC' : ["新 竹 縣"  ,"656 240 288 192", "HCC"],
                'CITY_ML'  : ["苗 栗 縣"  ,"626 280 288 192", "ML"],
                'CITY_TC'  : ["臺 中 市"  ,"589 335 288 192", "TC"],
                'CITY_NT'  : ["南 投 縣"  ,"603 405 288 192", "NT"],
                'CITY_CH'  : ["彰 化 縣"  ,"554 380 288 192", "CH"],
                'CITY_YL'  : ["雲 林 縣"  ,"537 445 288 192", "YL"],
                'CITY_CY'  : ["嘉 義 市"  ,"545 485 288 192", "CY"],
                'CITY_CYC' : ["嘉 義 縣"  ,"550 490 288 192", "CYC"],
                'CITY_TN'  : ["臺 南 市"  ,"525 535 288 192", "TN"],
                'CITY_KH'  : ["高 雄 市"  ,"540 575 288 192", "KH"],
                'CITY_PT'  : ["屏 東 縣"  ,"540 683 288 192", "PT"],
                'CITY_TT'  : ["臺 東 縣"  ,"552 550 480 320", "TT"],
                'CITY_HL'  : ["花 蓮 縣"  ,"562 370 480 320", "HL"],
                'CITY_IL'  : ["宜 蘭 縣"  ,"733 250 288 192", "IL"],

                'CITY_PH'  : ["澎湖縣"    ,"204 477 300 402", "PH"],
                'CITY_KM'  : ["金門縣"    ,"109 360 288 192", "KM"],
                'CITY_LJ'  : ["連江縣"    ,"254 106 388 292", "LJ"]
            };
        },

        _getDistrictArea: function getDistrictArea(key){
            return $.trim(this.districtArea[key]);
        },

        _setDistrictArea:function setDistrictArea(){
            this.districtArea = {
                                    'KL_01': ["七堵區"],
                                    'KL_02': ["安樂區"],
                                    'KL_03': ["中山區"],
                                    'KL_04': ["暖暖區"],
                                    'KL_05': ["仁愛區"],
                                    'KL_06': ["信義區"],
                                    'KL_07': ["中正區"],
                                    'TP_01': ["文山區"],
                                    'TP_02': ["萬華區"],
                                    'TP_03': ["中正區"],
                                    'TP_04': ["大安區"],
                                    'TP_05': ["信義區"],
                                    'TP_06': ["南港區"],
                                    'TP_07': [" 大同區"],
                                    'TP_08': [" 松山區"],
                                    'TP_09': [" 中山區"],
                                    'TP_10': [" 內湖區"],
                                    'TP_11': [" 士林區"],
                                    'TP_12': [" 北投區"],
                                    'NP_01': [" 三芝區"],
                                    'NP_02': [" 淡水區"], 
                                    'NP_03': [" 八里區"],
                                    'NP_04': [" 林口區"],
                                    'NP_05': [" 五股區"],
                                    'NP_06': [" 泰山區"], 
                                    'NP_07': [" 蘆洲區"],
                                    'NP_08': [" 新莊區"],
                                    'NP_09': [" 三重區"], 
                                    'NP_10': [" 板橋區"],
                                    'NP_11': [" 鶯歌區"],
                                    'NP_12': [" 樹林區"],
                                    'NP_13': [" 土城區"], 
                                    'NP_14': [" 中和區"], 
                                    'NP_15': [" 永和區"], 
                                    'NP_16': [" 三峽區"],
                                    'NP_17': [" 新店區"], 
                                    'NP_18': [" 烏來區"],
                                    'NP_19': [" 深坑區"], 
                                    'NP_20': [" 石碇區"], 
                                    'NP_21': [" 坪林區"],
                                    'NP_22': ["雙溪區"], 
                                    'NP_23': [" 貢寮區"], 
                                    'NP_24': [" 瑞芳區"], 
                                    'NP_25': [" 平溪區"],
                                    'NP_26': [" 汐止區"],
                                    'NP_27': [" 萬里區"],
                                    'NP_28': [" 金山區"], 
                                    'NP_29': [" 石門區"],
                                    'TY_01': [" 楊梅區"], 
                                    'TY_02': [" 新屋區"],
                                    'TY_03': [" 觀音區"], 
                                    'TY_04': [" 龍潭區"], 
                                    'TY_05': [" 平鎮區"], 
                                    'TY_06': [" 中壢區"], 
                                    'TY_07': [" 大園區"],
                                    'TY_08': [" 復興區"], 
                                    'TY_09': [" 大溪區"],
                                    'TY_10': [" 八德區"],
                                    'TY_11': [" 桃園區"], 
                                    'TY_12': [" 龜山區"], 
                                    'TY_13': [" 蘆竹區"],

                                    'HC_01': [" 香山區"], 
                                    'HC_02': [" 北區"],
                                    'HC_03': [" 東區"],

                                    'HCC_01': [" 五峰鄉"], 
                                    'HCC_02': [" 尖石鄉"],
                                    'HCC_03': [" 北埔鄉"],
                                    'HCC_04': [" 峨眉鄉"],
                                    'HCC_05': [" 寶山鄉"], 
                                    'HCC_06': [" 橫山鄉"], 
                                    'HCC_07': [" 竹東鎮"], 
                                    'HCC_08': [" 芎林鄉"], 
                                    'HCC_09': [" 竹北市"],
                                    'HCC_10': [" 關西鎮"],
                                    'HCC_11': [" 新埔鎮"],
                                    'HCC_12': [" 湖口鄉"], 
                                    'HCC_13': [" 新豐鄉"],
                                    'ML_01': [" 卓蘭鎮"], 
                                    'ML_02': [" 大湖鄉"],
                                    'ML_03': [" 獅潭鄉"],
                                    'ML_04': [" 公館鄉"],
                                    'ML_05': [" 銅鑼鄉"], 
                                    'ML_06': [" 三義鄉"],
                                    'ML_07': [" 頭屋鄉"], 
                                    'ML_08': [" 苗栗市"], 
                                    'ML_09': [" 西湖鄉"],
                                    'ML_10': [" 通霄鎮"], 
                                    'ML_11': [" 苑裡鎮"],
                                    'ML_12': [" 造橋鄉"], 
                                    'ML_13': ["後龍鎮"], 
                                    'ML_14': [" 泰安鄉"],
                                    'ML_15': [" 南庄鄉"], 
                                    'ML_16': [" 三灣鄉"],
                                    'ML_17': [" 頭份鎮"],
                                    'ML_18': [" 竹南鎮"],
                                    'TC_01': [" 霧峰區"], 
                                    'TC_02': [" 太平區"], 
                                    'TC_03': [" 北屯區"],
                                    'TC_04': [" 潭子區"],
                                    'TC_05': [" 大里區"],
                                    'TC_06': [" 南區"],
                                    'TC_07': [" 東區"],
                                    'TC_08': [" 中區"],
                                    'TC_09': [" 西區"],
                                    'TC_10': [" 北區"],
                                    'TC_11': [" 烏日區"],
                                    'TC_12': [" 南屯區"], 
                                    'TC_13': [" 西屯區"],
                                    'TC_14': [" 大雅區"],
                                    'TC_15': [" 大肚區"],
                                    'TC_16': [" 龍井區"], 
                                    'TC_17': [" 沙鹿區"],
                                    'TC_18': [" 梧棲區"],
                                    'TC_19': [" 新社區"],
                                    'TC_20': [" 石岡區"],
                                    'TC_21': [" 豐原區"], 
                                    'TC_22': [" 神岡區"], 
                                    'TC_23': [" 清水區"], 
                                    'TC_24': [" 和平區"],
                                    'TC_25': [" 東勢區"], 
                                    'TC_26': [" 后里區"],
                                    'TC_27': [" 外埔區"], 
                                    'TC_28': [" 大甲區"], 
                                    'TC_29': [" 大安區"],
                                    'NT_01': [" 魚池鄉"],
                                    'NT_02': [" 埔里鎮"],
                                    'NT_03': [" 水里鄉"],
                                    'NT_04': [" 中寮鄉"],
                                    'NT_05': [" 鹿谷鄉"],
                                    'NT_06': [" 集集鎮"],
                                    'NT_07': [" 國姓鄉"], 
                                    'NT_08': [" 竹山鎮"], 
                                    'NT_09': [" 信義鄉"],
                                    'NT_10': [" 草屯鎮"], 
                                    'NT_11': [" 仁愛鄉"],
                                    'NT_12': [" 南投市"],
                                    'NT_13': [" 名間鄉"],
                                    'CH_01': [" 二水鄉"], 
                                    'CH_02': [" 溪州鄉"], 
                                    'CH_03': [" 埤頭鄉"],
                                    'CH_04': [" 竹塘鄉"], 
                                    'CH_05': [" 大城鄉"], 
                                    'CH_06': [" 二林鎮"],
                                    'CH_07': [" 芳苑鄉"], 
                                    'CH_08': [" 田中鎮"], 
                                    'CH_09': [" 社頭鄉"], 
                                    'CH_10': [" 員林鎮"], 
                                    'CH_11': [" 北斗鎮"],
                                    'CH_12': [" 田尾鄉"], 
                                    'CH_13': [" 永靖鄉"], 
                                    'CH_14': [" 埔心鄉"],
                                    'CH_15': [" 溪湖鎮"],
                                    'CH_16': [" 埔鹽鄉"],
                                    'CH_17': [" 福興鄉"],
                                    'CH_18': [" 鹿港鎮"], 
                                    'CH_19': [" 芬園鄉"],
                                    'CH_20': [" 大村鄉"],
                                    'CH_21': [" 秀水鄉"],
                                    'CH_22': [" 花壇鄉"], 
                                    'CH_23': [" 彰化市"],
                                    'CH_24': [" 和美鎮"],
                                    'CH_25': [" 線西鄉"],
                                    'CH_26': [" 伸港鄉"],
                                    'YL_01': [" 古坑鄉"], 
                                    'YL_02': [" 斗六市"], 
                                    'YL_03': [" 斗南鎮"], 
                                    'YL_04': [" 大埤鄉"],
                                    'YL_05': [" 元長鄉"],
                                    'YL_06': [" 北港鎮"],
                                    'YL_07': [" 水林鄉"],
                                    'YL_08': [" 口湖鄉"],
                                    'YL_09': [" 四湖鄉"],
                                    'YL_10': [" 虎尾鎮"],
                                    'YL_11': [" 土庫鎮"],
                                    'YL_12': [" 褒忠鄉"], 
                                    'YL_13': [" 東勢鄉"], 
                                    'YL_14': [" 臺西鄉"],
                                    'YL_15': [" 林內鄉"],
                                    'YL_16': [" 莿桐鄉"], 
                                    'YL_17': [" 西螺鎮"],
                                    'YL_18': [" 二崙鄉"],
                                    'YL_19': [" 崙背鄉"],
                                    'YL_20': [" 麥寮鄉"],
                                    'CY_01': [" 東區"],
                                    'CY_02': [" 西區"],
                                    'CYC_01': [" 義竹鄉"],
                                    'CYC_02': [" 布袋鎮"],
                                    'CYC_03': [" 東石鄉"],
                                    'CYC_04': [" 鹿草鄉"],
                                    'CYC_05': [" 朴子市"],
                                    'CYC_06': [" 六腳鄉"],
                                    'CYC_07': [" 太保市"],
                                    'CYC_08': [" 新港鄉"], 
                                    'CYC_09': [" 溪口鄉"],
                                    'CYC_10': [" 水上鄉"], 
                                    'CYC_11': [" 民雄鄉"], 
                                    'CYC_12': [" 大林鎮"],
                                    'CYC_13': [" 中埔鄉"], 
                                    'CYC_14': [" 大埔鄉"],
                                    'CYC_15': [" 番路鄉"],
                                    'CYC_16': [" 竹崎鄉"],
                                    'CYC_17': [" 梅山鄉"], 
                                    'CYC_18': [" 阿里山鄉"],


                                    'TN_01': [" 善化區"], 
                                    'TN_02': [" 官田區"], 
                                    'TN_03': [" 下營區"], 
                                    'TN_04': [" 麻豆區"],
                                    'TN_05': [" 佳里區"],
                                    'TN_06': [" 西港區"],
                                    'TN_07': [" 安定區"],
                                    'TN_08': [" 新市區"],
                                    'TN_09': [" 山上區"],
                                    'TN_10': [" 大內區"],
                                    'TN_11': [" 六甲區"],
                                    'TN_12': [" 柳營區"],
                                    'TN_13': [" 新營區"],
                                    'TN_14': [" 鹽水區"], 
                                    'TN_15': [" 學甲區"],
                                    'TN_16': [" 北門區"], 
                                    'TN_17': [" 將軍區"],
                                    'TN_18': [" 七股區"],
                                    'TN_19': [" 安南區"],
                                    'TN_20': [" 安平區"],
                                    'TN_21': [" 南區"],
                                    'TN_22': [" 中西區"], 
                                    'TN_23': [" 東區"],
                                    'TN_24': [" 北區"],
                                    'TN_25': [" 永康區"], 
                                    'TN_26': [" 仁德區"],
                                    'TN_27': [" 歸仁區"],
                                    'TN_28': [" 關廟區"], 
                                    'TN_29': [" 龍崎區"],
                                    'TN_30': [" 新化區"],
                                    'TN_31': [" 左鎮區"], 
                                    'TN_32': [" 南化區"], 
                                    'TN_33': [" 玉井區"],
                                    'TN_34': [" 楠西區"],
                                    'TN_35': [" 東山區"],
                                    'TN_36': [" 白河區"],
                                    'TN_37': [" 後壁區"],



                                    'KH_01': [" 梓官區"], 
                                    'KH_02': [" 彌陀區"], 
                                    'KH_03': [" 永安區"], 
                                    'KH_04': [" 茄萣區"], 
                                    'KH_05': [" 路竹區"], 
                                    'KH_06': [" 湖內區"], 
                                    'KH_07': [" 旗津區"],
                                    'KH_08': [" 前鎮區"], 
                                    'KH_09': [" 苓雅區"],
                                    'KH_10': [" 鹽埕區"], 
                                    'KH_11': [" 前金區"], 
                                    'KH_12': [" 新興區"], 
                                    'KH_13': [" 鼓山區"], 
                                    'KH_14': [" 三民區"], 
                                    'KH_15': [" 左營區"], 
                                    'KH_16': [" 楠梓區"],
                                    'KH_17': [" 橋頭區"],
                                    'KH_18': [" 岡山區"], 
                                    'KH_19': [" 阿蓮區"],
                                    'KH_20': [" 林園區"],
                                    'KH_21': [" 小港區"], 
                                    'KH_22': [" 鳳山區"], 
                                    'KH_23': [" 鳥松區"],
                                    'KH_24': [" 仁武區"],
                                    'KH_25': [" 大社區"],
                                    'KH_26': [" 燕巢區"], 
                                    'KH_27': [" 田寮區"],
                                    'KH_28': [" 大寮區"],
                                    'KH_29': [" 大樹區"],
                                    'KH_30': [" 旗山區"],
                                    'KH_31': [" 內門區"],
                                    'KH_32': [" 美濃區"], 
                                    'KH_33': [" 杉林區"],
                                    'KH_34': [" 六龜區"],
                                    'KH_35': [" 甲仙區"],
                                    'KH_36': [" 那瑪夏區"], 
                                    'KH_37': [" 茂林區"],
                                    'KH_38': [" 桃源區"],


                                    'PT_01': [" 枋寮鄉"], 
                                    'PT_02': [" 佳冬鄉"], 
                                    'PT_03': [" 林邊鄉"],
                                    'PT_04': [" 東港鎮"], 
                                    'PT_05': [" 新園鄉"], 
                                    'PT_06': [" 萬丹鄉"], 
                                    'PT_07': [" 南州鄉"], 
                                    'PT_08': [" 新埤鄉"],
                                    'PT_09': [" 崁頂鄉"],
                                    'PT_10': [" 潮州鎮"], 
                                    'PT_11': [" 萬巒鄉"],
                                    'PT_12': [" 竹田鄉"],
                                    'PT_13': [" 內埔鄉"],
                                    'PT_14': [" 麟洛鄉"],
                                    'PT_15': [" 長治鄉"],
                                    'PT_16': [" 鹽埔鄉"],
                                    'PT_17': [" 屏東市"], 
                                    'PT_18': [" 九如鄉"],
                                    'PT_19': [" 高樹鄉"],
                                    'PT_20': [" 里港鄉"],
                                    'PT_21': [" 恆春鎮"],
                                    'PT_22': [" 滿州鄉"],
                                    'PT_23': [" 車城鄉"],
                                    'PT_24': [" 牡丹鄉"],
                                    'PT_25': [" 枋山鄉"],
                                    'PT_26': [" 獅子鄉"],
                                    'PT_27': [" 春日鄉"],
                                    'PT_28': [" 來義鄉"],
                                    'PT_29': [" 泰武鄉"],
                                    'PT_30': [" 瑪家鄉"],
                                    'PT_31': [" 霧台鄉"],
                                    'PT_32': [" 三地門鄉"], 
                                    'PT_33': [" 琉球鄉"],



                                    'TT_01': [" 大武鄉"],
                                    'TT_02': [" 太麻里鄉"], 
                                    'TT_03': [" 臺東市"],
                                    'TT_04': [" 達仁鄉"],
                                    'TT_05': [" 金鋒鄉"],
                                    'TT_06': [" 卑南鄉"],
                                    'TT_07': [" 延平鄉"], 
                                    'TT_08': [" 海端鄉"], 
                                    'TT_09': [" 鹿野鄉"],
                                    'TT_10': [" 關山鎮"],
                                    'TT_11': [" 池上鄉"], 
                                    'TT_12': [" 東河鄉"],
                                    'TT_13': [" 成功鎮"], 
                                    'TT_14': [" 長濱鄉"],
                                    'TT_15': [" 蘭嶼鄉"],
                                    'TT_16': [" 綠島鄉"],

                                    'HL_01': [" 富里鄉"],
                                    'HL_02': [" 玉里鎮"],
                                    'HL_03': [" 瑞穗鄉"],
                                    'HL_04': [" 豐濱鄉"], 
                                    'HL_05': [" 光復鄉"], 
                                    'HL_06': [" 鳳林鎮"], 
                                    'HL_07': [" 壽豐鄉"], 
                                    'HL_08': [" 吉安鄉"],
                                    'HL_09': [" 花蓮市"], 
                                    'HL_10': [" 新城鄉"], 
                                    'HL_11': [" 卓溪鄉"], 
                                    'HL_12': [" 萬榮鄉"], 
                                    'HL_13': [" 秀林鄉"],


                                    'IL_01': [" 蘇澳鎮"],
                                    'IL_02': [" 南澳鄉"],
                                    'IL_03': [" 大同鄉"],
                                    'IL_04': [" 冬山鄉"], 
                                    'IL_05': [" 五結鄉"],
                                    'IL_06': [" 羅東鎮"], 
                                    'IL_07': [" 三星鄉"],
                                    'IL_08': [" 員山鄉"], 
                                    'IL_09': [" 宜蘭市"],
                                    'IL_10': [" 壯圍鄉"], 
                                    'IL_11': [" 礁溪鄉"],
                                    'IL_12': [" 頭城鎮"],


                                    'PH_01': [" 湖西鄉"],
                                    'PH_02': [" 馬公市"], 
                                    'PH_03': [" 白沙鄉"], 
                                    'PH_04': [" 西嶼鄉"],
                                    'PH_05': [" 望安鄉"], 
                                    'PH_06': [" 七美鄉"],


                                    'KM_01': [" 金沙鎮"], 
                                    'KM_02': [" 金湖鎮"],
                                    'KM_03': [" 金寧鄉"], 
                                    'KM_04': [" 金城鎮"], 
                                    'KM_05': [" 烈嶼鄉"],
                                    'KM_06': [" 烏坵鄉"],


                                    'LJ_01': [" 南竿鄉"], 
                                    'LJ_02': [" 北竿鄉"], 
                                    'LJ_03': [" 莒光鄉"], 
                                    'LJ_04': [" 東引鄉"]
                                }
        }

    }

    $.fn.vrModuleMap = function(options, callback){
    	var thisCall = typeof options;

        switch(thisCall) {
            // method
            case 'string':
                var args = Array.prototype.slice.call(arguments, 1);

                this.each(function() {
                    var instance = $.data(this, 'vrModuleMap');

                    if (!instance) {
                        return false;
                    }

                    if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                        return false;
                    }

                    instance[options].apply(instance, args);
                });
            break;

            // creation
            case 'object':
                this.each(function() {
                    var instance = $.data(this, 'vrModuleMap');

                    if (instance) {
                        instance.init(options);
                    } else {
                       $.data(this, 'vrModuleMap', new $.vrModuleMap(options, callback, this));
                    }
                });
            break;
        }
    	return this;
    };
    
})(window, jQuery);