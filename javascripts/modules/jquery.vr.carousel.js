(function(window, $, undefined) {
	
    var _timeout;
    var _instance;
    var _isLock;

	$.vrModuleCarousel = function vrModuleCarousel(options, callback, element) {
        _instance = this;
        this.element = $(element);
        this._init(options, callback);
    };

    $.vrModuleCarousel.defaults = {};

    $.vrModuleCarousel.prototype = {
    	_init: function init(options, callback){
    		var instance = this, 
    			ops = opts = this.options = $.extend(true, {}, $.vrModuleCarousel.defaults, options);
    		
            this.container = this.options.container;
    		this.trigger = this.options.trigger;
            this.touch = $('.carousel-inner-touch');

    		this.clientHeight = $(window).innerHeight();
            this.clientWidth = $(window).innerWidth();
            _timeout = setTimeout($.proxy(this._timerOutHandle,this),8000);
            _isLock = false;
            
    		this._setupEvents();
    		if($.isFunction(callback)) callback(this);
    	},

    	_setupEvents:function setUpEvents(){
            this.trigger.on( "click", $.proxy( this._onclicked, this ));
            
            this.touch.on( "mousedown", $.proxy( this._onElmMoseDown, this ));
            this.container.on( "touchstart", $.proxy( this._onElmTouchStart, this ));
            
            $('.carousel-inner .item img').on('dragstart',$.proxy( this._onThumbImageClick, this ));
    	},

        _onThumbImageClick: function onThumbImageClick(){
            return false;
        },

        _onElmTouchStart: function _onElmTouchStart(evt){

            this.container.off( "touchstart", $.proxy( this._onElmTouchStart, this ));
            var touch = evt.originalEvent.changedTouches[0];
            var _orgx = touch.clientX;

            this.container.on( "touchmove",{orgx: _orgx}, $.proxy( this._onElmTouchMove, this ));
            this.container.on( "touchend", {orgx: _orgx}, $.proxy( this._onElmTouchEnd, this ));
        },


        _onElmTouchMove: function onElmTouchMove(evt){

            this.container.off( "touchmove", $.proxy( this._onElmTouchMove, this ));
            var touch = evt.originalEvent.changedTouches[0];
            var sx = touch.clientX;
            var _diffx = sx - evt.data.orgx;

            if(_diffx == 0){
                return false;
            }

            if(_diffx > 0){ 
                _instance._animate(1);
            }else if(_diffx < 0){
                _instance._animate(-1);
            }

            this.container.trigger("touchend");
        },

        _onElmTouchEnd: function onElmTouchEnd(evt){

            this.container.off( "touchmove", $.proxy( this._onElmTouchMove, this ));
            this.container.off( "touchend", $.proxy( this._onElmTouchEnd, this ));
            this.container.on( "touchstart", $.proxy( this._onElmTouchStart, this ));
        },

        _onElmMoseDown: function onElmMoseDown (evt){

            this.touch.off( "mousedown", $.proxy( this._onElmMoseDown, this ));
            var _orgx = evt.clientX;

            this.touch.on( "mousemove",{orgx: _orgx}, $.proxy( this._onElmMoseMove, this ));
            this.touch.on( "mouseup", $.proxy( this._onElmMoseUp, this ));
            evt.preventDefault();
        },
        
        _onElmMoseMove: function onElmMoseMove(evt){

           
            //this.container.css('cursor','grabbing');
            
            var sx = evt.clientX;
            var _diffx = sx - evt.data.orgx;
            if(_diffx == 0){
                return false;
            }

            if(_diffx > 0){ 
                _instance._animate(1);
            }else if(_diffx < 0){
                _instance._animate(-1);
            }
            
            evt.preventDefault();
            this.touch.trigger('mouseup');
        },


        _onElmMoseUp: function onElmMoseUp(evt){

            this.touch.off( "mousemove", $.proxy( this._onElmMoseMove, this ));
            this.touch.off( "mouseup", $.proxy( this._onElmMoseUp, this ));
            this.touch.on( "mousedown", $.proxy( this._onElmMoseDown, this ));
            
            //this.container.css('cursor','normal');
        },

        _timerOutHandle: function timerOutHandle(){
            _instance._animate(-1);
        },

    	_onclicked:function onclicked(evt){

            var delta = ($(evt.target).hasClass("carousel-left")) ? 1 : -1;
            _instance._animate(delta);
            this.touch.trigger('mouseup');
    	},

    	_animate:function animate($delta){
            //return;
            
            if(_timeout){
                clearTimeout(_timeout);
                _timeout = null;
            }

            if(_isLock){
                return false;
            }

             _isLock = true;

    		var active = $('.carousel-inner .active');
            var parent = active.parent();
            var len = active.parent().children().length;
            
            var direction = ($delta == -1) ? "left": "right";
            var classname = ($delta == -1) ? "next": "prev";;
            
            var partner = ($delta == -1) ? active.next(): active.prev(); 
            
            if($delta == 1 && active.index() === 0)
            {
                partner = parent.find(".item:last-child");
            }

            if($delta == -1 && active.index() === len-1)
            {
                partner = parent.find(".item:first-child");
            }
            
            partner.addClass(classname);

            if($delta == -1){
               active.stop().animate({left: '-100%'}, { duration: 600, queue: false, easing:'easeInOutCubic' , complete:function () {
                    active.removeClass('active').removeAttr('style');
               }});
               partner.stop().animate({left: '0%'}, { duration: 600, queue: false , easing:'easeInOutCubic' , complete:function () {
                    partner.addClass('active').removeClass(classname).removeAttr('style');;
                    _timeout = setTimeout($.proxy(_instance._timerOutHandle, this),8000);
                    _isLock = false;

              }});
            }else{
                active.stop().animate({left: '100%'}, { duration: 600, queue: false , easing:'easeInOutCubic' , complete:function () {
                    active.removeClass('active').removeAttr('style');

                }});
                partner.stop().animate({left: '0%'}, { duration: 600, queue: false , easing:'easeInOutCubic' , complete:function () {
                    partner.addClass('active').removeClass(classname).removeAttr('style');;
                    _timeout = setTimeout($.proxy(_instance._timerOutHandle, this),8000);
                    _isLock = false;
                }});
            }

/*
            active.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                console.log('active remove active');
                
            });

            partner.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                console.log('partner remove active direction');

                active.removeClass('active '+ direction);

                partner.addClass('active').removeClass(classname+' '+ direction);

                _timeout = setTimeout($.proxy(_instance._timerOutHandle, this),8000);
                _instance.trigger.on( "click", $.proxy( _instance._onclicked, _instance ));
                _instance.touch.on( "mousedown", $.proxy( _instance._onElmMoseDown, _instance ));
                _instance.container.on( "touchstart", $.proxy( _instance._onElmTouchStart, _instance ));
                _isLock = false;
            });

            partner.addClass(classname).delay(1).queue(function(){ 
                active.addClass(direction);
                $(this).addClass(direction).dequeue();
            });

*/

            
    	},

    	update: function update(){
           
        }
    }

    $.fn.vrModuleCarousel = function(options, callback){
    	var thisCall = typeof options;

        switch(thisCall) {
            // method
            case 'string':
                var args = Array.prototype.slice.call(arguments, 1);

                this.each(function() {
                    var instance = $.data(this, 'vrModuleCarousel');

                    if (!instance) {
                        return false;
                    }

                    if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                        return false;
                    }

                    instance[options].apply(instance, args);
                });
            break;

            // creation
            case 'object':
                this.each(function() {
                    var instance = $.data(this, 'vrModuleCarousel');

                    if (instance) {
                        instance.init(options);
                    } else {
                       $.data(this, 'vrModuleCarousel', new $.vrModuleCarousel(options, callback, this));
                    }
                });
            break;
        }

        return this;
    };
    
})(window, jQuery);