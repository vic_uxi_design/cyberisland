(function(window, $, undefined) {

    $.vrModuleDot = function vrModuleDot(options, callback, element) {
        this.element = $(element);
        this._init(options, callback);
    };

    $.vrModuleDot.defaults = {
        "ellipsis": " ... ",
        "lineHeight" : 30,
        "threshold_word_per_line" : 15
    };

    $.vrModuleDot.prototype = {
        _init: function init(options, callback){
            var instance = this,
                ops = opts = this.options = $.extend(true, {}, $.vrModuleDot.defaults, options);
/*
            this.lineHeight = 30;
            this.threshold_line = 3;
            this.threshold_word_per_line = 15;
            this.threshold_byte_per_line = 2 * this.threshold_word_per_line;
*/
            this.ellipsis = this.options.ellipsis;
           
            if(this.options.elemHeight){
                this.elemHeight = this.options.elemHeight;
            }else{
                this.elemHeight =  this.element.height() - 92 -20;
            }



            this.lineHeight = this.options.lineHeight;
            
            if(this.options.threshold_line){
                this.threshold_line = this.options.threshold_line;
            }else{
                this.threshold_line = 0;
            }

            this.threshold_word_per_line = this.options.threshold_word_per_line;
            this.threshold_byte_per_line = 2 * this.threshold_word_per_line;
            

            this._dotdotdot();
            if($.isFunction(callback)) callback(this);
        },

        _dotdotdot : function dotdotdot(){
            //var height = this.element.height() - 92 -20;
            
            
            if(this.threshold_line == 0){
                this.threshold_line = Math.round(this.elemHeight / this.lineHeight);
            }

            var threshold_byte = this.threshold_byte_per_line * this.threshold_line;
            var bytee = this._stringBytes(this.element.find('p').text());
            if(bytee > threshold_byte){
                var str = this.element.find('p').text().substr(0, (threshold_byte / 2) - 2); 
                str = str + this.ellipsis;
                this.element.find('p').text(str);
            }
        },

        _stringBytes : function stringBytes(c){
            var n=c.length,s;
            var len=0;
            for(var i=0; i <n;i++){
                s=c.charCodeAt(i);
                while( s > 0 ){
                    len++;
                    s = s >> 8;
                }
            }
            return len;
        }

    }

    $.fn.vrModuleDot = function(options, callback){
        var instance = new $.vrModuleDot(options, callback, this);
        return this;
    };

})(window, jQuery);