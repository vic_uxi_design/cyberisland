(function(window, $, undefined) {
    var _instance;

    $.vrModulelightbox = function vrModulelightbox(options, onStartcallback, onEndcallback, element) {
        _instance = this;
        this.element = $(element);
        this.lightbox_body = $('.lightbox-body');
        this.lightbox_wraper = $('.lightbox-wraper');
        this.lightbox_img = $('.lightbox-wraper img');
        this._init(options, onStartcallback, onEndcallback);
    };

    $.vrModulelightbox.defaults = {
        redirect_url: '/cyberisland/html/F/F02.html',
        org_url: window.location.href

    };

    $.vrModulelightbox.prototype = {
    	_init: function init(options, onStartcallback, onEndcallback){
    		var instance = this, 
    			ops = opts = this.options = $.extend(true, {}, $.vrModulelightbox.defaults, options);
    		
            this._org_url  = location.href;
            
            this.onStartcallback = onStartcallback;
            this.onEndcallback = onEndcallback;
            this._rediretc_url = this.options.redirect_url;
            this._org_url = this.options.org_url;
           // this.on('load'), $>proxy(this._onCompleteLoad, this ));

            console.log("enter init");
            this.onStartcallback();
            this._initGoogleMap();
            this._setupEvents();
            
            this._rewriteWindowHistory("pushState", this._rediretc_url);
            //if($.isFunction(callback)) callback(this);
    	},

    	_setupEvents:function setUpEvents(){
            $('.lightbox-wraper img').imagesLoaded($.proxy( this._calLightBox, this));
            
            $('.lightbox-thumb img').on('dragstart',$.proxy( this._onThumbImageClick, this ));
            
            $(window).on('resize', $.proxy( this._onResizeHandle, this ));
            
            $('.lightbox-pictitle p').on('click',$.proxy( this._onRisingHandle, this ));

            $('.lightbox-thumb').on('click',$.proxy( this._onThumbClickHandle, this ));

            if( $.fullscreen.isNativelySupported() ){
                 $('.lightbox-full').on('click',$.proxy( this._onEnterFullScreen, this ));
            }else{
                $('.lightbox-full').addClass('hidden');
            }

            $('.lightbox-report').on('click',$.proxy(this._onReportRequest, this) );
            
            $('.report-box-confirm').on('click',$.proxy( this._onReportConfirmClick, this ));
            $('.report-box-cancel').on('click',$.proxy( this._onReportCancelClick, this ));

            $('.dr-exif-info').on('click',$.proxy( this._onExifclick, this ));
            $('.dr-like').on('click',$.proxy( this._onLikeclick, this ));
            $('.lightbox-close').on('click',$.proxy( this._onCloseclick, this ));
    	},

        _onThumbImageClick: function onThumbImageClick(){
            return false;
        },

        _onCloseclick : function onCloseclick(){
            console.log("enter onCloseclick");
            var isFullScreen = this._isDocumentInFullScreenMode();
            if(isFullScreen){
                $.fullscreen.exit();
            }
            this.onEndcallback();
            this._rewriteWindowHistory("replaceState", this._org_url);
            //window.history.go(-1);
            this.element.remove();
        },

        _rewriteWindowHistory: function rewriteWindowHistory($state, $url){
            if(!window.history.pushState){
                return;
            }

            if($state == "pushState"){
                window.history.pushState({}, '', $url);
            }else if($state == "replaceState"){
                window.history.replaceState({}, '', $url);
            }
        },

        _onLikeclick : function onLikeclick(){
            var value = parseInt($('.dr-like-number').text());
            if( $('.dr-like').hasClass('icons-like') ){
                $('.dr-like').removeClass('icons-like');
                $('.dr-like').addClass('icons-unlike');
                $('.dr-like-number').text(value - 1);
            }else{
                $('.dr-like').removeClass('icons-unlike');
                $('.dr-like').addClass('icons-like');
                $('.dr-like-number').text(value + 1);
            }
        },

        _onExifclick : function onExifclick(){
            if( $('.dr-exif-content').hasClass('hidden') ){
                $('.dr-exif-content').removeClass('hidden');
                $('.dr-exif-string p').text('收起EXIF資訊');
            }else{
                $('.dr-exif-content').addClass('hidden');
                $('.dr-exif-string p').text('展開EXIF資訊');
            }
        },

        _onReportRequest : function onReportRequest(evt){
            if($('.report-box').hasClass('hidden')){
                $('.report-box').removeClass('hidden');
            }else{
                $('.report-box').addClass('hidden');
            }
        },

        _onReportConfirmClick : function onReportConfirmClick(evt){
            $('.report-box').addClass('hidden');
            $('.report-response-box').removeClass('hidden');
            setTimeout(function(){
               $('.report-response-box').addClass('hidden');
            },2000);
        },

        _onReportCancelClick : function onReportCancelClick(evt){
            $('.report-box').addClass('hidden');
        },

        _onEnterFullScreen : function onEnterFullScreen(evt){
            $('.lightbox-full').off('click',$.proxy( this._onEnterFullScreen, this ));
            $('.lightbox-full').on('click',$.proxy( this._onOffFullScreen, this ));
            //this.element.fullScreen({background:"#f7f7f7"});
            this.element.fullscreen({background:"#f7f7f7", overflow: "auto"});
        },

        _onOffFullScreen: function onOffFullScreen(evt){
            console.log("enter onOffFullScreen");
            $('.lightbox-full').off('click',$.proxy( this._onOffFullScreen, this ));
            $('.lightbox-full').on('click',$.proxy( this._onEnterFullScreen, this ));
            //this.element.cancelFullScreen();
            $.fullscreen.exit();
        },

        _onThumbClickHandle :  function onThumbClickHandle(evt){
            var target = evt.currentTarget;
            var src = $(target).find('img').attr('src');
            this.lightbox_img.attr('src',src);
            this.lightbox_img.removeAttr( "style" );
            this._calLightBox();
        },

        _onMoveLeftHandle : function onMoveLeftHandle(){
            var limit = parseInt($('.lightbox-watercourse').width()) - parseInt($('.lightbox-playbody').width());
            var offset = parseInt($('.lightbox-watercourse').css('margin-left'));

            if(offset - 210 >= -limit){
                offset = offset - 210;
                $('.lightbox-watercourse').animate({marginLeft: offset} , 1000, 'easeInOutCubic');
            }
        },

        _onMoveRightHandle : function onMoveRightHandle(){
            var offset = parseInt($('.lightbox-watercourse').css('margin-left'));
            if(offset + 210 <= 0){
                offset = offset + 210;
                $('.lightbox-watercourse').animate({marginLeft: offset} , 1000, 'easeInOutCubic');
            }
        },

        //

        _onPlbyMouseDown: function onPlbyMouseDown(evt){
            console.log("onPlbyMouseDown");
            $('.lightbox-playbody').off('mousedown',$.proxy( this._onPlbyMouseDown, this ));

            var _orgx = evt.pageX + document.documentElement.scrollTop;

            $('.lightbox-playbody').on('mousemove',{orgx: _orgx} ,$.proxy( this._onPlbyMousemove, this ));
            $('.lightbox-playbody').on('mouseup',$.proxy( this._onPlbyMouseup, this ));
        
            
        },

        _onPlbyMousemove: function _onPlbyMousemove(evt){
            console.log("onPlbyMousemove");
            $('.lightbox-playbody').off('mousemove',$.proxy( this._onPlbyMousemove, this ));

            var sx = evt.pageX + document.documentElement.scrollTop;
            var _diffx = sx - evt.data.orgx;
            if(_diffx == 0){
                return false;
            }

            if(_diffx > 0){ 
                this._onMoveLeftHandle();
            }else if(_diffx < 0){
                this._onMoveRightHandle();
            }

            $('.lightbox-playbody').trigger('mouseup');
        },

        _onPlbyMouseup: function _onPlbyMouseup(evt){
            console.log("onPlbyMouseup");
            $('.lightbox-playbody').on('mousedown',$.proxy( this._onPlbyMouseDown, this ));
            $('.lightbox-playbody').off('mousemove',$.proxy( this._onPlbyMousemove, this ));
            $('.lightbox-playbody').off('mouseup',$.proxy( this._onPlbyMouseup, this ));

        },

        _onRisingHandle : function onRisingHandle(){
            if($('.lightbox-footer').hasClass('active'))
            {
                $('.lightbox-playbody').off('mousedown',$.proxy( this._onPlbyMouseDown, this ));
                $('.lightbox-btn-left').off('click',$.proxy( this._onMoveRightHandle, this ));
                $('.lightbox-btn-right').off('click',$.proxy( this._onMoveLeftHandle, this ));
                $('.lightbox-footer').removeClass('active')
                                     .stop()
                                     .animate({ marginTop: "0" }, 1000, 'easeInOutCubic');

                return;
            }

            $('.lightbox-footer').addClass('active')
            .stop()
            .animate({ marginTop: "-178px" }, 1000, 'easeInOutCubic',function(){
                $('.lightbox-playbody').on('mousedown',$.proxy( _instance._onPlbyMouseDown, _instance ));
                $('.lightbox-btn-left').on('click',$.proxy( _instance._onMoveRightHandle, _instance ));
                $('.lightbox-btn-right').on('click',$.proxy( _instance._onMoveLeftHandle, _instance ));
            });

        },
   
        _calLightBox : function calLightBox(){
            console.log('enter calLightBox')
            var limit = 588;
            var clientHeight = $(window).innerHeight();
            var _light_w = this.lightbox_wraper.width();
            var _light_h = clientHeight - 220;
            
            if(_light_h < limit) {
                _light_h = limit;
            }

            this.lightbox_wraper.css('height', _light_h);

            var _org_w, _org_h, _new_w, _new_h, ratio_1, ratio_2;
            _org_w =  this.lightbox_img.width();
            _org_h =  this.lightbox_img.height();


            ratio_1 = _org_h / _org_w;
            ratio_2 = _org_w / _org_h;

            if(_org_w > _org_h){ 
                _new_w = _light_w;
                _new_h = _new_w * ratio_1;
            }else if(_org_w < _org_h){
                _new_h = _light_h;
                _new_w = _new_h * ratio_2;
            }else{
                _new_h = _light_h;
                _new_w = _new_h * ratio_2;
            }

            var offset_x, offset_y;
            offset_x = (_light_w - _new_w) / 2;
            offset_y = (_light_h - _new_h) / 2;

            this.lightbox_img.width(_new_w);
            this.lightbox_img.height(_new_h);

            if(offset_x != 0){
                this.lightbox_img.css('margin-left', offset_x + "px");
            }        

            if(offset_y != 0){
                this.lightbox_img.css('margin-top', offset_y  + "px");
            }
        },

        _onResizeHandle : function onResizeHandle(){
            console.log("enter onResizeHandle");
            this.lightbox_img.removeAttr( "style" );
            this._calLightBox();
        },


        _isDocumentInFullScreenMode: function isDocumentInFullScreenMode() {
            // Note that the browser fullscreen (triggered by short keys) might
            // be considered different from content fullscreen when expecting a boolean
            return ((document.fullscreenElement && document.fullscreenElement !== null) ||    // alternative standard methods
                document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen);                   // current working methods
        },

        _initGoogleMap:function initGoogleMap(){ 
            var agence = new google.maps.LatLng(23.965142, 120.972905),
                  parliament = new google.maps.LatLng(23.965142, 120.972905),
                  image = '/cyberisland/images/darkroom/icn_anchor.png',
                  marker,
                  map;

            var mapOptions = {
              zoom:  8,
              disableDefaultUI: false,
              draggable:true,
              disableDoubleClickZoom:false,
              scrollwheel:true,
              mapTypeControl:false,
              streetViewControl:false,
              zoomControl: true,
              panControl: true,
              zoomControlOptions: {
                  style:google.maps.ZoomControlStyle.SMALL
              },


              mapTypeId: google.maps.MapTypeId.ROADMAP,

              styles: [
                  {
                    featureType: 'landscape',
                    elementType: 'all',
                    stylers: [
                      { hue: '#ebebeb' },
                      { saturation: -100 },
                      { lightness: 29 },
                      { visibility: 'on' }
                    ]
                  },{
                    featureType: 'landscape.man_made',
                    elementType: 'all',
                    stylers: [
                      { hue: '#ebebeb' },
                      { saturation: -100 },
                      { lightness: 29 },
                      { visibility: 'on' }
                    ]
                  },{
                    featureType: 'landscape.natural',
                    elementType: 'all',
                    stylers: [
                      { hue: '#d6d6d6' },
                      { saturation: -100 },
                      { lightness: -12 },
                      { visibility: 'on' }
                    ]
                  },{
                    featureType: 'road.local',
                    elementType: 'all',
                    stylers: [
                      { hue: '#ffffff' },
                      { saturation: -100 },
                      { lightness: 100 },
                      { visibility: 'on' }
                    ]
                  },{
                    featureType: 'poi',
                    elementType: 'all',
                    stylers: [
                      { hue: '#cccccc' },
                      { saturation: -100 },
                      { lightness: 9 },
                      { visibility: 'on' }
                    ]
                  },{
                    featureType: 'road.highway',
                    elementType: 'all',
                    stylers: [
                      { hue: '#f7f7f7' },
                      { saturation: -100 },
                      { lightness: 91 },
                      { visibility: 'on' }
                    ]
                  },{
                    featureType: 'water',
                    elementType: 'all',
                    stylers: [
                      { hue: '#c8d6ed' },
                      { saturation: 10 },
                      { lightness: 40 },
                      { visibility: 'on' }
                    ]
                  }
                ],
                center: agence
            };

            map = new google.maps.Map($('.dr-goole-map')[0],mapOptions);

            marker = new google.maps.Marker({
              icon: image,
              map:map,
              draggable:false,
              animation: google.maps.Animation.DROP,
              position: parliament
            });
        }
    }

    $.fn.vrModulelightbox = function(options, onStartcallback, onEndcallback){
    	var thisCall = typeof options;
        switch(thisCall) {
            // method
            case 'string':
                var args = Array.prototype.slice.call(arguments, 1);

                this.each(function() {
                    var instance = $.data(this, 'vrModulelightbox');

                    if (!instance) {
                        return false;
                    }

                    if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                        return false;
                    }

                    instance[options].apply(instance, args);
                });
            break;

            // creation
            case 'object':
                this.each(function() {
                    var instance = $.data(this, 'vrModulelightbox');
                    if (instance) {
                        instance._init(options, onStartcallback, onEndcallback);
                    } else {
                       $.data(this, 'vrModulelightbox', new $.vrModulelightbox(options, onStartcallback, onEndcallback, this));
                    }
                });
            break;
        }

        return this;
    };
    
})(window, jQuery);