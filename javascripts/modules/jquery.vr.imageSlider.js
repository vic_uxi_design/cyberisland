(function(window, $, undefined) {
    
    var mc, masker , bounds, x1, x2, xOffset, xOverlap;

    $.vrModuleImageSlider = function vrModuleImageSlider(options, callback, element) {
        this.element = $(element);
        this._init(options, callback);
    };

    $.vrModuleImageSlider.defaults = {
        masker: $('.dr-sliders-masker'),
        mc: $('.dr-sliders-masker'),
        leftBottom: $('.dr-btn-left'),
        rightBottom: $('.dr-btn-right')
    };

    $.vrModuleImageSlider.prototype = {
        _init: function init(options, callback){
            var instance = this, 
                ops = opts = this.options = $.extend(true, {}, $.vrModuleImageSlider.defaults, options);
                
            mc = this.options.mc;
            masker = this.options.masker;
            leftBottom = this.options.leftBottom;
            rightBottom = this.options.rightBottom;

            bounds = {left:this.masker.offset().left, top:this.masker.offset().top, width: this.masker.width(), height: this.masker.height()};

            this._setupEvents();
            if($.isFunction(callback)) callback(this);
        },
        _setupEvents:function setUpEvents(){
            this.element.on('remove', $.proxy(this._destroy, this));
            masker.on('mousedown', $.proxy(this._onMouseDown, this));
            leftBottom.on('click', $.proxy(this._onLeftBtnClicked, this));
            leftBottom.on('click', $.proxy(this._onRightBtnClicked, this));
        },

        _onMouseDown: function onMouseDown(e){
            var target = e.currentTarget;

            x1 = x2 = mc.marginLeft;

            xOffset = e.pageX - mc.offset().left;
            xOverlap = Math.max(0, mc.width - bounds.width);
            
            t1 = t2 = new Date().getTime();
            mc.on('mousemove', $.proxy(this._onMouseMove, this));
            mc.on('mouseup', $.proxy(this._onMouseUp, this));
        },

        _onMouseMove: function onMouseMove(e){
            var newX = e.pageX - xOffset;
            var pos;

            if (newX > bounds.left) {
                pos = (newX + bounds.left) * 0.5;
            } else if (newX < bounds.left - xOverlap) {
                pos = (newX + bounds.left - xOverlap) * 0.5;
            } else {
                pos = newX;
            }

            var diff = pos - mc.offset().left;
            mc.marginLeft = mc.marginLeft + diff;

            var t = new Date().getTime();
            if (t - t2 > 50) {
                t2 = t1;
                x1 = mc.marginLeft;
                t1 = t;
            }

            if(e.pageX < bounds.left || e.pageX >  bounds.left + bounds.width || e.pageX < bounds.top || e.pageX > bounds.top + bounds.height){
                mc.trigger('mouseup');
            }
        },

        _onMouseUp: function onMouseUp(e){
            mc.off('mousemove', $.proxy(this._onMouseMove, this));
            mc.off('mouseup', $.proxy(this._onMouseUp, this));

            var time = (new Date().getTime() - t2) / 1000;
            var xVelocity = (mc.offset().left - x2) / time;
            var p = (mc.offset().left - x2 < 0) ? -1 : 1;

            var a = xVelocity * time ;
            var s = (xVelocity·time + a·(time^2)/2) % 20;

            var max = bounds.left, min:bounds.left - xOverlap, resistance = 300;

            var pos = mc.marginLeft + (s * (resistance / 1000)) * p; 
            


            if(xVelocity > 0){
                mc.animate({marginLeft:pos+"px"},{ duration: 300, queue: false, easing:'easeOutQuart'});
            }else{
                mc.animate({marginLeft:pos+"px"},{ duration: 300, queue: false, easing:'easeOutQuart'});
            }



            
            ThrowPropsPlugin.to(mc, {throwProps:{
                x:{velocity:xVelocity, max:bounds.left, min:bounds.left - xOverlap, resistance:300}
            }, onUpdate:blitMask.update, ease:Strong.easeOut
            }, 20, 0.3, 1);


        },

        _onLeftBtnClicked: function onLeftBtnClicked(e){

        },

        _onRightBtnClicked: function onRightBtnClicked(e){

        },

        _onMouseDown: function onMouseDown(e){

        },

        _moveToLeftHandle: function moveToLeftHandle(e){

        },

        _moveToRightHandle: function _moveToRightHandle (e) {
            // body...
        },

        _destroy: function destroy(e){
            mc = null;
            bounds = null;
            x1 = NaN;
            x2 = NaN;
            xOffset = NaN;
            xOverlap = NaN;
        }
    }

    $.fn.vrModuleImageSlider = function(options, callback){
        var thisCall = typeof options;

        switch(thisCall) {
            // method
            case 'string':
                var args = Array.prototype.slice.call(arguments, 1);

                this.each(function() {
                    var instance = $.data(this, 'vrModuleImageSlider');

                    if (!instance) {
                        return false;
                    }

                    if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                        return false;
                    }

                    instance[options].apply(instance, args);
                });
            break;

            // creation
            case 'object':
                this.each(function() {
                    var instance = $.data(this, 'vrModuleImageSlider');

                    if (instance) {
                        instance.init(options);
                    } else {
                       $.data(this, 'vrModuleImageSlider', new $.vrModuleImageSlider(options, callback, this));
                    }
                });
            break;
        }
        return this;
    };
    
})(window, jQuery);