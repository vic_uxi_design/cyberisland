(function(window, $, undefined) {
	
	$.vrModuleGrid = function vrModuleGrid(options, callback, element) {
        this.element = $(element);
        this._init(options, callback);
    };

    $.vrModuleGrid.defaults = {};

    $.vrModuleGrid.prototype = {
    	_init: function init(options, callback){
    		var instance = this, 
    			ops = opts = this.options = $.extend(true, {}, $.vrModuleGrid.defaults, options);
    		this._setupEvents();
    		if($.isFunction(callback)) callback(this);

    	},
    	_setupEvents:function setUpEvents(){
  			this.element.find('.grd-like, .grd-unlike').on('click',this._onClickHandle);
        	this.element.find('.grd-itmbody').mouseover(this._onMouseOver);
        	this.element.find('.grd-itmbody').mouseout(this._onMouseOut);
            this.element.find('col-1').mouseout(this._onMouseOut);
    	},

    	_onClickHandle:function onClickHandle(){
    		var count = parseInt($(this).find(".grd-likenum").text());
	        if($(this).hasClass("grd-like")){
	            $(this).css('background-position','-69px 0px')
	            $(this).addClass("grd-unlike").removeClass('grd-like');
	            $(this).find(".grd-likenum").text(--count);
	        }else{
	            $(this).css('background-position','-69px -24px')
	            $(this).addClass("grd-like").removeClass('grd-unlike');
	            $(this).find(".grd-likenum").text(--count);
	        }
    	},

    	_onMouseOver:function onMouseOver(){
            alert('k');
    		$(this).find('.grd-itminfo').css('display','block');
    	},

    	_onMouseOut:function onMouseOut(){
    		$(this).find('.grd-itminfo').css('display','none');
    	}
    }

    $.fn.vrModuleGrid = function(options, callback){
    	var instance = new $.vrModuleGrid(options, callback, this);
    	return this;
    };
    
})(window, jQuery);