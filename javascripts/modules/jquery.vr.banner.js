(function($) {  
    $.vrModuleSlider = function vrModuleSlider(options, callback, element) {
        this.element = $(element);
        this._init(options, callback);
    }

    $.vrModuleSlider.defaults = {
        container: $(".banner-wrap"),
        trigger: $(".banner-lfarw, .banner-rgarw")
    };

    $.vrModuleSlider.prototype = {
        _init:function init(options, callback){
            
            this.options = $.extend($.vrModuleSlider.defaults , options);
            
            this.container = this.options.container;
            this.trigger = this.options.trigger;

            this.clientWidth = $(window).width();
            this.clientHeight = $(window).height();

            var slider = this.element.find('.banner-slider'),
            first = slider.filter(':first'),
            last = slider.filter(':last');
            first.before(last.clone(true)); 
            last.after(first.clone(true));
            first.addClass("banner-slider_current");
            slider = this.element.find('.banner-slider');

            var len = this.len = slider.children().length;
            this.current = 2;

            this.bannerSize = this._calBannerSize();
            //this._resetImage(this.bannerSize.width, this.bannerSize.height);
            
            var len = this.len;
            this.element.css("width", this.bannerSize.width + "px");
            this.element.css("height", this.bannerSize.height + "px");
            this.container.css("width", len * 100 + "%");
            this.container.css("left", - this.bannerSize.width + "px");
            
            slider.each(function(){
                console.log(1 / len);
                $(this).css("width", (1 / len) * 100 + "%");
            });

            this._setupEvents();
        },

        _setupEvents:function setUpEvents(){
            this.trigger.on( "click", $.proxy( this._onclicked, this ));
        },

        _onclicked:function onclicked(evt){
            var delta = ($(evt.target).hasClass("banner-lfarw")) ? -1 : 1;
            this._animate(delta);
        },

        _animate: function slider_animate($delta){
           
            /*
            if($timer != null) {
                clearTimeout($timer);
            }*/

            var offset = (-(this.bannerSize.width) * $delta);
            
            this.container.stop().animate({ left: "+=" + offset+"px" }, 2000, 'easeInOutCubic', this._animateComplete($delta));
        },

        _animateComplete : function animate_Complete($delta){
            this.container.children(this.current-1).removeClass("banner-slider_current");
            this.current += $delta;
            this.container.children(this.current-1).addClass("banner-slider_current");
          
            var cycle = !!(this.current === 1 || this.current === this.len);

            if (cycle) {
                this.current = (this.current === 1)? this.len -1 : 2;
                offset = -this.bannerSize.width * (this.current - 1);
                this.container.css("left" , offset + "px");
            }
        },

        _resetImage : function resetImage($width, $height){
            $(".banner-slidimg").each(function(){
                var offset_top = 0;
                var offset_left = 0;

                if($(this).width() >= $width){
                    offset_left = -($(this).width() - $width) * 0.5 ;
                }else{
                    offset_left = -($width - $(this).width()) * 0.5 ;
                }

                if($(this).height() >= $height){
                    offset_top = -($(this).height() -  $height) * 0.5 ;
                }else{
                    offset_top = -( $height - $(this).height()) * 0.5 ;
                }

                $(this).css("top", offset_top + "px");
                $(this).css("top", offset_left + "px");
            });
        },

        _calBannerSize:function calculate_banner_size(){
            var bannerWidth = this.clientWidth;
            if(this.clientWidth <= 1290 && this.clientWidth > 960){
                bannerWidth = 1290;
            }else if(this.clientWidth <= 960){
                bannerWidth = 960;
            }

            var bannerHeight = this.clientHeight - 60;
            bannerHeight = (bannerHeight < 800) ? 800 : bannerHeight;
            //console.log({"width":bannerWidth , "height":bannerHeight});
            return {"width":bannerWidth , "height":bannerHeight};
        },

        update: function update(){
            this.clientWidth = $(window).width();
            this.clientHeight = $(window).height();

            this.bannerSize = this._calBannerSize();
            this._resetImage(this.bannerSize.width, this.bannerSize.height);

            var offset = -this.bannerSize.width * (this.current - 1);
            //if(_timer != null) { clearTimeout(_timer);}
            this.container.stop().animate({ left: offset+"px" }, 2000, 'easeInOutCubic', function () {
                //$timer = setTimeout(timeOutHandle, 3000);
                console.log("333pdate");
            });
        }
    };


    $.fn.vrModuleSlider = function(options, callback){
        var thisCall = typeof options;

        switch(thisCall) {
            // method
            case 'string':
                var args = Array.prototype.slice.call(arguments, 1);

                this.each(function() {
                    var instance = $.data(this, 'vrModuleSlider');

                    if (!instance) {
                        return false;
                    }

                    if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                        return false;
                    }

                    instance[options].apply(instance, args);
                });
            break;

            // creation
            case 'object':
                this.each(function() {
                    var instance = $.data(this, 'vrModuleSlider');

                    if (instance) {
                        instance.init(options);
                    } else {
                       $.data(this, 'vrModuleSlider', new $.vrModuleSlider(options, callback, this));
                    }
                });
            break;
        }

        return this;         
    }
    
})(jQuery);