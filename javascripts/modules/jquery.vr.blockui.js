(function(window, $, undefined) {
	var vrModuleBlockUI = function vrModuleBlockUI(options) {
        this._init(options);
    };

    vrModuleBlockUI.defaults = {
        html: "",
        style: "background-color:rgba(#e5ebeb,0.8);",
        position: "fixed"
    };

   vrModuleBlockUI.prototype = {
    	_init: function init(options){
    		var instance = this, 
    			ops = opts = this.options = $.extend(true, {}, $.vrModuleBlockUI.defaults, options);
    		this.style = this.options.style;
            this.position = this.options.position;
            this._drawLayout(this.options.html);
    	},

        _drawLayout: function drawLayout($html){

            $('html, body').scrollTop(0);

            switch(this.position){
                case "fixed":    this._drawFixedLayout($html); break;
                case "absolute":  this._drawAbsoltLayout($html); break;
                default: this._drawFixedLayout($html); break;
            }
        },

        _drawFixedLayout: function drawFixedLayout($html){ 

            this.section = $('<section class="container-fluid popupbkui"><div class="popupbg"></div></section>');
            this.section.find('.popupbg').attr('style', this.style);
            this.section.append($html);
            $('body').append(this.section);
            this._setupEvents();
        },

        _drawAbsoltLayout: function drawAbsoltLayout($html){

            this.section = $('<section class="container-fluid popupbkui-absl"><div class="popupbg"></div></section>');
            this.section.find('.popupbg').attr('style', this.style);
            this.section.append($html);
            $('body').append(this.section);

            var bodyHeight = $(document).innerHeight();
            $('.popupbkui-absl').css('height', bodyHeight);
            this._setupEvents();
        },
    
    	_setupEvents:function setUpEvents(){
            this.section.find('.popupbg').on('click', $.proxy(this._onClosePopUI, this));
  		},

        _onClosePopUI: function onClosePopUI(){
            this.section.remove();
        }
    }

    $.vrModuleBlockUI = function(options){
    	var instance = new vrModuleBlockUI(options);
    	return this;
    };
    
})(window, jQuery);