window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.btn').on('click', $.proxy(this.doSearchHandle, this));
        $('.btn-close').on('click', $.proxy(this.onCloseHandle, this));
    };

    Manipulator.prototype.onCloseHandle = function(evt){
        var target = evt.currentTarget;
        $(target).parent().remove();
    }


    Manipulator.prototype.doSearchHandle = function(evt){
        $('.pop-search').parent().remove();
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});