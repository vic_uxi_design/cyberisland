window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
       // $('.sign-guidelines-content').jScrollPane();
        $(".sign-guidelines-content").mCustomScrollbar();
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.icon-close12x12').on('click', $.proxy(this.oncloseGuidePanel , this));  
    };

    Manipulator.prototype.oncloseGuidePanel = function(evt){
        $('.pop-signup-guidelines').parent().remove();
    };

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});