window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.btn-close').on('click',$.proxy(this.onCancelClick, this ));
        $('.btn-cancel').on('click',$.proxy(this.onCancelClick, this ));
        $('.btn-confirm').on('click',$.proxy(this.onConfirmClick, this ));
        $('.btn-files').on('click',$.proxy(this.onOpenFilesManager, this ));
        $('.btn-create').on('click',$.proxy(this.onOpenAddAlbumPanel, this ));
        $('.file1').on('change', $.proxy(this.getFilesToShowOnPanel, this));
        $('.file-desc-txt').on('click',$.proxy(this.onOpenDescEditor, this ));
        $('.icon-btn-small-rotate-d').on('click',$.proxy(this.doRotateFileItem, this ));
        $('.icon-btn-small-del-d').on('click',$.proxy(this.doDelFileItem, this ));
        $('.file-name').on('click', $.proxy(this.onOpenNameEditor, this ));
    };

    Manipulator.prototype.onOpenNameEditor = function(evt){
        var target = evt.currentTarget;
        var file_name_box = $(target).find('.file-name-box');
        var file_name_text = $(target).find('.file-name-text')
        
        if(file_name_box.hasClass('hidden')){
            file_name_box.removeClass('hidden');
            file_name_text.addClass('hidden');
        }

        $(file_name_box).on('blur', $.proxy(this.onTextFieldBlur, this ));
        $(file_name_box).trigger('focus');
        $(file_name_box).val($(file_name_text).text());
    }

    Manipulator.prototype.onTextFieldBlur = function(evt){ 
        var target = evt.currentTarget;
        var parent = $(target).parent();
        $(target).off('blur', $.proxy(this.onTextFieldBlur, this ));
       
        var file_name_box = $(parent).find('.file-name-box');
        var file_name_text = $(parent).find('.file-name-text')
        
        if(!file_name_box.hasClass('hidden')){
            file_name_box.addClass('hidden');
            file_name_text.removeClass('hidden');
        }

        var str = $.trim($(file_name_box).val());
        if(str == ""){ str = "empty"}

        $(file_name_text).text(str);
    }

    Manipulator.prototype.onOpenAddAlbumPanel = function(evt){
        var request = $.ajax({
                url: "/cyberisland/html/partials/editAlbum.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).done(  $.proxy( this.donCallEditPanel, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    };

    Manipulator.prototype.donCallEditPanel = function(html){
        $.vrModuleBlockUI({html: html, position: 'absolute'});
    };

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
        console.log("Request failed: " + textStatus );
    }

    Manipulator.prototype.doRotateFileItem = function(evt){
        var target = evt.currentTarget;
        var parent =  $(target).parent().parent();
        var img = $(parent).find('img');
        var _org_w = img.width();
        var _org_h = img.height();

        var angle1 = this.getRotationDegrees($(img));
        angle1 = angle1 - 90;
        $(img).css("-webkit-transform", "rotate("+ angle1+"deg)");
        $(img).css("-moz-transform", "rotate("+ angle1+"deg)");
        $(img).css("-ms-transform", "rotate("+ angle1+"deg)");
        $(img).css("-o-transform", "rotate("+ angle1+"deg)");
        $(img).css("transform", "rotate("+ angle1+"deg)");
    }

    Manipulator.prototype.doDelFileItem = function(evt){
        var target = evt.currentTarget;
        var parent =  $(target).parent().parent().parent();
        $(parent).remove();
    }

    Manipulator.prototype.onOpenDescEditor = function(evt){
        var target = evt.currentTarget;
        var parent =  $(target).parent();
        $(target).addClass('hidden');
        $(parent).find('.file-desc-box').removeClass('hidden');
        $(parent).find('.file-textarea').on('blur', $.proxy(this.onTextareaBlur, this ));
        $(parent).find('.file-textarea').trigger('focus');
        $(parent).find('.file-textarea').val($(parent).find('.file-desc-txt p').text());

    }

    Manipulator.prototype.onTextareaBlur = function(evt){
        var target = evt.currentTarget;
        var parent =  $(target).parent().parent();
        $(target).off('blur', $.proxy(this.onTextareaBlur, this));
        $(parent).find('.file-desc-box').addClass('hidden');
        $(parent).find('.file-desc-txt').removeClass('hidden');

        var str = $.trim($(parent).find('.file-textarea').val());
        if(str == ""){ str = "empty"}

        $(parent).find('.file-desc-txt p').text(str);
    }

    Manipulator.prototype.getFilesToShowOnPanel = function(){
        console.log("ss");
        if( !$('.uploadEmtyBox').hasClass('hidden') ){
            $('.uploadEmtyBox').addClass('hidden');
        }

        if( $('.uploadArea').hasClass('hidden')){
            $('.uploadArea').removeClass('hidden');
        }
    }

    Manipulator.prototype.onOpenFilesManager = function(evt){
        //$('#file1').trigger('click');
        this.getFilesToShowOnPanel();
    }

    Manipulator.prototype.onCancelClick = function(evt){
        $('.file-desc-txt').off('click',$.proxy(this.onOpenDescEditor, this ));
        $('.file-desc-txt').on('click',$.proxy(this.onOpenDescEditor, this ));
        $('.pop-upload').parent().remove();
    }

    Manipulator.prototype.onConfirmClick = function(evt){
        $('.btn-create').off('click',$.proxy(this.onOpenAddAlbumPanel, this ));
        $('.btn-confirm').off('click',$.proxy(this.onConfirmClick, this ));
        $('.btn-files').off('click',$.proxy(this.onOpenFilesManager, this ));

        $('.btn-cancel').off('click',$.proxy(this.onCancelClick, this ));
        $('.btn-cancel').on('click',$.proxy(this.doResetStatus, this ));

        var target = evt.currentTarget;
        //$('.pop-upload').parent().remove();
        $('.file-desc-txt').off('click',$.proxy(this.onOpenDescEditor, this ));

        
        $('.fileItem').addClass('disabled');
        $('.btn-confirm').addClass('disabled');
        
        $('.btn-create').addClass('disabled');
        $('.tbl-dropbox-xlg select').attr('disabled', true);
        $('.btn-files').addClass('disabled', true);

        $('.file-status').removeClass('hidden');
    }


    Manipulator.prototype.doResetStatus = function(evt){
        $('.btn-cancel').off('click',$.proxy(this.doResetStatus, this ));
        $('.btn-cancel').on('click',$.proxy(this.onCancelClick, this ));
        $('.file-desc-txt').on('click',$.proxy(this.onOpenDescEditor, this ));
        $('.btn-confirm').on('click',$.proxy(this.onConfirmClick, this ));
        $('.btn-files').on('click',$.proxy(this.onOpenFilesManager, this ));
        $('.btn-create').on('click',$.proxy(this.onOpenAddAlbumPanel, this ));

         $('.fileItem').removeClass('disabled', true);
        $('.btn-confirm').removeClass('disabled', true);
        
        $('.btn-create').removeClass('disabled', true);
        $('.tbl-dropbox-xlg select').removeAttr('disabled');
        $('.btn-files').removeClass('disabled', true);

        $('.file-status').addClass('hidden');
    }

    Manipulator.prototype.getRotationDegrees = function(obj) {
        var matrix = obj.css("-webkit-transform") ||
        obj.css("-moz-transform")    ||
        obj.css("-ms-transform")     ||
        obj.css("-o-transform")      ||
        obj.css("transform");
        if(matrix !== 'none') {
            var values = matrix.split('(')[1].split(')')[0].split(',');
            var a = values[0];
            var b = values[1];
            var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
        } else { var angle = 0; }
        return (angle < 0) ? angle +=360 : angle;
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});