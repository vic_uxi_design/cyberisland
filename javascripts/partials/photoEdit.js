window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.setupEvents();
        this.initGoogleMap(); //tbl-labeladddate tbl-adddate tbl-labeladdmon tbl-addmon
    };

    Manipulator.prototype.setupEvents = function(){
        $('.photoEdit').on('load', $.proxy( this.onloaded, this ));
        google.maps.event.addDomListener(window, 'resize', $.proxy( this.initGoogleMap, this ));
       
        $('.btn-close').on('click',$.proxy(this.onCancelClick, this ));
        $('.cancelBottom').on('click',$.proxy(this.onCancelClick, this ));
        $('.confirmBottom').on('click',$.proxy(this.onConfirmClick, this ));
        $('.btn-advancd-modify').on('click',$.proxy(this.openAdvancdModify, this ));
        $('.tbl-period').on('click', $.proxy(this.onPeriodClicked, this));
    
    };

    Manipulator.prototype.openAdvancdModify = function(){
      $('.btn-advancd-modify').off('click',$.proxy(this.openAdvancdModify, this ));
      $('.btn-cancel-modify').on('click',$.proxy(this.openNormalModify, this ));
      $('.normal-modify-wraper').addClass('hidden');
      $('.advancd-modify-wraper').removeClass('hidden');

      $('.tbl-period').on('click', $.proxy(this.onPeriodClicked, this));
    }  

    Manipulator.prototype.openNormalModify = function(){
      $('.btn-cancel-modify').off('click',$.proxy(this.openNormalModify, this ));
      $('.btn-advancd-modify').on('click',$.proxy(this.openAdvancdModify, this ));

      $('.normal-modify-wraper').removeClass('hidden');
      $('.advancd-modify-wraper').addClass('hidden');

      $('.tbl-period').off('click', $.proxy(this.onPeriodClicked, this));

      $('.tbl-periodMon').removeClass('hidden').next().addClass('hidden')

      $('.tbl-periodDate').addClass('hidden').next().addClass('hidden');

      $('.tbl-periodYear').removeClass('hidden').next().addClass('hidden')
                          .next().addClass('hidden').next().addClass('hidden');
    }

    Manipulator.prototype.onPeriodClicked = function(e){
       var target = e.currentTarget;
       $(target).addClass('hidden');

       var next = $(target).next();
       if(next != undefined && next.hasClass('hidden')){
         next.removeClass('hidden');
       }

       next = $(target).next().next();
       if(next != undefined && next.hasClass('hidden')){
         next.removeClass('hidden');
       }
    }

    Manipulator.prototype.onloaded = function(){
       this.initGoogleMap();
    }

    Manipulator.prototype.onCancelClick = function(evt){
        $('.pop-photoEdit').parent().remove();
    }

    Manipulator.prototype.onConfirmClick = function(evt){
        $('.pop-photoEdit').parent().remove();
    }

    Manipulator.prototype.initGoogleMap = function(){ 
        var agence = new google.maps.LatLng(23.965142, 120.972905),
              parliament = new google.maps.LatLng(23.965142, 120.972905),
              image = '/cyberisland/images/darkroom/icn_anchor.png',
              marker,
              map;

        var mapOptions = {
          zoom:  8,
          disableDefaultUI: false,
          draggable:true,
          disableDoubleClickZoom:false,
          scrollwheel:true,
          mapTypeControl:false,
          streetViewControl:false,
          zoomControl: true,
          panControl: true,
          zoomControlOptions: {
              style:google.maps.ZoomControlStyle.SMALL
          },


          mapTypeId: google.maps.MapTypeId.ROADMAP,

          styles: [
              {
                featureType: 'landscape',
                elementType: 'all',
                stylers: [
                    { hue: '#ebebeb' },
                    { saturation: -100 },
                    { lightness: 29 },
                    { visibility: 'on' }
                ]
                },{
                    featureType: 'landscape.man_made',
                    elementType: 'all',
                    stylers: [
                        { hue: '#ebebeb' },
                        { saturation: -100 },
                        { lightness: 29 },
                        { visibility: 'on' }
                    ]
                },{
                    featureType: 'landscape.natural',
                    elementType: 'all',
                    stylers: [
                        { hue: '#d6d6d6' },
                        { saturation: -100 },
                        { lightness: -12 },
                        { visibility: 'on' }
                    ]
                },{
                    featureType: 'road.local',
                    elementType: 'all',
                    stylers: [
                        { hue: '#ffffff' },
                        { saturation: -100 },
                        { lightness: 100 },
                        { visibility: 'on' }
                    ]
                },{
                    featureType: 'poi',
                    elementType: 'all',
                    stylers: [
                        { hue: '#cccccc' },
                        { saturation: -100 },
                        { lightness: 9 },
                        { visibility: 'on' }
                    ]
                },{
                    featureType: 'road.highway',
                    elementType: 'all',
                    stylers: [
                        { hue: '#f7f7f7' },
                        { saturation: -100 },
                        { lightness: 91 },
                        { visibility: 'on' }
                    ]
                },{
                    featureType: 'water',
                    elementType: 'all',
                    stylers: [
                        { hue: '#c8d6ed' },
                        { saturation: 10 },
                        { lightness: 40 },
                        { visibility: 'on' }
                    ]
                }
            ],
            center: agence
        };

        map = new google.maps.Map($('.tbl-goolemap')[0],mapOptions);

        marker = new google.maps.Marker({
          icon: image,
          map:map,
          draggable:false,
          animation: google.maps.Animation.DROP,
          position: parliament
        });

        google.maps.event.addListener(map, "click", function(e){
            if (marker) {
                marker.setPosition(e.latLng);
            } else {
               marker = new google.maps.Marker({
                    icon: image,
                    map:map,
                    draggable:false,
                    animation: google.maps.Animation.DROP,
                    position: e.latLng
                });
            }
        });
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});