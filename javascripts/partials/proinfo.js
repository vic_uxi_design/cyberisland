window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        //$('.pop-pro .cycle img').imagesLoaded($.proxy( this.initCalAvarta, this));
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.btn-close').on('click', $.proxy(this.onCloseHandle, this));
    };

    Manipulator.prototype.onCloseHandle = function(e){
        var target = e.currentTarget;
        $(target).parent().parent().remove();
    }

    Manipulator.prototype.initCalAvarta = function(){
        var cycle = $('.cycle');
        var avartar = cycle.find('img');
        var _cycle_w = cycle.width();
        var _cycle_h = cycle.height();
        var _avartar_w = avartar.width();
        var _avartar_h = avartar.height();

        var offset_x = (_cycle_w - _avartar_w) / 2;
        var offset_y = (_cycle_h - _avartar_h) / 2;

        avartar.css('width','210px');

        avartar.css('margin-left',offset_x);
        avartar.css('margin-top',offset_y);
    };

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});