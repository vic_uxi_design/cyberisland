window.Manipulator = (function() {
	var _instance_ = null;
    var _current_step = 0;

    var Manipulator = function(){
    	_instance_ = this;
        this.init();
    };

    Manipulator.prototype.init = function() {
    	this.leftButtom = $('.tu-leftbuttom');
    	this.rightButtom = $('.tu-rightbuttom'); 
    	
        this.topleftmask = $('.tu-topleft-mask');
    	this.toprightmask = $('.tu-topright-mask');
    	this.topmiddlemask = $('.tu-topmiddle-mask');

        this.cntleftmask = $('.tu-cntleft-mask');
        this.cntmiddlemask = $('.tu-cntmiddle-mask');
        this.cntrightmask = $('.tu-cntright-mask');
        
        this.contentmask = $('.tu-content-mask');
    	this.tuBreifP = $('.tu-breif p');
    	this.tuDirectArrow = $('.tu-direct-arrow');
    	this.infoArrowLeft = $('.icon-infoArrowLeft');
    	this.infoArrowRight = $('.icon-infoArrowRight');
        
        this.onSetSizebyWindow();
        this.enterStartProcess();
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
    	$(window).on('resize', $.proxy(this.onSetSizebyWindow, this));
    };

	Manipulator.prototype.enterStartProcess = function(evt){

        _current_step = 0;
		this.rightButtom.on('click', $.proxy(this.enterStep1Process, this));
		this.tuBreifP.html("歡迎您成為數位島嶼的會員！<br>您可以由上面黑色底選單 及 反白的數位島嶼Logo 辨識成功登入會員站台</br>以下將有幾個步驟簡單帶您了解會員站台的操作</br>");
		
		var header_fixed_top = $('.navbar-fixed-top');
		var navbar_backen_header = $('.navbar-backen-header');
	
        if(!this.topleftmask.hasClass('hide')){
            this.topleftmask.addClass('hide');
        }

        if(!this.toprightmask.hasClass('hide')){
            this.toprightmask.addClass('hide');
        }

        if(!this.topmiddlemask.hasClass('hide')){
            this.topmiddlemask.addClass('hide');
        }


		if(!this.leftButtom.hasClass('hide')){
			this.leftButtom.addClass('hide');
		}
		
		if(!this.infoArrowLeft.hasClass('hidden')){
    		this.infoArrowLeft.addClass('hidden');
    	}

    	if(!this.infoArrowRight.hasClass('hidden')){
    		this.infoArrowRight.addClass('hidden');
    	}

    };

    Manipulator.prototype.enterStep1Process = function(evt){

        _current_step = 1;
        this.leftButtom.off();
    	this.rightButtom.off();

    	this.leftButtom.on('click', $.proxy(this.enterStartProcess, this));
        this.rightButtom.on('click', $.proxy(this.enterStep2Process, this));

        if(this.leftButtom.hasClass('hide')){
        	this.leftButtom.removeClass('hide');	
        }

        if(this.topleftmask.hasClass('hide')){
            this.topleftmask.removeClass('hide');
        }

        if(this.toprightmask.hasClass('hide')){
            this.toprightmask.removeClass('hide');
        }

        if(this.topmiddlemask.hasClass('hide')){
            this.topmiddlemask.removeClass('hide');
        }

        this.tuBreifP.html("我的首頁可以觀看所有上傳、收藏的照片、及以地理位置標示的照片");

        this.calMaskposition(1);
        
    };

    Manipulator.prototype.enterStep2Process = function(evt){

        _current_step = 2;
    	this.leftButtom.off();
    	this.rightButtom.off();

        this.leftButtom.on('click', $.proxy(this.enterStep1Process, this));
        this.rightButtom.on('click', $.proxy(this.enterStep3Process, this));

		if($('.nav-user-opiton').hasClass('active')){
			$('.nav-user-opiton').removeClass('active');
		}

        this.tuBreifP.html("透過影像中心與快速按鈕新增相簿、上傳照片按鈕可有效率管理個人相片");

        if(this.rightButtom.hasClass('done')){
        	this.rightButtom.find('p').text("下一步");
        	this.rightButtom.removeClass('done');	
        }

        this.calMaskposition(2);

        
    };

    Manipulator.prototype.enterStep3Process = function(evt){

        _current_step = 3;
    	this.leftButtom.off();
    	this.rightButtom.off();

        this.leftButtom.on('click', $.proxy(this.enterStep2Process, this));
        this.rightButtom.on('click', $.proxy(this.startTouseBackend, this));

        if(!$('.nav-user-opiton').hasClass('active')){
            $('.nav-user-opiton').addClass('active');    
        }
		
        this.tuBreifP.html("點選LOGO可以回到數位島嶼前台網站，點選功能選單可<br>預覽個人站台、管理設定、登出會員站台");
        this.rightButtom.find('p').text("都知道了，開始使用")
        this.rightButtom.addClass('done');

        this.calMaskposition(3);

    };

    Manipulator.prototype.startTouseBackend = function(evt){
    	console.log("startTouseBackend");

    	if($('.nav-user-opiton').hasClass('active')){
			$('.nav-user-opiton').removeClass('active');
		}

        $('.bk-tutorial-wraper').on("remove", $.proxy(this.destroy, this));

    	$('.bk-tutorial-wraper').remove();
    };

    Manipulator.prototype.calMaskposition = function($step){
        var diff = (this.clientWidth  - this.cotainerWidth) / 2;
        this.cntleftmask.css('width', diff + this.cotainerWidth - 218);
        this.cntmiddlemask.css('width', 98 );
        this.cntmiddlemask.css('margin-right', diff + 120 );
        this.cntrightmask.css('width', diff + 120);

        switch($step){
            case 1: this.calStep1MaskPos(diff); break;
            case 2: this.calStep2MaskPos(diff); break;
            case 3: this.calStep3MaskPos(diff); break;
        }
    };

    Manipulator.prototype.calStep1MaskPos = function(diff){
        this.topleftmask.css('width', diff + 175 );
        this.topmiddlemask.css('width', this.cotainerWidth - 249);
        this.topmiddlemask.css('margin-left', diff + 249);
        this.toprightmask.css('width', diff);

        this.infoArrowLeft.css('margin-left', '200px');

        if(this.infoArrowLeft.hasClass('hidden')){
            this.infoArrowLeft.removeClass('hidden');
        }
    }

    Manipulator.prototype.calStep2MaskPos = function(diff){
        this.topleftmask.css('width', diff + 279 );
        this.topmiddlemask.css('width', this.cotainerWidth - 455);
        this.topmiddlemask.css('margin-left', diff + 355);
        this.toprightmask.css('width', diff);

        if(this.cntmiddlemask.hasClass('active')){
            this.cntmiddlemask.removeClass('active');
        }

        this.infoArrowLeft.css('margin-left', '310px');

        if(!this.infoArrowRight.hasClass('hidden')){
            this.infoArrowRight.addClass('hidden');
        }

    }

    Manipulator.prototype.calStep3MaskPos = function(diff){
        this.topleftmask.css('width', diff );
        this.topmiddlemask.css('width', this.cotainerWidth - 365);
        this.topmiddlemask.css('margin-left', diff + 145);
        this.toprightmask.css('width', diff + 115);
        
        if(!this.cntmiddlemask.hasClass('active')){
            this.cntmiddlemask.addClass('active');
        }

        this.infoArrowLeft.css('margin-left', '30px');

        if(this.infoArrowRight.hasClass('hidden')){
            this.infoArrowRight.removeClass('hidden');
        }

        this.infoArrowRight.css('margin-left', this.cotainerWidth - 250);
        this.infoArrowRight.css('margin-top', '105px');
    }

    Manipulator.prototype.onSetSizebyWindow = function(){
    	this.clientWidth = $(window).innerWidth();
    	this.cotainerWidth = 1320;
    	if(this.clientWidth < 1320){
    		this.cotainerWidth = 990;
    	}

        if(this.clientWidth < 990){
            this.clientWidth = 990;
        }

        this.calMaskposition(_current_step);
    };

    Manipulator.prototype.destroy = function(){
        this.leftButtom = null;
        this.rightButtom = null;
        
        this.topleftmask = null;
        this.toprightmask = null;
        this.topmiddlemask = null;

        this.cntleftmask = null;
        this.cntmiddlemask = null;
        this.cntrightmask = null;
        
        this.contentmask = null;
        this.tuBreifP = null;
        this.tuDirectArrow = null;
        this.infoArrowLeft = null;
        this.infoArrowRight = null;

        _instance_ = null;
        _current_step = 0;
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});