window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.element = $('.pop-alert');
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.btn-confirm').on('click', $.proxy(this.onConfirmClick, this));
        $('.btn-cancel, .btn-close').on('click', $.proxy(this.onCancelClick, this));
    };

    Manipulator.prototype.onConfirmClick = function(){
        this.element.parent().remove();
    }
    
    Manipulator.prototype.onCancelClick = function(){
        this.element.parent().remove();
    }
    
    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});