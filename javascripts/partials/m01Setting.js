window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.btn-close').on('click',$.proxy(this.onCancelClick, this ));
        $('.cancelBottom').on('click',$.proxy(this.onCancelClick, this ));
        $('.confirmBottom').on('click',$.proxy(this.onConfirmClick, this ));
    };

    Manipulator.prototype.onCancelClick = function(evt){
        $('.pop-setting').parent().remove();
    }

    Manipulator.prototype.onConfirmClick = function(evt){
        $('.pop-setting').parent().remove();
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});