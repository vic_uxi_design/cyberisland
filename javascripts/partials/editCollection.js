window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        
        $('.btn-close').on('click',$.proxy(this.onCancelClick, this ));
        $('.cancelBottom').on('click',$.proxy(this.onCancelClick, this ));
        $('.confirmBottom').on('click',$.proxy(this.onConfirmClick, this ));
        $('.icon-deleteCollection').on('click',$.proxy(this.onConfirmDelete, this ));
    };

    Manipulator.prototype.onConfirmDelete = function(){
        var request = $.ajax({
                url: "/cyberisland/html/partials/confirmDelete.html",
                type: "GET",
                data: { id : '0' },
                dataType: "html"
        }).done(  $.proxy( this.donCallEditPanel, this ))
        .fail( $.proxy( this.onAjaxFail, this ));
    }

    Manipulator.prototype.donCallEditPanel = function(html){
        $.vrModuleBlockUI({html: html, position: 'fixed'});
    };

    Manipulator.prototype.onAjaxFail = function(jqXHR, textStatus){
        console.log("Request failed: " + textStatus );
    }


    Manipulator.prototype.onCancelClick = function(evt){
        $('.pop-editCollection').parent().remove();
    }

    Manipulator.prototype.onConfirmClick = function(evt){
        $('.pop-editCollection').parent().remove();
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});