window.Manipulator = (function() {

    var Manipulator = function(){
        this.init();
    };

    Manipulator.prototype.init = function() {
        this.setupEvents();
    };

    Manipulator.prototype.setupEvents = function(){
        $('.btn-close').on('click', $.proxy(this.closeCityListPage, this)); 
    };

    Manipulator.prototype.closeCityListPage = function(e){
        $('.citylstPage').remove();
    }

    return Manipulator;
}(jQuery));

$(document).ready(function() {
    new window.Manipulator();
});